# Author : Marion Estoup
# Email : marion_110@hotmail.fr
# Date June 2023




########################################### LINKS
### Colors
# https://plotly.com/python/discrete-color/#controlling-discrete-color-order
# https://developer.mozilla.org/en-US/docs/Web/CSS/named-color
# https://community.plotly.com/t/plotly-colours-list/11730/3

### Deploy app
# https://www.youtube.com/watch?v=H16dZMYmvqo


########################################### DASHBOARD PROFESSIONAL PROJECT M2 HEALTH DATA SCIENCE



############################## LOAD LIBRARIES 


# Data manipulation, data cleaning
import pandas as pd
import numpy as np

# Sentiment analysis
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer

# Vizualization
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

# Wordcloud
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import base64
from io import BytesIO


# Network graph 
import dash_cytoscape as cyto
import spacy
import nltk
from nltk import bigrams
import itertools
import collections
import networkx as nx
from nltk import ngrams


# Dash app
import dash 
import dash_bootstrap_components as dbc
from dash import dcc, html
from dash.dependencies import Input, Output
from dash_bootstrap_templates import ThemeSwitchAIO, template_from_url
from dash import dash_table
import plotly.io as pio





################################################################## OPEN FILES

################################################# PATHS
# If app runs on computer
processed_files_path = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/Final_after_january/Files/"


# If app is deployed (dash render and gitlab repository)
#processed_files_path = "https://gitlab.com/m21000/python_projects/covid_app/-/raw/main/"


################################################# FILES FOR TABLE 1 DATA INFORMATION
################################################# SUBTAB 1 BASIC INFORMATION ABOUT THE DASHBOARD

# No file needed, it's just text

################################################# SUBTAB 2 NUMBER OF COLLECTED TWEETS
df_nb_tweets_year = pd.read_csv(processed_files_path+"df_nb_tweets_year_python.csv", sep='\t', index_col=0)


################################################# SUBTAB 3 NUMBER OF COLLECTED TWEETS PER MONTH EACH YEAR
all_df_nb_tweet_month =  pd.read_csv(processed_files_path+"all_df_nb_tweet_month_python.csv", sep='\t', index_col=0)


################################################# SUBTAB 4 CHRONOLOGICAL EVENTS
### Open covid chronological events image and convert it to display it in the app
# If app runs on computer
image_chronological_events = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/chronological_events.png"

# If app is deployed (dash render and gitlab repository)
#image_chronological_events ="chronological_events.png"
image_chronological_events = base64.b64encode(open(image_chronological_events, 'rb').read()).decode('ascii')




################################################################################################################################################




################################################# FILES FOR TABLE 2 SENTIMENT ANALYSIS 
################################################# SUBTAB 1 (id 5) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS
df_nb_tweets_sentiment = pd.read_csv(processed_files_path+"df_nb_tweets_sentiment_python.csv", sep='\t', index_col=0)


################################################# SUBTAB 2 (id 6) PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS
df_sentiment_percentage_range_1 = pd.read_csv(processed_files_path+'df_sentiment_percentage_range_1_python.csv', sep='\t', index_col=0)
df_sentiment_percentage_range_2 = pd.read_csv(processed_files_path+'df_sentiment_percentage_range_2_python.csv', sep='\t', index_col=0)
df_sentiment_percentage_range_3 = pd.read_csv(processed_files_path+'df_sentiment_percentage_range_3_python.csv', sep='\t', index_col=0)
df_sentiment_percentage_range_4 = pd.read_csv(processed_files_path+'df_sentiment_percentage_range_4_python.csv', sep='\t', index_col=0)


################################################# SUBTAB 3 NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS
df_all_time_slots = pd.read_csv(processed_files_path+'df_all_time_slots_python.csv', sep='\t', index_col=0)
df_all_time_slots_percentage = pd.read_csv(processed_files_path+'df_all_time_slots_percentage_python.csv', sep='\t', index_col=0)


################################################# SUBTAB 4 NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS EACH YEAR
df_all_per_year = pd.read_csv(processed_files_path+'df_all_per_year_python.csv', sep='\t', index_col=0)


################################################# SUBTAB 5 SENTIMENTS PER HOUR, DAY, MONTH
final_df_by_time = pd.read_csv(processed_files_path+'final_df_by_time_python.csv', sep='\t', index_col=0)
final_df_day = pd.read_csv(processed_files_path+'final_df_day_python.csv', sep='\t', index_col=0)
final_df_month = pd.read_csv(processed_files_path+'final_df_month_python.csv', sep='\t', index_col=0)


################################################# SUBTAB 6 (id 31) NUMBER AND SENTIMENTS DURING WEEK VS WEEKEND
df_nb_tweets = pd.read_csv(processed_files_path+"df_nb_tweets_python.csv",sep='\t', index_col=0)
df_nb_tweets_per_week = pd.read_csv(processed_files_path+"df_nb_tweets_per_week_python.csv", sep='\t', index_col=0)
df_nb_tweets_per_weekend = pd.read_csv(processed_files_path+"df_nb_tweets_per_weekend_python.csv", sep='\t', index_col=0)






################################################################################################################################################





################################################# FILES FOR TABLE 3 WORDCLOUDS

# Stopwords
stopwords_teletravail = ["de", "le", "la", "et", "un", "une", "à", "au", "je","ma","que","ont",
             "en","on","donc","il","moi","ce","ai","vos","parce","auront","a","me",
             "suis","ça","dans","mes", "e", "ils","est","c","h","car","d","spire","mon",
             "hui", "ne","du","eu","les","s","m","v","l","teletr","teletra","sa",
             "avez","cet","jim","quand","jai","votr","dtype","emplo","salut","vu",
             "denis","jsuis","lmaoooo","halpert","sommes","aujourd","Name","commeunlundi",
             "fais","Preprocessed_tweets","après","mêmes","auprès","avoir","nous",
             "vous","redécouvrent","être","était","entre","the","ben","j","mais",
             "allez","object","soeur","annonçaient","des","oh", "aussi", "clim",
             "quel", "dont", "cec", "ah","nos","sujet","life","si","stock",
             "cela","mis","giec","twee","quadra","veulent","moui","oui",
             "tu","va","depuis","ainperdu","ou","as","vient","toi",
             "qui","es","là","p","où","today","étais","sont","avons","net",
             "par","elon","rt","tous","tain","n","même","bonjour",
             "y","lorsque","peut","lié","vais","fair","insère",
             "pour","fait","décli","somme","pour","apres",
             "habitent","chez","dernière","chaque","tout","Length",
             "demain","dom","très","comme","pas","healthcare",
             "auxiliaire","citoyens","croyais","menant","'","be",
             "so","se","t","vue","commeunlundi","citoyen","spire",
             "croire","bureau","foi","pense","révélé","vie",
             "travail",
             "pratique","domicile","vive","entreprise","excessive", 
             "jour","avec","moin","leur","qu","plus","trop","non","elle","temp","sans","son", 
             "semaine","personne", "bon","an","maison","cette","sur","alors","autre","journée", 
             "votre","aux","faire","encore","gens","moins", "faut","ca","ses","comment", 
             "rien","pendant","peu","beaucoup","juste", "avant","déjà","voir","vraiment", 
             "lieu","jours","te","aller","temps","toute","merci","etc","bah","vois","ni","ta","vont",
             "selon","veux","savoir","via","ton","font","ces","notre","truc","dit","trouve","certain","avais",
             "exemple","lui","tant","point"] 


################################################# SUBTAB 1 GENERAL WORDCLOUD
final_df = pd.read_csv(processed_files_path+'final_df_dash_python.csv', sep='\t', index_col=0)

# explore data 
final_df.columns # columns User, Tweet, Lanugage, Tweet_coordinates, Tweet_place, etc
final_df.dtypes # all columns have dtype object
# Convert List_words column from object to string 
final_df["List_words"] = final_df["List_words"].astype('string')
# Check types of column 
final_df.dtypes
# Column List_words is a string type

# Count how many different users we have
final_df.User.nunique() # 6705 so on 7639 tweets in total, only 6705 expressed by different users

# Generate General wordcloud
wc = WordCloud(stopwords=stopwords_teletravail,
               background_color = 'black',
               width = 800,
               height = 600).generate(' '.join(final_df["Preprocessed_tweets"])).to_image()




################################################# SUBTAB 2 POSITIVE WORDCLOUD 
### Open files final_df_sentiment with sentiments and dates (months, positive, negative, neutral sentiments)
final_df_positive = pd.read_csv(processed_files_path+'final_df_positive_python.csv', sep='\t', index_col=0)

# Generate Positive wordcloud
wc_positive = WordCloud(stopwords=stopwords_teletravail,
               background_color = 'black',
               width = 800,
               height = 600).generate(' '.join(final_df_positive["Preprocessed_tweets"])).to_image()




################################################# SUBTAB 3 NEGATIVE WORDCLOUD
final_df_negative = pd.read_csv(processed_files_path+'final_df_negative_python.csv', sep='\t', index_col=0)

# Generate Negative wordcloud
wc_negative = WordCloud(stopwords=stopwords_teletravail,
               background_color = 'black',
               width = 800,
               height = 600).generate(' '.join(final_df_negative["Preprocessed_tweets"])).to_image()




################################################# SUBTAB 4 NEUTRAL WORDCLOUD
final_df_neutral = pd.read_csv(processed_files_path+'final_df_neutral_python.csv', sep='\t', index_col=0)

# Generate Neutral wordcloud
wc_neutral = WordCloud(stopwords=stopwords_teletravail,
               background_color = 'black',
               width = 800,
               height = 600).generate(' '.join(final_df_neutral["Preprocessed_tweets"])).to_image()


# Decode wordcloud images to be able to display them with dash
with BytesIO() as buffer:
    wc.save(buffer, format='PNG')
    wc_positive.save(buffer, format='PNG')
    wc_negative.save(buffer, format='PNG')
    wc_neutral.save(buffer, format='PNG')
    general_wordcloud = base64.b64encode(buffer.getvalue()).decode()
    positive_wordcloud = base64.b64encode(buffer.getvalue()).decode()
    negative_wordcloud = base64.b64encode(buffer.getvalue()).decode()
    neutral_wordcloud = base64.b64encode(buffer.getvalue()).decode()




################################################# SUBTAB 5 WORDCLOUD TABLE
wordclouds_table = pd.read_csv(processed_files_path+'wordclouds_table_python.csv', sep='\t',index_col=0)






###################################################################################################################################################


################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 1 LDA MODEL INTERPRETATION

# Nothing to import

################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 2 LDA MODEL 5 TOPICS

# Nothing to import
# Define the path in spyder on the top right where the asset file is 
# Thus we can open the LDA models in dash app

################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 2 LDA MODEL 3 TOPICS

# Nothing to import
# Define the path in spyder on the top right where the asset file is 
# Thus we can open the LDA models in dash app





###################################################################################################################################################


################################################# TAB 5 NETWORK GRAPH

# The path on the top right needs to be the one where the list is saved
# Function to open the list
def loadList(filename):
    # the filename should mention the extension 'npy'
    tempNumpyArray=np.load(filename,allow_pickle=True)
    return tempNumpyArray.tolist()

# Open the list
docs_teletravail = loadList('docs_teletravail_list.npy')


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 1 NETWORK BIGRAMS
# If app runs on computer
network_bigram = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/networkgraph_40bigrams.png"

# If app is deployed (dash render and gitlab repository)
#network_bigram = "networkgraph_40bigrams.png"
network_bigram = base64.b64encode(open(network_bigram, 'rb').read()).decode('ascii')

bigram_df_teletravail = pd.read_csv(processed_files_path+'bigram_df_teletravail_python.csv', sep='\t',index_col=0)


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 2 NETWORK TRIGRAMS
# If app runs on computer
network_trigram = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/networkgraph_40trigrams.png"

# If app is deployed (dash render and gitlab repository)
#network_trigram = "networkgraph_40trigrams.png"
network_trigram = base64.b64encode(open(network_trigram, 'rb').read()).decode('ascii')

trigram_df_teletravail = pd.read_csv(processed_files_path+'trigram_df_teletravail_python.csv', sep='\t',index_col=0)


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 3 NETWORK FOURGRAMS
# If app runs on computer
network_fourgram = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/networkgraph_40fourgrams.png"


# If app is deployed (dash render and gitlab repository)
#network_fourgram = "networkgraph_40fourgrams.png"
network_fourgram = base64.b64encode(open(network_fourgram, 'rb').read()).decode('ascii')

fourgram_df_teletravail = pd.read_csv(processed_files_path+'fourgram_df_teletravail_python.csv', sep='\t',index_col=0)





################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 4 INTERACTIVE NETWORK
### Create fifthgrams, save them and afterwards create network graph with fifthgrams

# How many grams
n_5=5

# List for trigram
fifthgram_teletravail = [list(ngrams(words,n_5)) for words in docs_teletravail]

# Create list
fifthgrams_list = list(itertools.chain(*fifthgram_teletravail))

# Create counter of words to count occurence of each word contained in our trigrams
fifthgram_counts = collections.Counter(fifthgrams_list)

# Create dataframe of trigram and counter for the 40 most common trigrams
fifthgram_df_teletravail = pd.DataFrame(fifthgram_counts.most_common(40), 
                             columns=['trigram', 'count'])

# Create dictionary of trigrams and their counts
fifthgram_dictionnary_teletravail = fifthgram_df_teletravail.set_index('trigram').T.to_dict('records')



### Create network graph trigram and export/save it

# Create network plot, store it in network_graph 
# Create empty graph with no nodes and no edges
network_graph_fifth  = nx.Graph()

# Create connections between nodes
# Add edges to our graph network_graph 
# Create connections between nodes
# k are all the bigrams
# v I think the number of times there is each bigrams
for k, v in fifthgram_dictionnary_teletravail[0].items():
    network_graph_fifth .add_edge(k[0], k[1], weight=(v)) # v or v*10


# Define figure and axes sizes
fig, ax = plt.subplots(figsize=(20, 15))

# k distance between the nodes
# seed for reproductibility
pos = nx.spring_layout(network_graph_fifth , k=10, seed=7)

# Plot networks
nx.draw_networkx(network_graph_fifth, pos,
                 #edge_labels= edge_weight,
                 #font_size=8,
                 width=3, # width of the edges
                 edge_color='grey',
                 node_color='purple',
                 with_labels = False, # Nodes names, if True they are inside the circles but not clean because big
                 ax=ax)  # ax size of the white panel (background)

# Create offset labels (labels for nodes appearing above or under the symbols)
for key, value in pos.items():
    x, y = value[0]+.080, value[1]+.050 # Define location of labels text
    ax.text(x, y, # location of labels text
            s=key,
            bbox=dict(facecolor='red', alpha=0.25), # red color and transparency of background with alpha=0.25
            horizontalalignment='center', fontsize=13) # align label text in the center and size of text with fontsize
    
plt.axis('off')


# Convert column type
fifthgram_df_teletravail["trigram"] = fifthgram_df_teletravail["trigram"].astype('string')
fifthgram_df_teletravail.dtypes
# Sort values of data frame
fifthgram_df_teletravail = fifthgram_df_teletravail.sort_values('count')


###################################################################################################################################################








########################################################################## DASH APPLICATION 



################################################ DASH APP LAYOUT

# Two different templates stored in variables for the dash app
#template_theme1 = "cyborg"
#template_theme2 = "darkly"
#url_theme1 = dbc.themes.CYBORG
#url_theme2 = dbc.themes.DARKLY

# Link which gives access to the templates
dbc_css = dbc_css = "https://cdn.jsdelivr.net/gh/AnnMarieW/dash-bootstrap-templates/dbc.min.css"
# Load templates to use them with plotly express
pio.templates

## Creating the app 

# Create the app and define the main theme in external_stylesheets
app = dash.Dash("Covid_Dashboard", external_stylesheets=[dbc.themes.DARKLY, dbc_css]) #  



# Define a sentence that appears on the top left of the app in order to choose between the two proposed themes
#header = html.P("Click if you want to change theme of graphics",style={'textAlign': 'left','font-size':'300','font-family':'helvetica'})


# If app is deployed (dash render and gitlab repository) run server=app.server
# If app runs on computer, remove this line
#server= app.server

# Create the layout of the app 
#app.layout = dbc.Container(
app.layout= html.Div([ # html Div to use html components (style, text, tabs, subtabs etc)
                       
                       html.Br(), # add space
                       # Add title on top of the app
                       html.H2('Health consequences during covid-19 pandemic dashboard', style={'textAlign': 'center','font-size':'300','font-family':'helvetica'}),
                       html.Br(), # add space
                       
                           #dbc.Row(
                            #   [
                             #      dbc.Col( # Define the two possible themes to choose
                              #         [
                                           #header, # header is the sentence that we've defined earlier in header variable
                                          
                               #            ThemeSwitchAIO( # switch with icons
                                #               aio_id="theme", # id of the switch
                                 #              themes=[url_theme1,url_theme2], # proposed themes that we've defined earlier
                                  #             ),
                                           
                                   #        ], style={'marginLeft': 15})]),
                       #html.Br(),
                       
                       # Create tabs and their sub tabs
                       dcc.Tabs(id='tabs', value='Tab1', children=[
                           
                                                                   
                                                           
                           # First tab Data information and its subtabs                                        
                           dcc.Tab(label='Data information', id='tab1', value='Tab1', style={'textAlign': 'center', 'font-size':'100','font-family':'helvetica', 'border-radius': '15px', 'marginLeft': 15},children =[ 
                               dcc.Tabs(id="subtabs1", value='Subtab1', children=[ # Create sub tabs 
                                   dcc.Tab(label='Basic information about the dashboard', id='subtab1', value='Subtab20', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px', 'marginLeft': 15}), 
                                   dcc.Tab(label='Number of collected tweets', id='subtab2', value='Subtab2', style={'textAlign': 'center', 'font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                   dcc.Tab(label='Number of collected tweets per month each year', id='subtab3', value='Subtab3', style={'textAlign': 'center', 'font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                   dcc.Tab(label='Chronological Events', id='subtab4', value='Subtab4', style={'textAlign': 'center', 'font-size':'100','font-family':'helvetica', 'border-radius': '15px'})]) 
                                   
                                   ]),
                           
                               
                           # Second tab Sentiment Analysis and its subtabs
                           dcc.Tab(label='Sentiment Analysis', id='tab2', value='Tab2', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}, children=[
                                dcc.Tabs(id="subtabs2", value='Subtab2', children=[ # Create sub tabs 
                                    dcc.Tab(label='Number or percentage of tweets according to sentiments', id='subtab5', value='Subtab5', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px', 'marginLeft': 15}), 
                                    dcc.Tab(label='Percentage of tweets according to sentiments and time slots', id='subtab6', value='Subtab6', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                    dcc.Tab(label='Number or percentage of tweets according to sentiments and time slots', id='subtab7', value='Subtab7', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}),
                                    dcc.Tab(label='Number or percentage of tweets according to sentiments each year', id='subtab8', value='Subtab8', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}),
                                    dcc.Tab(label='Sentiments per hour, day, month', id='subtab30', value='Subtab30', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}),
                                    dcc.Tab(label='Sentiments during week and weekend', id='subtab31', value='Subtab31', style={'textAlign': 'center','font-size':'100','font-family':'helvetica','border-radius': '15px'})])
                                    
                                    ]), 
                           
                           
                           
                           # Third tab Wordclouds and its subtabs
                           dcc.Tab(label='Wordclouds', id='tab21', value='Tab21', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}, children=[ 
                                dcc.Tabs(id="subtabs21", value='Subtab21', children=[ # Create sub tabs 
                                    dcc.Tab(label='General Wordcloud', id='subtab22', value='Subtab22', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px', 'marginLeft': 15}), 
                                    dcc.Tab(label='Positive Wordcloud', id='subtab23', value='Subtab23', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                    dcc.Tab(label='Negative Wordcloud', id='subtab24', value='Subtab24', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                    dcc.Tab(label='Neutral Wordcloud', id='subtab25', value='Subtab25', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}),
                                    dcc.Tab(label='Wordcloud Table', id='subtab26', value='Subtab26', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'})]) 
                                
                                ]),
                                    
                                
                                
                           
                           # Fourth tab Latent Dirichlet Allocation Model and its subtabs
                           dcc.Tab(label='Latent Dirichlet Allocation Model', id='tab3', value='Tab3', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}, children=[ 
                                dcc.Tabs(id="subtabs3", value='Subtab3', children=[ # Create sub tabs
                                    dcc.Tab(label='LDA Model Interpretation', id='subtab9', value='Subtab9', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px', 'marginLeft': 15}), 
                                    dcc.Tab(label='LDA Model 5 topics', id='subtab10', value='Subtab10', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                    dcc.Tab(label='LDA Model 3 topics', id='subtab11', value='Subtab11', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'})]) 
                                    
                                    ]), 
                                
                                
                           
                           # Fifth tab Network Graph and its sub tabs
                           dcc.Tab(label='Network Graph', id='tab4', value='Tab4',style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}, children=[ 
                                dcc.Tabs(id="subtabs4", value='Subtab4', children=[ # Create sub tabs
                                    dcc.Tab(label='Network bigrams', id='subtab13', value='Subtab13', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px', 'marginLeft': 15}), 
                                    dcc.Tab(label='Network threegrams', id='subtab14', value='Subtab14', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}), 
                                    dcc.Tab(label='Network fourgrams', id='subtab15', value='Subtab15', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'}),
                                    dcc.Tab(label='Interactive Network', id='subtab16', value='Subtab16', style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'border-radius': '15px'})])   
                                    
                                    ])
                           
                           ])
                       # Use only 98% of screen width for the app
                       ], style={'width': "98%"}, className="dbc") # className to have template within tabs and subtabs
                           #className="dbc", style={ "height" : "100%",'width': "100%"})


                                      


###################################################################################################################################################

    
    

################################################ DASH APP CONTENT (graphs)




################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 1 (id 20) BASIC INFORMATION ABOUT THE DASHBOARD

# Callback for Tab 1 and Subtab 1 Basic information about the dashboard
@app.callback(Output('subtab1', 'children'), # Output 
              [Input('subtabs1', 'value')])#, # Input 
              #Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the selected theme/template

# Create function to display info in subtab 1 of tab 1 Basic information about the dashboard
def display_info_subtab1(value): 
    
    if value == 'Subtab20':# If we are in subtab2 in tab 1 we return a text
        
    # We return text with dcc.Markdown and we style the text (color, justify, width, ...)
        return html.Br(), html.Div([dcc.Markdown('''
                            
                            ### This dashboard contains five tabs :
                                ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}), # #E45756 rgb(115,111,76) #316395 #3366CC
                               
                                
                            
                          dcc.Markdown('''
                            * Data information
                            * Sentiment Analysis
                            * Wordclouds
                            * Latent Dirichlet Allocation Model
                            * Network Graph''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}), # rgb(175,100,88) rgb(254,217,166)
                            
                            
                            dcc.Markdown('''
                            ### Each tab contains several subtabs. The first tab Data information contains :
                                ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}), # #4C78A8
                                
                                
                            dcc.Markdown('''
                                         * Basic information about the dashboard
                                         * Number of collected tweets (Bar chart)
                                         * Number of collected tweets per month each year (Bar chart)
                                         * Chronological events (Picture)
                                         ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}), # rgb(255,255,204)
                                         
                            dcc.Markdown('''
                            ### The second tab Sentiment Analysis contains :
                                ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}),
                                
                                
                            dcc.Markdown('''
                                         
                                         * Number or percentage of tweets according to sentiments (2 bar charts or 2 pie charts)
                                         * Percentage of tweets according to sentiments and time slots (4 bar charts or 4 pie charts)
                                         * Number or percentage of tweets according to sentiments and time slots (2 bar charts)
                                         * Number or percentage of tweets according to sentiments each year (2 bar charts)
                                         * Sentiments per hour, day, month (3 line charts)
                                         * Sentiments during week and weekend (6 bar charts or 6 pie charts)
                            ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}), # #8C564B rgb(175,100,88)
                            
                            
                            
                            dcc.Markdown('''
                                         ### The third tab Wordclouds contains :
                                             ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}), # rgb(217,175,107) #316395
                                             
                                             
                            dcc.Markdown('''
                                         
                                         * General Wordcloud (based on all tweets)
                                         * Positive Wordcloud (based on tweets considered as positive)
                                         * Negative Wordcloud (based on tweets considered as negative)
                                         * Neutral Wordcloud (based on tweets considered as neutral)
                                         * Wordcloud Table
                                         ''', style= {'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}),
                                         
                                         
                            dcc.Markdown('''
                                         ### The fourth tab Latent Dirichlet Allocation Model contains :
                                             ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}),
                                             
                                             
                            dcc.Markdown('''
                                         * LDA Model Interpretation (LDA explanations)
                                         * LDA Model 5 topics
                                         * LDA Model 3 topics
                                           ''', style= {'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}),
                                           
                                           
                            dcc.Markdown('''
                                         ### The fifth tab Network Graph contains :
                                             ''', style={'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'}),
                                             
                                             
                            dcc.Markdown('''
                                         * Network bigrams (1 network, 1 bar chart)
                                         * Network trigrams (1 network, 1 bar chart)
                                         * Network fourgrams (1 network, 1 bar chart)
                                         * Interactive Network (1 interactive network, 1 bar chart)
                                         ''', style= {'text-align': 'justify', "width":"70%", 'marginLeft': 250, 'color':'rgb(166,86,40)'})
                                        
                            
                            
                            ]) # className="dbc"



    

################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 2 (id 2) NUMBER OF COLLECTED TWEETS

# Callback for Tab 1 and subtab 2 Number of collected tweets
@app.callback(Output('subtab2', 'children'), # Output (what we see) is the content of subtab 2 in tab 1
              [Input('subtabs1', 'value')])#, # Input (what we give) is the content of the different subtabs that we choose in tab 1
              #Input(ThemeSwitchAIO.ids.switch("theme"),"value")]) # Another input is the selected theme/template

# Create function to display graph in subtab 2 of tab 1 Number of collected tweets
def display_graph_subtab2(value): 
    
    if value == 'Subtab2':# If we are in subtab2 in tab 1 we return a graph
        #template = template_theme1 if toggle else template_theme2 # We define the theme selected
        
        figure = px.bar(df_nb_tweets_year, # bar plot
                        x="Year", # x axis
                        y='Tweet', # y axis
                        labels = {'Year': 'Years', # x axis name
                                  'Tweet': 'Number of tweets'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of collected tweets per year').update_xaxes(type='category') # plot title
        
        
        figure.update_layout(title=dict(x=0.5)) # font=dict(family="helvetica", size=50) # title position
        #figure.update_traces(marker=dict(colors=['darkblue']))
        figure.update_traces(marker_color='cornflowerblue') # bar colors
        
        return html.Div([ # html components
                         html.Br(), # add space
                         # We return the graph previouslyc reated
                         dcc.Graph(id="graph_subtab2", figure=figure, style={'width':'60%', 'marginLeft': 300})  
                         ]) # style={"text-align":"center"}



    

################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 3 (id 3) NUMBER OF COLLECTED TWEETS PER MONTH EACH YEAR

# User select year that he wants (between 2019-2022)
# Then he can see bar plot or line plot with number of collected tweet each month for the selected year

# Callback for Tab 1 and subtab 3 to display graph and dropdown (list of choices (years))
@app.callback(Output('subtab3', 'children'), # Output 
              [Input('subtabs1', 'value')])  # Input 

# Function to create a dropdown menu and graph
def dropdown_subtab3(value):  
    
    if value == 'Subtab3': # If we are in Subtab3
        return html.Div([ # html components
                         
                    html.Br(),
                    html.P("Choose the year when you want to see the number of collected tweets per month", # Create sentence that appears above the dropdown list
                           style={'textAlign': 'center', 'font-size':'100','font-family':'helvetica'}), # Style of the sentence
                    dcc.Dropdown(id='dropdown_subtab3', # Create dropdown (list of choices (years))
                    options=[ # Define the options of the dropdown
                             {'label': years, 'value': years} for years in all_df_nb_tweet_month['Year'].unique()],# the dropdown list contains each year
                    placeholder= 'Select the year', # Placeholder, text that appears within the dropdown 
                    style={'textAlign': 'center','font-size':'100','font-family':'helvetica', 'background-color':'#222',
                           "width": "50%", 'margin':'auto'}, # dropdown style
                    className="dbc"), # Placeholder style
                    
                    html.Br(), # add space
                    dcc.Graph(id="plot_subtab3", style={'width':'60%','marginLeft': 300}) # id of graph
                    ])
                                    

       
    
# Callback for Tab 1 and sub tab 3 to display graph
@app.callback(
    Output("plot_subtab3", "figure"), # Output is the plot
    [Input("dropdown_subtab3", "value")]) # Input is the data from the list in the dropdown 

# Function to update the graph depending on which year we choose in the dropdown menu 
def update_graph_subtab3(dropdown): 

    mask = (all_df_nb_tweet_month.Year == dropdown) # The item selected in the dropdown corresponds to the value of years
    
    fig = px.bar(all_df_nb_tweet_month[mask], # create bar plot
                 x='Month', y='Tweet', # x and y axis
                 labels = {'Month':'Months', # x axis name
                           'Tweet':'Number of tweets'}, # y axis name
                 template="plotly_dark", # template
                 category_orders={"Month": ["January", "February", "March", "April", # orders for the categories in x axis
                                            "May", "June", "July", "August",
                                            "September", "October", "November", "December"]},
                 title="Number of collected tweets per month for the selected year") # plot title
    
    fig.update_layout(title=dict(x=0.5)) # font=dict(family="helvetica") size=40 # title position
    fig.update_traces(marker_color='cornflowerblue') # bar colors
    #fig.update_traces(marker=dict(colors=['darkblue']))
    
    
    return fig # style={"text-align":"center"}
                        








################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 4 (id 4) CHRONOLOGICAL EVENTS


# Callback for Tab 1 and subtab 4 Image of covid chronological events France
@app.callback(Output('subtab4', 'children'), # Output 
              [Input('subtabs1', 'value')]) # Input

# Create function to display image (chronological events)
def display_wordcloud(value): # 2 parameters value for which subtab and toggle for theme/template
    
    if value == 'Subtab4':# If we are in subtab 4
        
        return html.Div(children=[ # We return title with dcc Markdown
            
            html.Br(), # add space
            
            dcc.Markdown('''
                         ### Main COVID chronological events (France)
                         ''', style= {'color':'rgb(166,86,40)', 'text-align':'center', 'font-family':'helvetica'}), # title style
                                        
            #html.H3(
             #   children="Main COVID chronological events (France)",
              #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px",
               # ),
            
            html.Br(),
            html.Div(children= # we return the image with html.Img
                     [html.Img(src='data:image/png;base64,{}'.format(image_chronological_events),width=800, height=500)],
                      style={"text-align":"center"}
                      )])


 

###################################################################################################################################################
    












################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 1 (id 5) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS

# Callback for Tab 2 Subtab 5 Number and percentage of tweets positive, negative, neutral
@app.callback(Output('subtab5', 'children'), # Output 
              [Input('subtabs2', 'value')]) # Input

# Function to create graph and radioitem (choose type of graph bar plot or pie chart)
def radioitem_subtab5(value): 
    if value == 'Subtab5': # If we are in subtab 5 Tab 2 we return a graph and radioitems

        return html.Div([ # html components
                         html.Br(),
                         html.P("Choose the type of graph that you want to see", # Sentence above the radio item
                                style={'textAlign': 'center','font-size':'100','font-family':'helvetica'}),  # Style of the sentence
                         
                         dcc.RadioItems(id='radio_items_type_graph', # dcc.RadioItems allows to create radio items selection
                                        options=[ # Definition of the options  within the radio items
                                                 {'label': 'Pie chart', 'value':'pie'}, # First radio item with label Pie chart
                                                 {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                        value='bar', # Default value when we open the app/ sub tab (bar chart)
                                        style = {'text-align': 'center'},
                                        className="dbc"), # Style of radio items
                         html.Br(),
                         dcc.Graph(id="first_graph", style={'display': 'inline-block', "width": "50%", "height": "50%"}), # First  graph
                         dcc.Graph(id="second_graph", style={'display': 'inline-block', "width": "50%", "height": "50%"}) # Second graph
                         # inline block to have graphs side by side
                         ])
                     


# Callback for Tab 2 Subtab 5 Number and percentage of tweets positive, negative, neutral
@app.callback(
    [Output("first_graph", "figure"), # Output
     Output("second_graph", "figure")],# Output
    [Input('radio_items_type_graph', 'value')]) # Another input is the selection of the radio item (pie chart or bar chart)


# Function to update the graph depending on radio item selection (pie or bar chart) for tab 2 subtab 5
def update_graph_subtab5(radio_items):

     
    if radio_items == 'pie': # If the user chooses the radio item pie we return a pie chart
    
        # pie chart for percentage of tweets (positive, negative, neutral)
        fig = px.pie(df_nb_tweets_sentiment, values=df_nb_tweets_sentiment['Sentiment_obtained_percentage'], # create pie chart
                     names=df_nb_tweets_sentiment['Sentiment_obtained_teletravail'], # values within the pies
                     title='Percentage of tweets considered as negative, positive and neutral', # graph title
                     template="plotly_dark") # template
        
        fig.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # pie colors
        fig.update_layout(showlegend=False, title_x=0.5) # hide legend and center title
        
        # pie chart for number of tweets (positive, negative, neutral)
        fig1 = px.pie(df_nb_tweets_sentiment, values=df_nb_tweets_sentiment['Tweet'], # create second pie chart
                     names=df_nb_tweets_sentiment['Sentiment_obtained_teletravail'], # values within the pies
                     title='Number of tweets considered as negative, positive and neutral', # graph title
                     template="plotly_dark") # template
        
        fig1.update_traces(textinfo='value',marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # allows to add numbers (values) instead of percentage
        fig1.update_layout(title_x=0.5) # center graph title
        
            
        return fig, fig1 # We return fig and fig1 (percentage and number, pie charts)
    

    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar chart
    
        fig3 = px.bar(df_nb_tweets_sentiment, x="Sentiment_obtained_teletravail", # create bar chart (percentage)
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained_teletravail", # values in the bars
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        
        fig3.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral', # Graph title
            title_x=0.5, # center title
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments"), # legend title
            showlegend=False # hide legend
            )
  
    
        fig2 = px.bar(df_nb_tweets_sentiment, x="Sentiment_obtained_teletravail", # create second bar chart (number))
                      y="Tweet", # y axis
                      color="Sentiment_obtained_teletravail", # values in the bars
                      color_discrete_map = {"Positive":"olive", # bar oclors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        fig2.update_layout(
            title_text='Number of tweets considered as negative, positive and neutral', # Plot title
            title_x=0.5, # center title
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Number of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments") # Legend title
            )
        
        return fig3, fig2 # We return fig3 and fig2 (number and percentage, bar charts)








################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 2 (id 6) PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS

# Callback for Tab 2 sub tab 6
@app.callback(Output('subtab6', 'children'), # Output 
              [Input('subtabs2', 'value')]) # Input 

# Function to create graph and radio item to choose type of graph (pie or bar chart)
def radioitem_subtab6(value): 
    if value == 'Subtab6': # If we are in subtab6 tab 2 we return a radio items and graph

        return html.Div([# html components
                         
                         html.Br(),
                         html.P("Choose the type of graph that you want to see", # Sentence above the radio item
                                style={'textAlign': 'center','font-size':'100','font-family':'helvetica'}),  # Style of the sentence
                         
                         dcc.RadioItems(id='radio_items_type_graph_2', # dcc.RadioItems allows to create radio items selection
                                        options=[ # Definition of the options  within the radio items
                                                 {'label': 'Pie chart', 'value':'pie_chart'}, # First radio item with label Pie chart
                                                 {'label': 'Bar chart', 'value': 'bar_chart'}], # Second radio item with label Bar chart
                                        value='bar_chart', # Default value when we open the app/ sub tab
                                        style = {'text-align': 'center'},
                                        className="dbc"), # Style of radio items
                         html.Br(),
                         dcc.Graph(id="first_graph_time", style={'display': 'inline-block', "width": "50%", "height": "50%"}), # First graph
                         dcc.Graph(id="second_graph_time", style={'display': 'inline-block', "width": "50%", "height": "50%"}), # Second graph
                         #html.Br(),
                         dcc.Graph(id="third_graph_time", style={'display': 'inline-block', "width": "50%", "height": "50%"}), # Third graph
                         dcc.Graph(id="fourth_graph_time", style={'display': 'inline-block', "width": "50%", "height": "50%"}) # Fourth graph
                         ])



# Callback for Tab 2 subtab 6 Number of tweets considered as positive, negative and neutral according to different time slots
@app.callback(
    [Output("first_graph_time", "figure"), # Output
     Output("second_graph_time", "figure"), # Output
     Output("third_graph_time", "figure"), # Output
     Output("fourth_graph_time", "figure")],# Output
    [Input('radio_items_type_graph_2', 'value')]) # Input is the selection of the radio item (pie chart or bar chart)



# Function to update the graph 
def update_graph_subtab6(radio_items): # radio_items for radio items selection and toggle for theme/template

     
    if radio_items == 'pie_chart': # If the user chooses the radio item pie we return a pie plot
        
        # First pie chart for first time range midnight - 8 am
        fig_time = px.pie(df_sentiment_percentage_range_1, values=df_sentiment_percentage_range_1["Sentiment_obtained_percentage"], # Create first pie chart
                     names=df_sentiment_percentage_range_1['Sentiment_obtained'], # pie values
                     title='Percentage of tweets considered as negative, positive and neutral between midnight and 9am', # Graph title
                     template="plotly_dark", # template
                     hole=.3) # allows to create donut pie (hole size)
        fig_time.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # pie colors
        fig_time.update_layout(annotations=[dict(text='Midnight - 9am', x=0, y=0.50, font_size=15, showarrow=False)], # text on the left
                               showlegend=False, # hide legend
                               font=dict(size=10)) # title size
        
        # Second pie chart for second time range 8am midday
        fig_time_1 = px.pie(df_sentiment_percentage_range_2, values=df_sentiment_percentage_range_2["Sentiment_obtained_percentage"], # Create second pie chart
                     names=df_sentiment_percentage_range_2['Sentiment_obtained'],
                     title='Percentage of tweets considered as negative, positive and neutral between 9am and 1pm', # Graph title
                     template="plotly_dark", # template
                     hole=.3) # allows to create donut pie (hole size)
        fig_time_1.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive']))
        fig_time_1.update_layout(annotations=[dict(text='9am - 1pm', x=0, y=0.50, font_size=15, showarrow=False)], # text on the left
                                 font=dict(size=10)) # title size
        
        
        # Third pie chart for third time range midday - 6pm
        fig_time_2 = px.pie(df_sentiment_percentage_range_3, values=df_sentiment_percentage_range_3["Sentiment_obtained_percentage"], # Create third pie chart
                     names=df_sentiment_percentage_range_3['Sentiment_obtained'],
                     title='Percentage of tweets considered as negative, positive and neutral between 1pm and 5pm', # Graph title
                     template="plotly_dark", # template
                     hole=.3) # allows to create donut pie (hole size)
        fig_time_2.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # pie colors
        fig_time_2.update_layout(annotations=[dict(text='1pm - 5pm', x=0, y=0.50, font_size=15, showarrow=False)], # text on the left
                                 showlegend=False, # hide legend
                                 font=dict(size=10)) # title size
        
        
        # Fourth pie chart for fourth time range 6pm - midnight
        fig_time_3 = px.pie(df_sentiment_percentage_range_4, values=df_sentiment_percentage_range_4["Sentiment_obtained_percentage"], # Create fourth pie chart
                     names=df_sentiment_percentage_range_4['Sentiment_obtained'],
                     title='Percentage of tweets considered as negative, positive and neutral between 5pm and midnight', # Graph title
                     template="plotly_dark", # template
                     hole=.3) # allows to create donut pie (hole size)
        fig_time_3.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # pie colors
        fig_time_3.update_layout(annotations=[dict(text='5pm - Midnight', x=0, y=0.50, font_size=15, showarrow=False)],# text on the left
                                 font=dict(size=10)) # title size
        

        # Return the fourth pie charts for percentage of positive, negative, neutral tweets
        return fig_time, fig_time_1, fig_time_2, fig_time_3

    
    # If the user chooses the radio item bar we return a bar chart
    if radio_items == 'bar_chart': 

        # First bar chart for first time range midnight 8 am
        fig_time_4 = px.bar(df_sentiment_percentage_range_1, x="Sentiment_obtained", # Create first bar chart
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        
        fig_time_4.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral between midnight and 9am', # Plot title
            font=dict(size=10), # title size
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments"), # legend title
            showlegend=False# Hide legend
            )
        
        # Second bar chart for second time range 8 am - midday
        fig_time_5 = px.bar(df_sentiment_percentage_range_2, x="Sentiment_obtained", # Create second bar chart
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        fig_time_5.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral between 9am and 1pm', # Plot title
            font=dict(size=10), # title size
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments") # Legend title
            )
        
        # Third bar chart for third time range midday - 6pm
        fig_time_6 = px.bar(df_sentiment_percentage_range_3, x="Sentiment_obtained", # Create third bar chart
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        fig_time_6.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral between 1pm and 5pm', # Plot title
            font=dict(size=10), # title size
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments"), # legend title
            showlegend=False# Hide legend
            )
        
        
        # Third bar chart for third time range 6pm - midnight
        fig_time_7 = px.bar(df_sentiment_percentage_range_4, x="Sentiment_obtained", # Create fourth bar chart
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        fig_time_7.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral between 5pm and midnight', # Plot title
            font=dict(size=10), # title size
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments") # Legend title
            )
        




        # Return the fourth bar charts for percentage of positive, negative, neutral tweets
        return fig_time_4, fig_time_5, fig_time_6, fig_time_7
  




        



################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 3 (id 7) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS

### All time slots on the same graph instead of separated graph for each time slot like in tab 1 subtab 6
# Callback for Tab 1 and subtab 2 Number of collected tweets
@app.callback(Output('subtab7', 'children'), # Output 
              [Input('subtabs2', 'value')]) # Input 

# Create function to display graph 
def display_graph_subtab7(value): # 2 parameters value for which subtab and toggle for theme/template
    
    if value == 'Subtab7':# If we are in subtab7 in tab 2 we return a graph
        
        # First bar chart to display Number of negative, positive, and neutral tweets according to different time slots
        figure = px.bar(df_all_time_slots, # bar plot
                        x="Time_slot", # x axis
                        y='Tweet', # y axis
                        color="Sentiment_obtained_teletravail",
                        color_discrete_map = {"Positive":"olive",
                                              "Negative":"firebrick",
                                              "Neutral":"cornflowerblue"},
                        labels = {'Time_slot': 'Time slots', # x axis name
                                  'Tweet': 'Number of tweets'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of negative, positive, and neutral tweets according to different time slots').update_xaxes(type='category')
        
        figure.update_layout(title=dict(x=0.5, font=dict(family="helvetica", size=20)), # size, font, center title
                             legend=dict(title="Sentiments"))
                              

        
        # Second bar chart to display Percentage of negative, positive, and neutral tweets according to different time slots
        figure_2 = px.bar(df_all_time_slots_percentage, # bar plot
                        x="Time_slot", # x axis
                        y='Sentiment_obtained_percentage', # y axis
                        color="Sentiment_obtained",
                        color_discrete_map = {"Positive":"olive",
                                              "Negative":"firebrick",
                                              "Neutral":"cornflowerblue"},
                        labels = {'Time_slot': 'Time slots', # x axis name
                                  'Sentiment_obtained_percentage': 'Percentage of tweets'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Percentage of negative, positive, and neutral tweets according to different time slots').update_xaxes(type='category')

        
        figure_2.update_layout(title=dict(x=0.5, font=dict(family="helvetica", size=20)), # size, font, center title
                               showlegend=False) # # hide legend
        
        
        return html.Div([ # html components
                         html.Br(), # add space
                         # we return both graphs
                         dcc.Graph(id="graph_subtab7", figure=figure_2, style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         dcc.Graph(id="graph2_subtab7", figure=figure, style={'display': 'inline-block', "width": "50%", "height": "50%"}) 
                         ])
    
    

    


################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 4 (id 8) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS EACH YEAR

### Bar charts and pie charts (user can choose)

# Callback for Tab 2 subatb 8 Number of tweets positive negative and neutral per year
@app.callback(Output('subtab8', 'children'), # Output 
              [Input('subtabs2', 'value')]) # Input

# Create function to display graph 
def display_graph_subtab8(value): 
    
    if value == 'Subtab8':# If we are in subtab 8 tab 2 we return a graph
        
        # First bar chart to display Number of negative, positive, and neutral tweets according to different years
        figure = px.bar(df_all_per_year, # bar plot
                        x="Year", # x axis
                        y='Tweet', # y axis
                        color="Sentiment_obtained_teletravail", # bar values
                        color_discrete_map = {"Positive":"olive", # bar colors
                                              "Negative":"firebrick",
                                              "Neutral":"cornflowerblue"},
                        labels = {'Year': 'Years', # x axis name
                                  'Tweet': 'Number of tweets'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of negative, positive, and neutral tweets each year').update_xaxes(type='category') # plot title
        
        figure.update_layout(title=dict(x=0.5, font=dict(family="helvetica", size=25)), # size, font, center title
                             legend=dict(title="Sentiments")) # legend title
                             

        
        # Second bar chart to display Percentage of negative, positive, and neutral tweets according to different years
        figure_2 = px.bar(df_all_per_year, # bar plot
                        x="Year", # x axis
                        y='Sentiment_obtained_percentage', # y axis
                        color="Sentiment_obtained_teletravail", # bar values
                        color_discrete_map = {"Positive":"olive", # bar colors
                                              "Negative":"firebrick",
                                              "Neutral":"cornflowerblue"},
                        labels = {'Year': 'Years', # x axis name
                                  'Sentiment_obtained_percentage': 'Percentage of tweets'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Percentage of negative, positive, and neutral tweets each year').update_xaxes(type='category') # plot title

        
        figure_2.update_layout(title=dict(x=0.5, font=dict(family="helvetica", size=25)), # size, font, center title
                               showlegend=False) # hide legend
        
        
        return html.Div([ # html components
                         html.Br(), # add space
                         # Return both graphs
                         dcc.Graph(id="graph_subtab8", figure=figure_2, style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         dcc.Graph(id="graph2_subtab8", figure=figure, style={'display': 'inline-block', "width": "50%", "height": "50%"}) 
                         ])











################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 5 (id 30) SENTIMENTS PER HOUR, DAY, MONTH


# Callback for Tab 3 and subtab 22 General Wordcloud
@app.callback(Output('subtab30', 'children'), # Output 
              [Input('subtabs2', 'value')]) # Input

# Create function to display Wordcloud tab 3 subtab 22
def display_wordcloud(value): 
    
    if value == 'Subtab30':# If we are in subtab 22 in tab 3 we return a graph
        figure = px.line(final_df_by_time, # bar plot
                        x="Rounded_Time", # x axis
                        y='Sentiment_scores_teletravail', # y axis
                        labels = {'Rounded_Time': 'Rounded Time', # x axis name
                                  'Sentiment_scores_teletravail': 'Sentiments\' scores (mean)'}, # y axis name
                        markers=True, # points appearing on the lines
                        template="plotly_dark", # template and categories in x axis
                        title='Tweets\' sentiments by hour (mean)').update_xaxes(type='category') # plot title
        figure.update_layout(title_x=0.5, yaxis_range=[-1,1]) # title position and y axis range
        #figure.update_traces(marker_color='darkblue')
        figure.update_traces(line_color='cornflowerblue') # line color
        #figure.add_shape(type='line',
         #       x0=0,
          #      y0=1,
           #     x1=23,
            #    y1=1,
             #   line=dict(color='Red',))#,
                #xref='x',
                #yref='y'

        
        #figure.update_layout(title=dict(x=0.5, font=dict(family="helvetica", size=50)))
        
        # Second plot
        figure2 = px.line(final_df_day, # line plot
                          x="Days", # x axis
                          y='Sentiment_scores_teletravail', # y axis
                          labels = {'Days': 'Days', # axis title
                                    'Sentiment_scores_teletravail': 'Sentiments\' scores (mean)'}, # axis title
                          markers=True, # points appearing on the line
                          template="plotly_dark", # template
                          #category_orders={"Days": ["Monday", "Tuesday", "Wednesday", "Thursday", # orders for the categories in x axis
                           #                          "Friday", "Saturday", "Sunday"]},
                          title='Tweets\' sentiments by day (mean)') # plot title
        figure2.update_layout(title_x=0.5, yaxis_range=[-1,1])  # title position
        #figure2.update_traces(marker_color='darkblue')
        figure2.update_traces(line_color='cornflowerblue') # line color
        
        # Third plot
        figure3 = px.line(final_df_month, # line plot
                          x='Month', # x axis
                          y='Sentiment_scores_teletravail', # y axis
                          labels = {'Month': 'Months', # axis title
                                    'Sentiment_scores_teletravail': 'Sentiments\' scores (mean)'}, # axis title
                          markers=True, # points appearing on the line
                          template="plotly_dark", # template
                          #category_orders={"Month": ["January", "February", "March", "April", # orders for the categories in x axis
                           #                          "May", "June", "July", "August",
                            #                         "September", "October", "November", "December"]},
                          title='Tweets\' sentiments by month (mean)') # plot title
        figure3.update_layout(title_x=0.5, yaxis_range=[-1,1]) # title position
        figure3.update_xaxes(categoryorder='array', categoryarray= ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']) # order for x axis
        #figure3.update_traces(marker_color='darkblue')
        figure3.update_traces(line_color='cornflowerblue') #  line color


        
        return html.Div([ # html components
                         html.Br(), # add space
                         # We return the plots and a text with dcc markdown
                         dcc.Graph(id="graph_subtab2", figure=figure, style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         dcc.Graph(id="graph2_subtab2", figure=figure2, style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         #html.Br(),
                         dcc.Graph(id="graph3_subtab2", figure=figure3, style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         dcc.Markdown('''The **sentiment analysis** was realized with textblob library.
                                                The sentiment's scores are included between **-1 and +1**.
                                                Normally, if the value is **equals to 0** the sentiment is considered as **neutral**, 
                                                if it's between **-1 and 0** it's considered as **negative**, and if it's between **0 and +1** 
                                                it's considered as **positive**. Here, we **adjusted** the values so that **positive** tweets are 
                                                between **0.30 and +1**, **negative** tweets are between **-1 and 0.30**, 
                                                and **neutral** tweets are **equal to 0.3** (it was more appropriate).''',
                                                style={"width": "40%", "height": "50%",'textAlign': 'justify','marginLeft': 40,
                                                       'display': 'inline-block','vertical-align':'top', 'color':'rgb(166,86,40)'}
                                                #style={'display': 'inline-block',"width": "40%",'textAlign': 'justify','marginLeft': 40,
                                                 #      'align':"center"
                                                       )])


   


    
################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 6 (id 31) NUMBER AND SENTIMENTS DURING WEEK VS WEEKEND


# Callback for Tab 2 Subtab 5 Number and percentage of tweets positive, negative, neutral
@app.callback(Output('subtab31', 'children'), # Output 
              [Input('subtabs2', 'value')]) # Input

# Function to create graph and radioitem (choose type of graph bar plot or pie chart)
def radioitem_subtab5(value): 
    if value == 'Subtab31': # If we are in subtab 5 Tab 2 we return a graph and radioitems

        return html.Div([ # html components
                         html.Br(), # add space
                         html.P("Choose the type of graph that you want to see", # Sentence above the radio item
                                style={'textAlign': 'center','font-size':'100','font-family':'helvetica'}),  # Style of the sentence
                         
                         dcc.RadioItems(id='radio_items_type_graph_week', # dcc.RadioItems allows to create radio items selection
                                        options=[ # Definition of the options  within the radio items
                                                 {'label': 'Pie chart', 'value':'pie'}, # First radio item with label Pie chart
                                                 {'label': 'Bar chart', 'value': 'bar'}], # Second radio item with label Bar chart
                                        value='bar', # Default value when we open the app/ sub tab (bar chart)
                                        style = {'text-align': 'center'},
                                        className="dbc"), # Style of radio items
                         html.Br(),
                         dcc.Graph(id="first_graph_week", style={'display': 'inline-block', "width": "50%", "height": "50%"}), # First  graph
                         dcc.Graph(id="second_graph_week", style={'display': 'inline-block', "width": "50%", "height": "50%"}), # Second graph
                         #html.Br(),
                         dcc.Graph(id="third_graph_week", style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         dcc.Graph(id="fourth_graph_week", style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         #html.Br(),
                         dcc.Graph(id="fifth_graph_week", style={'display': 'inline-block', "width": "50%", "height": "50%"}),
                         dcc.Graph(id="sixth_graph_week", style={'display': 'inline-block', "width": "50%", "height": "50%"})

                         # inline block to have graphs side by side
                         ])
                     


# Callback for Tab 2 Subtab 5 Number and percentage of tweets positive, negative, neutral
@app.callback(
    [Output("first_graph_week", "figure"), # Outputs
     Output("second_graph_week", "figure"),
     Output("third_graph_week", "figure"),
     Output("fourth_graph_week", "figure"),
     Output("fifth_graph_week", "figure"),
     Output("sixth_graph_week", "figure")],
    [Input('radio_items_type_graph_week', 'value')]) # Another input is the selection of the radio item (pie chart or bar chart)


# Function to update the graph depending on radio item selection (pie or bar chart) for tab 2 subtab 5
def update_graph_subtab5(radio_items):

     
    if radio_items == 'pie': # If the user chooses the radio item pie we return a pie chart
    
        # pie chart for percentage of tweets (positive, negative, neutral)
        fig20 = px.pie(df_nb_tweets, values=df_nb_tweets['Percentage_tweets'], # create pie chart
                     names=df_nb_tweets['When'], # pie values
                     title='Percentage of tweets during week vs weekend', # graph title
                     template="plotly_dark") # template
        
        #fig.update_traces(marker=dict(colors=['red', 'blue', 'green']))
        fig20.update_layout(showlegend=False, title_x=0.5) # hide legend and center title
        fig20.update_traces(marker=dict(colors=['cornflowerblue', 'olive'])) # pie colors
        
        # pie chart for number of tweets (positive, negative, neutral)
        fig21 = px.pie(df_nb_tweets, values=df_nb_tweets['Number_tweets'], # create second pie chart
                     names=df_nb_tweets['When'], # pie values
                     title='Number of tweets during week vs weekend', # graph title
                     template="plotly_dark") # template
        
        fig21.update_traces(textinfo='value')#,marker=dict(colors=['red', 'blue', 'green'])) # allows to add numbers (values) instead of percentage
        fig21.update_layout(title_x=0.5) # center graph title
        fig21.update_traces(marker=dict(colors=['cornflowerblue', 'olive'])) # pie colors
        
        
        # Sentiments week vs weekend number and percentage
        
        # pie chart for percentage of tweets (positive, negative, neutral)
        fig22 = px.pie(df_nb_tweets_per_week, values=df_nb_tweets_per_week['Sentiment_obtained_percentage'], # create pie chart
                     names=df_nb_tweets_per_week['Sentiment_obtained_teletravail'], # pie values
                     title='Percentage of tweets considered as negative, positive and neutral during the week', # graph title
                     template="plotly_dark", # template
                     hole=.3) # hole size
        
        fig22.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # pie colors
        fig22.update_layout(showlegend=False, title_x=0.5, annotations=[dict(text='Week', x=0, y=0.50, font_size=15, showarrow=False)]) # hide legend and center title
        
        
        
        # pie chart for number of tweets (positive, negative, neutral)
        fig23 = px.pie(df_nb_tweets_per_week, values=df_nb_tweets_per_week['Tweet'], # create second pie chart
                     names=df_nb_tweets_per_week['Sentiment_obtained_teletravail'], # pie values
                     title='Number of tweets considered as negative, positive and neutral during the week', # graph title
                     template="plotly_dark", # template
                     hole=.3) # hole size
        
        fig23.update_traces(textinfo='value',marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # allows to add numbers (values) instead of percentage
        fig23.update_layout(title_x=0.5, annotations=[dict(text='Week', x=0, y=0.50, font_size=15, showarrow=False)]) # center graph title
        
        
        
        
        ####### WEEKEND
        
        # pie chart for percentage of tweets (positive, negative, neutral)
        fig24 = px.pie(df_nb_tweets_per_weekend, values=df_nb_tweets_per_weekend['Sentiment_obtained_percentage'], # create pie chart
                     names=df_nb_tweets_per_weekend['Sentiment_obtained_teletravail'], # pie values
                     title='Percentage of tweets considered as negative, positive and neutral during the weekend', # graph title
                     template="plotly_dark", # template
                     hole=.3) # hole size
        
        fig24.update_traces(marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # pie colors
        fig24.update_layout(showlegend=False, title_x=0.5,annotations=[dict(text='Weekend', x=0, y=0.50, font_size=15, showarrow=False)]) # hide legend and center title
        
        
        
        # pie chart for number of tweets (positive, negative, neutral)
        fig25 = px.pie(df_nb_tweets_per_weekend, values=df_nb_tweets_per_weekend['Tweet'], # create second pie chart
                     names=df_nb_tweets_per_weekend['Sentiment_obtained_teletravail'], # pie values
                     title='Number of tweets considered as negative, positive and neutral during the weekend', # graph title
                     template="plotly_dark", # template
                     hole=.3) # hole size
        
        fig25.update_traces(textinfo='value',marker=dict(colors=['firebrick', 'cornflowerblue', 'olive'])) # allows to add numbers (values) instead of percentage
        fig25.update_layout(title_x=0.5,annotations=[dict(text='Weekend', x=0, y=0.50, font_size=15, showarrow=False)]) # center graph title
        
        
            
        return fig20, fig21, fig22, fig23, fig24, fig25 # We return the different plots
        
        

    

    if radio_items == 'bar': # If the user chooses the radio item bar we return a bar chart
    
        fig26 = px.bar(df_nb_tweets, x="When", # create bar chart (percentage)
                      y="Percentage_tweets", # y axis
                      color="When", # bar values
                      #color_discrete_map = {"Positive":"green",
                       #                     "Negative":"red",
                        #                    "Neutral":"blue"},
                      template="plotly_dark", # template
                      color_discrete_map = {"Week":"cornflowerblue", # bar colors
                                            "Weekend":"olive"
                                            },) # template
        
        fig26.update_layout(
            title_text='Percentage of tweets during week vs weekend', # Graph title
            title_x=0.5, # center title
            xaxis_title_text='Time', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Time"), # legend title
            showlegend=False # hide legend
            )
        
  
    
        fig27 = px.bar(df_nb_tweets, x="When", # create second bar chart (number))
                      y="Number_tweets", # y axis
                      color="When",
                      #color="Sentiment_obtained_teletravail",
                      #color_discrete_map = {"Positive":"green",
                       #                     "Negative":"red",
                        #                    "Neutral":"blue"},
                      template="plotly_dark",
                      color_discrete_map = {"Week":"cornflowerblue",
                                            "Weekend":"olive"
                                            }) # template
        fig27.update_layout(
            title_text='Number of tweets during week vs weekend', # Plot title
            title_x=0.5, # center title
            xaxis_title_text='Time', # x axis label
            yaxis_title_text='Number of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Time") # Legend title
            )
        
        
        
        
        ############### Number and percentage sentiments during week vs weekend
        
        ######## WEEK
        fig28 = px.bar(df_nb_tweets_per_week, x="Sentiment_obtained_teletravail", # create bar chart (percentage)
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained_teletravail", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        
        fig28.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral during the week', # Graph title
            title_x=0.5, # center title
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments"), # legend title
            showlegend=False # hide legend
            )
  
    
        fig29 = px.bar(df_nb_tweets_per_week, x="Sentiment_obtained_teletravail", # create second bar chart (number))
                      y="Tweet", # y axis
                      color="Sentiment_obtained_teletravail", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        fig29.update_layout(
            title_text='Number of tweets considered as negative, positive and neutral during the week', # Plot title
            title_x=0.5, # center title
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Number of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments") # Legend title
            )
        
        
        
        ######## WEEKEND
        fig30 = px.bar(df_nb_tweets_per_weekend, x="Sentiment_obtained_teletravail", # create bar chart (percentage)
                      y="Sentiment_obtained_percentage", # y axis
                      color="Sentiment_obtained_teletravail", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        
        fig30.update_layout(
            title_text='Percentage of tweets considered as negative, positive and neutral during the weekend', # Graph title
            title_x=0.5, # center title
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Percentage of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments"), # legend title
            showlegend=False # hide legend
            )
  
    
        fig31 = px.bar(df_nb_tweets_per_weekend, x="Sentiment_obtained_teletravail", # create second bar chart (number))
                      y="Tweet", # y axis
                      color="Sentiment_obtained_teletravail", # bar values
                      color_discrete_map = {"Positive":"olive", # bar colors
                                            "Negative":"firebrick",
                                            "Neutral":"cornflowerblue"},
                      template="plotly_dark") # template
        fig31.update_layout(
            title_text='Number of tweets considered as negative, positive and neutral during the weekend', # Plot title
            title_x=0.5, # center title
            xaxis_title_text='Tweets\' sentiments', # x axis label
            yaxis_title_text='Number of tweets', # y axis label
            bargap=0.2, 
            bargroupgap=0.1,
            legend=dict(title="Sentiments") # Legend title
            )
        
        return fig26, fig27, fig28, fig29, fig30, fig31 # We return the plots










###################################################################################################################################################





################################################# TAB 3 WORDCLOUDS



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 1 (id 22) GENERAL WORDCLOUD

# Callback for Tab 3 and subtab 22 General Wordcloud
@app.callback(Output('subtab22', 'children'), # Output 
              [Input('subtabs21', 'value')]) # Input

# Create function to display Wordcloud tab 3 subtab 22
def display_wordcloud(value): 
    
    if value == 'Subtab22':# If we are in subtab 22 in tab 3 we return a wordcloud
        return html.Div(children=[
            html.Br(), # add space
            # We return a title with dcc markdown
            dcc.Markdown('''
                         ### Wordcloud representing the most used words in the collected tweets
                         ''',style= {'color':'white', 'font-family':'helvetica', "text-align":"center"}),
            #html.H3(
             #   children="Wordcloud representing the most used words in the collected tweets",
              #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
               # ),
            html.Br(),
            html.Div(children=
                     # We return the wordcloud image with html.Img
                     [html.Img(src="data:image/png;base64," + general_wordcloud)],
                     style={"text-align":"center"}
            )])



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 2 (id 23) POSITIVE WORDCLOUD 


# Callback for Tab 3 and subtab 23 Positive Wordcloud
@app.callback(Output('subtab23', 'children'), # Output 
              [Input('subtabs21', 'value')]) # Input

# Create function to display Wordcloud tab 3 subtab 22
def display_wordcloud(value): 
    
    if value == 'Subtab23':# If we are in subtab 22 in tab 3 we return a wordcloud
        return html.Div(children=[
            html.Br(),
            # Wordcloud title 
            dcc.Markdown('''
                         ### Wordcloud representing the most used words in the positive tweets
                         ''',style= {'color':'white', 'font-family':'helvetica', "text-align":"center"}),
            #html.H3(
             #   children="Wordcloud representing the most used words in the positive tweets",
              #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
               # ),
            html.Br(),
            html.Div(children=
                     # We return the wordcloud image with html.Img
                     [html.Img(src="data:image/png;base64," + positive_wordcloud)],
                     style={"text-align":"center"}
            )])
    
    



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 3 (id 24) NEGATIVE WORDCLOUD

# Callback for Tab 3 and subtab 24 Negative Wordcloud
@app.callback(Output('subtab24', 'children'), # Output 
              [Input('subtabs21', 'value')]) # Input

# Create function to display Wordcloud tab 3 
def display_wordcloud(value): 
    
    if value == 'Subtab24':# 
        return html.Div(children=[
            html.Br(),
            # Wordcloud title
            dcc.Markdown('''
                         ### Wordcloud representing the most used words in the negative tweets
                         ''',style= {'color':'white', 'font-family':'helvetica', "text-align":"center"}),
            #html.H3(
             #   children="Wordcloud representing the most used words in the negative tweets",
              #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
               # ),
            html.Br(),
            # We return the wordcloud image with html.Img
            html.Div(children=
                     [html.Img(src="data:image/png;base64," + negative_wordcloud)],
                     style={"text-align":"center"}
            )])







################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 4 (id 25) NEUTRAL WORDCLOUD

# Callback for Tab 3 and subtab 25 Neutral Wordcloud
@app.callback(Output('subtab25', 'children'), # Output 
              [Input('subtabs21', 'value')]) # Input

# Create function to display Wordcloud tab 3 subtab 22
def display_wordcloud(value): 
    
    if value == 'Subtab25':
        return html.Div(children=[
            html.Br(),
            # Wordcloud title
            dcc.Markdown('''
                         ### Wordcloud representing the most used words in the neutral tweets
                         ''',style= {'color':'white', 'font-family':'helvetica', "text-align":"center"}),
            #html.H3(
             #   children="Wordcloud representing the most used words in the neutral tweets",
              #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
               # ),
            html.Br(),
            # We return wordcloud image with html.Img
            html.Div(children=
                     [html.Img(src="data:image/png;base64," + neutral_wordcloud)],
                     style={"text-align":"center"}
            )])


    
    

################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 5 (id 26) WORDCLOUD TABLE

# Callback for Tab 3 and subtab 26
@app.callback(Output('subtab26', 'children'), # Output 
              [Input('subtabs21', 'value')]) # Input

# Create function to display table and text
def display_wordcloud(value): 
    
    if value == 'Subtab26':
        return html.Div([

            html.Div([
                html.Br(),
                # Wordcloud table title
                dcc.Markdown('''
                             ### Frequencies of the most used words in all type of tweets (positive, negative, neutral)
                             ''',style= {'color':'rgb(166,86,40)', 'font-family':'helvetica', "text-align":"center"}),
                             
                #html.H3(children="Frequencies of the most used words in all type of tweets (positive, negative, neutral, all)",
                 #             style={'font-family':'helvetica'} # 'fontSize':'44px',)
                              
                                ]),#style={"text-align":"center"}
            
            
                

            html.Div([
                
                html.Div([html.Br(),
                          # We create the table and its colors depending on the values
                dash_table.DataTable(wordclouds_table.to_dict('records'), [{"name": i, "id": i} for i in wordclouds_table.columns],
                                     style_data_conditional=[
                                         # If values smaller than 100 white color in the background
                                         # If values between 100 and 400 orange color in the background (darkorange)
                                         # If values higher than 400 red color in the background (firebrick)
                                         # Words_positive column
                                         {'if': {'filter_query' : '{Words_Positive} <= 100',
                                                 'column_id':'Words_Positive'
                                                 },
                                          'background-color': 'white',
                                          'color':'black'
                                          },
                                        
                                        {'if': {'filter_query' : '{Words_Positive} > 100',
                                                'column_id':'Words_Positive'
                                                },
                                         'background-color': 'darkorange',
                                         'color':'black'
                                         },
                                        
                                        {'if': {'filter_query' : '{Words_Positive} > 400',
                                                'column_id':'Words_Positive'
                                                },
                                         'background-color': 'firebrick',
                                         'color':'black'
                                         },
                                        
                                        
                                        # Words_negative column
                                     
                                        {'if': {'filter_query' : '{Words_Negative} <= 100',
                                                'column_id':'Words_Negative'
                                                },
                                         'background-color': 'white',
                                         'color':'black'
                                         },
                                        
                                        {'if': {'filter_query' : '{Words_Negative} > 100',
                                                'column_id':'Words_Negative'
                                                },
                                         'background-color': 'darkorange',
                                         'color':'black'
                                         },
                                        
                                        {'if': {'filter_query' : '{Words_Negative} > 400',
                                                'column_id':'Words_Negative'
                                                },
                                         'background-color': 'firebrick',
                                         'color':'black'
                                         },
                                        
                                        
                                        # Words_neutral column
                                        
                                        {'if': {'filter_query' : '{Words_Neutral} <= 100',
                                                'column_id':'Words_Neutral'
                                                },
                                         'background-color': 'white',
                                         'color':'black'
                                         },
                                        
                                        {'if': {'filter_query' : '{Words_Neutral} > 100',
                                                'column_id':'Words_Neutral'
                                                },
                                         'background-color': 'darkorange',
                                         'color':'black'
                                         },
                                        
                                        {'if': {'filter_query' : '{Words_Neutral} > 400',
                                                'column_id':'Words_Neutral'
                                                },
                                         'background-color': 'firebrick',
                                         'color':'black'
                                         }],
                                    
                                     fill_width=False # Reduce width of columns,
                                     
                                     
                                     )], style={'display': 'inline-block', "width": "50%", "margin-left": "100px"}),

    
    
            # Text to add next to table
            html.Div([html.Br(), dcc.Markdown('''
                                   If the background color is **white**, it means that the value is **smaller or equal to 100**. 
                                   When it's **orange**, the value is **between 100 and 400**. If the background color is **red**, the value is 
                                   **higher than 400**. We see that most of those words are spread in all type of tweets (except for the word 
                                   "heureux/happiness" which isn't in neutral tweets). Nonetheless, we notice that some **words related to 
                                   health** are more found in **negative tweets** such as "fatigue/tiredness", "risque/risk", 
                                   "dépression/depression", "déprime/bout of depression", "anxiété/anxiety", "santé/health", 
                                   "mal/ache ", "stress". In addition, words like "télétravail/teleworking", "confinement/lockdown", 
                                   "trajet/ride", "enfant/child", "seul/lonely", "voiture/car", "covid", "travailler/to work", are also 
                                   found a lot of times in negative tweets. Hence, the **previous words** are mostly **associated with bad, 
                                   negative feelings**. We observe a few **health consequences** that we have seen in the **literature**, especially 
                                   **psychological problems** (tiredness, depression, anxiety, stress). On a smaller scale, there are **other 
                                   health impacts** that people talked about in the negative tweets such as ache, burnout, problem, 
                                   exhausted, overload, sick, crisis, fear. Furthermore, we see that **people felt negative about several 
                                   subjects** like teleworking, lockdown, ride, child, woman, family, train, and they felt lonely, 
                                   exhausted, isolated. Finally, with these words from the different wordclouds, we recognize a lot of 
                                   **similarities with what we have read in the literature**. 
                                   ''', style={'color':'rgb(166,86,40)'})], style={'display': 'inline-block', 'text-align': 'justify', "width": "40%"
                                   })],style={"display": "flex"}) #  #8C564B
                                   ])










    
    
###################################################################################################################################################






################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL


################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 1 LDA MODEL INTERPRETATION

@app.callback(Output('subtab9', 'children'), # Output 
              [Input('subtabs3', 'value')]) # Input

# Create function to display lda model explanation
def display_wordcloud(value): 
    
    if value == 'Subtab9': # if we are in subtab 9
        
        # We return text with dcc markdown
        return html.Div([html.Br(), dcc.Markdown('''
                            ### What is LDA ? 
                            ''',style={'color':'rgb(166,86,40)', 'text-align': 'justify', "width":"70%", 'marginLeft': 250}), # rgb(115,111,76)
                            
                            
                            dcc.Markdown('''

                            **Topic modeling** is a method for **unsupervised classification** of documents. 
                            It helps us to find **groups of items** (topics) even when we’re not sure what we’re looking for. 
                            In our study, the documents are the **tweets** that we have collected. **LDA** is one of the topic modeling methods. 
                            Its purpose is to **find topics a document belongs to**, based on the words in it. Each document is 
                            just a **collection of words** or a “bag of words”. Thus, the **order** of the words and the 
                            **grammatical role** of the words are not considered in the model. Words that don’t carry any 
                            interesting information are eliminated during processings **before creating the LDA model** 
                            (words like of/a/but/am/they,etc), because we know that we won’t lose any information, sense without 
                            them. We have to define the **number of topics** that we would like to have before creating the LDA model. 
                            If a word (w) has **high probability** of being in a theme (t), all the documents having w will 
                            be more strongly associated with t as well. If w is **not very probable** to be in t, 
                            the documents which contain the w will be having very low probability of being in t, 
                            because the rest of the words in d will belong to some other topic and hence, d will have 
                            a **higher** probability for those topics.  
                            
                            ''', style={'color':'rgb(166,86,40)', 'text-align': 'justify', "width":"70%", 'marginLeft': 250}),
                            
                            
                            dcc.Markdown('''
                            
                            ### What we did 
                            ''',style={'color':'rgb(166,86,40)', 'text-align': 'justify', "width":"70%", 'marginLeft': 250}),
                            
                            
                            dcc.Markdown('''

                            Several models have been realized by changing and comparing *different parameters*, 
                            especially the number of passes that is done on the reading of each list of words, 
                            and the number of topics that we would like to have. Those different tests allow us to 
                            compare the different models and observe how many “big” topics seem to appear each time. 
                            When themes are *close*, it means that *they are related* to each other,
                            and when topics are *spread*, it means that they are *different* from each other.  
                            ''',style={'color':'rgb(166,86,40)', 'text-align': 'justify', "width":"70%", 'marginLeft': 250}), # #8C564B
                            
                            dcc.Markdown('''
                            ### What we decided to conclude
                            ''',style={'color':'rgb(166,86,40)', 'text-align': 'justify', "width":"70%", 'marginLeft': 250}),
                            
                            dcc.Markdown('''

                            Among all created models, we could differentiate three big topics each time. That is why we kept an 
                            LDA model with 3 topics, and another one with more topics (5) in order to demonstrate that in the end, 
                            there are mainly 3 approached subjects. Indeed, the Intertopic Distance Map for the LDA Model 5 topics 
                            shows that topics 1 and 3 are close (first big theme), topics 4 and 5 are close (second big theme), 
                            and topic 2 is alone (third big theme). This is our consideration but we can interpret it in a 
                            different way. If we have a look at the Top-30 Most Salient Terms, we have an idea of the different 
                            topics of conversation. For example, it seems that people talked about domicile/home, accident, 
                            télétravail/teleworking, protéger/protect, mobilité/mobility, population, ordi/computer, etc in 
                            the topic number 2 (third big theme in our example).If we have a look at the Top-30 Most Salient Terms 
                            for Topics 5 and 4 (second big theme in our example), we note that conversations dealt with 
                            domicile/home, risque/risk, travail/to work, trajet/ride, télétravail/teleworking, 
                            confinement/lockdown, déplacement/moving, emploi/job, burnout, sécurité/security, dépression/depression,
                            stress, anxiété/anxiety, covid, salarié/employee, santé/health, mental, etc. The last group with 
                            topics 1 and 3 (first big theme in our example) concerns more travail/job, bien/good, 
                            télétravail/teleworking, domicile/home, aime/like, super, semaine/week, enfant/children, etc. 
                            Therefore, it looks that the first big theme  (topics 1 and 3) focuses on thoughts that are more 
                            positive towards teleworking (they seem to like it, they are feeling good, it's super). 
                            The second big theme (topics 4 and 5) seems to be a little bit negative towards teleworking and the 
                            situation because people talked about lockdowns, the risks, but also health consequences such as 
                            stress, burnout, depression, anxiety, and mental problems. Finally, the third big theme (topic 2) 
                            talks more about working and teleworking. 
                            
                            If we have a closer look at the LDA model with 3 topics in total, we find more or less the same 
                            results again. The first theme (topic 1) focuses more on travail/job, domicile/home, bien/good, 
                            trajet/ride, stress, aide/help, lieu/location, distance, vélo/bike, santé/health, etc. 
                            The second theme (topic 2) tends towards risque/risk, télétravail/teleworking, travail/job, covid, 
                            dépression/depression, anxiété/anxiety, domicile/home, stress, salarié/employee, étude/studies, etc. 
                            The last one (topic 3) speaks about travail/job, bien/good, télétravail/teleworking, domicile/home, 
                            aime/like, super, risque/risk, travailler/to work, adore/like, etc. We encounter similarities with 
                            the previous LDA model. Indeed, there is a theme that focuses more on the job, how to travel or go 
                            to work (distance, ride, bike, location). Then there is a theme which seems quite negative 
                            (risk, covid, depression, anxiety, stress). And the third one looks a little bit more positive 
                            (good, like, super, but also risk).
                            
                            Those LDA models depict the different approached subjects in the conversation which could have 
                            shown topics we haven’t thought about. This is not the case here, but at least we know what people 
                            talked about the most.

                            
                            ''', style={'color':'rgb(166,86,40)', 'text-align': 'justify', "width":"70%", 'marginLeft': 250})
                            
                            ])
            




################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 2 LDA MODEL 5 TOPICS

# To make the lda model working we have to create a folder called assets in the computer and put the html lda model file inside
# on the main path (with spyder on the top right) we have to put the path where the folder assets is
# then we can open the lda model with html.Iframe in the dash app


# Callback for Tab 3 and subtab 10 LDA Model 10 topics and 5 passes
@app.callback(Output('subtab10', 'children'), # Output 
              [Input('subtabs3', 'value')]) # Input 

# Create function to display graph (here LDA model done previously and saved on computer)
def display_lda_subtab10(value):
    
    if value == 'Subtab10':# If we are in subtab 10 tab 3 we return a lda model
        
        # We return the corresponding lda model saved on computer in assets folder
        # If app deployed with python anywere path is /home/MarionEtp/mysite/assets/
        # which gives
        #return html.Div([html.Iframe(src="/home/MarionEtp/mysite/assets/lda_model_passes_10_topics_5.html", width=1550,height=600)])#, style=dict(width="100%", height="100%"))])
        
        # If app runs on computer we return lda model like this
        return html.Div([html.Iframe(src="assets/lda_model_passes_10_topics_5.html", width=1550,height=600)])




################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 3 LDA MODEL 3 TOPICS


# To make the lda model working we have to create a folder called assets in the computer and put the html lda model file inside
# on the main path (with spyder on the top right) we have to put the path where the folder assets is
# then we can open the lda model with html.Iframe in the dash app


# Callback for Tab 3 and subtab 11 LDA Model 5 topics and 10 passes
@app.callback(Output('subtab11', 'children'), # Output 
              [Input('subtabs3', 'value')]) # Input 

# Create function to display graph (here LDA model done previously and saved on computer)
def display_lda_subtab11(value): 
    
    if value == 'Subtab11':# If we are in subtab 11 tab 3 we return a graph
        
        # If app deployed path is /home/MarionEtp/mysite/assets/
        # If app on computer , only assets (if path on top right spyder is where the folder assets is)
        return html.Div([html.Iframe(src="assets/lda_model_passes_10_topics_3.html", width=1550,height=600)])#, style=dict(width="100%", height="100%"))])
     










###################################################################################################################################################






 

################################################# TAB 5 NETWORK GRAPH



################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 1 NETWORK BIGRAMS


# Callback for Tab 1 and subtab 3 to display graph and dropdown (list of choices (years))
@app.callback(Output('subtab13', 'children'), # Output 
              [Input('subtabs4', 'value')])  # Input 

# Function to create a dropdown menu and graph
def dropdown_subtab3(value):  
    
    if value == 'Subtab13': # If we are in Subtab13
        
        fig_bigram = px.bar(bigram_df_teletravail, # bar plot
                        x="count", # x axis
                        y='bigram', # y axis
                        orientation='h', # horizontal bars
                        color='count', # bar values
                        labels = {'bigram': 'Bigrams', # x axis name
                                  'count': 'Number of times each bigram appears'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of times that each bigram occurs', # plot title
                        color_continuous_scale=px.colors.sequential.RdBu # bar colors
                        ).update_xaxes(type='category')
        
        
        fig_bigram.update_layout(title=dict(x=0.5),height=800, font=dict(size=15))# title position
                             #plot_bgcolor='rgba(0, 0, 0, 0)',paper_bgcolor='rgba(0, 0, 0, 0)') # font=dict(family="helvetica", size=50, color='white')
        

        return html.Div(children=[
            
            html.Br(),
            # Network title
            dcc.Markdown(''' ### Network Graph showing the 40 most common bigrams
                         ''',style={'color':'white', 'font-family':'helvetica', "text-align":"center"}), # title style
            #html.H3(
             #   children="Network Graph showing the 40 most common bigrams",
              #  style={'font-family':'helvetica', "text-align":"center"} # "background-color" : "black", "fontSize": "44px", 
               # ),
            
            html.Br(),
            html.Div(children=
                     # Network image with html.Img
                     [html.Img(src='data:image/png;base64,{}'.format(network_bigram),width=800, height=500),
                      html.Br(),
                      html.Br(),
                      dcc.Graph(id="graph_bigram", figure=fig_bigram, style={'width':'80%', 'marginLeft': 150})],
                      style={"text-align":"center"} # "background-color" : "black",'plot_bgcolor':'grey'
                      )])

 
    
    

################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 2 NETWORK TRIGRAMS

# Callback for Tab 1 and subtab 3 to display graph and dropdown (list of choices (years))
@app.callback(Output('subtab14', 'children'), # Output 
              [Input('subtabs4', 'value')])  # Input 

# Function to create a dropdown menu and graph
def dropdown_subtab3(value):  
    
    if value == 'Subtab14': # If we are in Subtab14
        
        fig_trigram = px.bar(trigram_df_teletravail, # bar plot
                        x="count", # x axis
                        y='trigram', # y axis
                        orientation='h', # horizontal bars
                        color='count', # bar values
                        labels = {'trigram': 'Trigrams', # x axis name
                                  'count': 'Number of times each threegram appears'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of times that each threegram occurs', # plot title
                        color_continuous_scale=px.colors.sequential.RdBu # bar colors
                        ).update_xaxes(type='category')
        
        fig_trigram.update_layout(title=dict(x=0.5),height=800, font=dict(size=15))# title position
                             #plot_bgcolor='rgba(0, 0, 0, 0)',paper_bgcolor='rgba(0, 0, 0, 0)') # font=dict(family="helvetica", size=50, color='white')




        
        return html.Div(children=[
            
            html.Br(),
            # Network title
            dcc.Markdown(''' ### Network Graph showing the 40 most common threegrams
                         ''',style={'color':'white', 'font-family':'helvetica', "text-align":"center"}),
                         
           #html.H3(
            #    children="Network Graph showing the 40 most common threegrams",
             #   style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
              #  ),
            html.Br(),
            html.Div(children=
                     # Network image with html.Img
                     [html.Img(src='data:image/png;base64,{}'.format(network_trigram),width=800, height=500),
                      html.Br(),
                      html.Br(),
                      dcc.Graph(id="graph_trigram", figure=fig_trigram, style={'width':'80%', 'marginLeft': 150})],
                      style={"text-align":"center"}
                      )])


            

################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 3 NETWORK FOURGRAMS

# Callback for Tab 1 and subtab 3 to display graph and dropdown (list of choices (years))
@app.callback(Output('subtab15', 'children'), # Output 
              [Input('subtabs4', 'value')])  # Input  

# Function to create a dropdown menu and graph
def dropdown_subtab3(value):  
    
    if value == 'Subtab15': # If we are in Subtab15
        
        fig_fourgram = px.bar(fourgram_df_teletravail, # bar plot
                        x="count", # x axis
                        y='fourgram', # y axis
                        orientation='h', # horizontal bars
                        color='count', # bar values
                        labels = {'fourgram': 'Fourgrams', # x axis name
                                  'count': 'Number of times each fourgram appears'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of times that each fourgram occurs', # plot title
                        color_continuous_scale=px.colors.sequential.RdBu # bar colors
                        ).update_xaxes(type='category')
        
        fig_fourgram.update_layout(title=dict(x=0.5),height=800, font=dict(size=15))# title position
                             #plot_bgcolor='rgba(0, 0, 0, 0)',paper_bgcolor='rgba(0, 0, 0, 0)') # font=dict(family="helvetica", size=50, color='white')
        
        return html.Div(children=[
            
            html.Br(),
            # Network title
            dcc.Markdown(''' ### Network Graph showing the 40 most common fourgrams
                         ''',style={'color':'white', 'font-family':'helvetica', "text-align":"center"}),
                         
            #html.H3(
             #   children="Network Graph showing the 40 most common fourgrams",
              #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
               # ),
            html.Br(),
            html.Div(children=
                     # Wordcloud image with html.Img
                     [html.Img(src='data:image/png;base64,{}'.format(network_fourgram),width=800, height=500),
                      html.Br(),
                      html.Br(),
                      dcc.Graph(id="graph_fourgram", figure=fig_fourgram, style={'width':'80%', 'marginLeft': 150})],
                      style={"text-align":"center"}
                      )])    






################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 4 INTERACTIVE NETWORK 

@app.callback(Output('subtab16', 'children'), # Output 
              [Input('subtabs4', 'value')]) # Input 

# Create function to display graph in subtab 2 of tab 1 Number of collected tweets
def display_network_bigrams(value): 
    
    if value == 'Subtab16': # if we are in subtab 13 we return the bigram network graph
        
        fig_fifthgram = px.bar(fifthgram_df_teletravail, # bar plot
                        x="count", # x axis
                        y='trigram', # y axis
                        orientation='h', # horizontal bars
                        color='count', # bar values
                        labels = {'trigram': 'Fifthgrams', # x axis name
                                  'count': 'Number of times each fifthgram appears'}, # y axis name
                        template="plotly_dark", # template and categories in x axis
                        title='Number of times that each fifthgram occurs',
                        color_continuous_scale=px.colors.sequential.RdBu # bar colors
                        ).update_xaxes(type='category')
        
        fig_fifthgram.update_layout(title=dict(x=0.5),height=800, font=dict(size=15))# title position
                             #plot_bgcolor='rgba(0, 0, 0, 0)',paper_bgcolor='rgba(0, 0, 0, 0)') #  font=dict(family="helvetica", size=50, color='white')
        


        return html.Div([html.Br(), 
                         # Network title
                         dcc.Markdown(''' ### Network Graph showing the 40 most common fifthgrams
                                      ''',style={'color':'white', 'font-family':'helvetica', "text-align":"center"}),
                         #html.H3(
                          #   children="Network Graph showing the 40 most common fifthgrams",
                           #  style={'font-family':'helvetica', "text-align":"center"} # "fontSize": "44px", 
                            # ),
                         html.Br(),
                         # Create interactive network graph with cytoscape
                         cyto.Cytoscape(id='networkgraph', # graph id (compulsory)
                                        # layout = breadthfirst means tree structure
                                        layout={'name': 'breadthfirst'}, #appearance of graph (could be circle, grid, random, preset which means we assign position of the nodes ourselves, etc)
                                        style={'width': '80%', 'height': '500px', 'background-color': 'white', 'marginLeft': 150},  
                                        elements=[
                                            
                                            #nodes
                                            # id to use node in the code
                                            # label is the name that appears on the network graph
                                            # we can add position to define x and y coordinates of labels (here we don't do it because lots of words/labels)
                                            # locked to be able to move the node or not
                                            # grabbable to be able to grab the node or not
                                            # selectable to be able to select the node or not (appears blue for example when we click on it)
                                            # selected : the node appears automatically blue without clicking on it
                                            # classes to style the graph (color, shape etc)
                                            {'data': {'id': x, 'label': x, 'locked': False, 'grabbable': True, 'selectable': True}} for x in network_graph_fifth.nodes()

                                            ]
                                        
                                        +
                                        
                                        [
                                            
                                            #edges
                                            # source where to start the link
                                            # target where to end/join the link from the source
                                            # here source is the first first in the bigram and target the second word of the same bigram etc for all bigrams
                                            # label if we want to name the edges/links
                                            {'data': {'source': y, 'target': z}} for y,z in network_graph_fifth.edges()
                                            
                                            ]
            
                                        #autoungrabify=True,
                                        #minZoom=0.2,
                                        #maxZoom=1,
                                        
                                        
                                        

                                        ),
                         html.Br(),
                         dcc.Markdown(id='selected_node_markdown'),
                         html.Br(),
                         dcc.Graph(id="graph_fifthgram", figure=fig_fifthgram, style={'width':'80%', 'marginLeft': 150})
                         ])
    
    
    
# Call back for tab 4 Subtab 13 selected node markdown on the network graph
@app.callback(Output('selected_node_markdown', 'children'), # Output 
              [Input('networkgraph', 'selectedNodeData') # Input
              ]) 
   
# Create function to display text when user selects a node
def display_selectednode_data(selected_node):
    
    if selected_node is None:
        return "No node/word selected"
    
    words_list = [data['label'] for data in selected_node]
    
    return "You selected the following node: " + "\n* ".join(words_list)



# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)  