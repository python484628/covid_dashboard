# Covid_app

As part of my Master Degree called Heath Data Science, we had to realize a Master thesis. Firstly, the aim of this study was to identify the health consequences due to teleworking during the COVID-19 pandemic. Secondly, the objective was to compare those health
impacts between men and women. To that end, we used web scraping to retrieve natural language data coming from Twitter. We succeeded in collecting the tweets’ contents, dates, times, users, languages, while the tweets’ coordinates and tweets’ location were rarely
recovered. This web scraping has been done between January 2019 and September 2022 using specific key-words within queries. We performed several analyses and tests (cleaning and exploring data, sentiment analysis, word cloud analysis, LDA analysis, network
analysis, chi-squared tests), but also different visualizations (bar charts, pie charts, tabs, Intertopic Distance Map, Top-30 Most Salient Terms, network graphs, word clouds). Finally, we created a dashboard to show the graphs and results, and we used IRaMuTeQ software to complete our interpretations. To conclude, we found a lot of similarities with the literature (health consequences due to teleworking, especially psychological and mental ones, but also negative thoughts and opinions about teleworking).

You can have a look at the app here : https://covid-app-vbq1.onrender.com/ 

If you want to run the app on your computer, you will need the different csv files and the code app_computer.py. Of course, you'll have to adapt your path. The file app.py is the code used with dash render to access the app directly without runing the code on your computer.

If you want to look how the different processings were made before creating the dashboard you can have a look at the other codes provided : 

- LDA_code.py which allows to create the LDA models (you'll need final_df_cleaned_python.csv)

- clean_tweets_working.py which shows the different processings made to clean the collected tweets (you'll need final_df_teletravail.csv, final_df_maison.csv, final_df_domicile.csv)

- processes_before_dash_app.py which shows the different processings made after collecting the tweets (sentiment analysis, wordclouds, create tables needed for the dashboard, network graphs) (you'll need final_df_cleaned_python.csv, docs_teletravail_list.npy)

- scrape_tweets_working.py which allows to collect tweets on Twitter with specific key words







Author : Marion Estoup 

E-mail : marion_110@hotmail.fr

Between 2021 and 2023

The project was supervised by Djamel Zitouni and Perrine Hanicotte-Zitouni.


