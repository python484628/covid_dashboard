# Author : Marion Estoup
# Email : marion_110@hotmail.fr
# Date November 2022


############################################## LDA LATENT DIRICHLET ALLOCATION


 
# Help with : https://curiousml.github.io/teaching/DSA/dsa_nlp_tp_corr.html
# LDA is a model that allows to do topic modeling
# Topic modeling is used to discover the different topics that are in our data
# Topic modeling doesn't create a title for each topics. It's up to us to create a title for each topic found
# For example it will find the words/topics "player", "game", "ball" and it's up to us to create the title soccer
# It's a non supervized approach



### Load libraries

# LDA analysis
import spacy
#from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel

# Vizualization (LDA)
import pyLDAvis.gensim_models
#import warnings
#import importlib

# Data manipulation, data cleaning
import pandas as pd


#################### PATHS

path_to_import = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/Final codes and files/Clean_data/final_df_cleaned/"
path_to_export = 

#################### OPEN FILE AND CREATE LIST DOCS_TELETRAVAIL

### Open file 

# Open final_df (cleaned tweets collected)
final_df = pd.read_csv(path_to_import+ 'final_df_cleaned_python.csv', sep='\t', index_col=0)

# explore data 
final_df.columns # columns User, Tweet, Lanugage, Tweet_coordinates, Tweet_place, etc
final_df.dtypes # all columns have dtype object





### Clean and vectorize the documents to create list docs_teletravail

# Before training LDA model we have to tokenize our text
# We tokenize with spaCy library
# We load a pipeline trained for french language
# We store this pipeline in nlp_teletravail
nlp_teletravail = spacy.load("fr_core_news_sm")


# We have to define words that we want to keep (the ones that we want to remove from the default stopwords):
# We can keep "pas", "ça", "suffit", "tellement", "différents", "differents", "sans", "seule", 'moins', 'effet', 'longtemps',
# 'stop', 'suffisant', 'nul', 'sent', 'très', 'seul', 'tres', 'différentes', 'differentes', 'differents', 'different',
# 'seules', 'seuls'
# because they are important in our analysis



# Remove some words from the default stopwords list (in order to keep them in our analysis)
nlp_teletravail.vocab["pas"].is_stop = False
nlp_teletravail.vocab["ça"].is_stop = False
nlp_teletravail.vocab["suffit"].is_stop = False
nlp_teletravail.vocab["tellement"].is_stop = False
nlp_teletravail.vocab["différents"].is_stop = False
nlp_teletravail.vocab["differents"].is_stop = False
nlp_teletravail.vocab["sans"].is_stop = False
nlp_teletravail.vocab["seule"].is_stop = False
nlp_teletravail.vocab["moins"].is_stop = False
nlp_teletravail.vocab["effet"].is_stop = False
nlp_teletravail.vocab["longtemps"].is_stop = False
nlp_teletravail.vocab["stop"].is_stop = False
nlp_teletravail.vocab["suffisant"].is_stop = False
nlp_teletravail.vocab["nul"].is_stop = False
nlp_teletravail.vocab["sent"].is_stop = False
nlp_teletravail.vocab["très"].is_stop = False
nlp_teletravail.vocab["seul"].is_stop = False
nlp_teletravail.vocab["tres"].is_stop = False
nlp_teletravail.vocab["différentes"].is_stop = False
nlp_teletravail.vocab["differentes"].is_stop = False
nlp_teletravail.vocab["différents"].is_stop = False
nlp_teletravail.vocab["differents"].is_stop = False
nlp_teletravail.vocab["seules"].is_stop = False
nlp_teletravail.vocab["seuls"].is_stop = False


# We can add new stopwords which are  not on the default stopwords list (words that we don't want to keep in our analysis)
# We can add "en" which is not in the default list
nlp_teletravail.vocab["en"].is_stop = True



### Create spacy document and list of words docs_teletravail


# Create spacy_docs_teletravail to create spacy document for each tweet
spacy_docs_teletravail = list(nlp_teletravail.pipe(final_df["Preprocessed_tweets"]))


# Now we have a list of documents for each tweet with spacy stored in spacy_docs_teletravail
# We can transform each document in a list of tokens
# We are going to :
    # 1- remove word that have less than 2 letters
    # 2- remove stop words
    # 3- lemmatize the words left
    # 4- put words in lower case
    

# Create empty list called docs_teletravail
docs_teletravail = []

# Pre processing
# For each document in spacy_docs_teletravail
for doc in spacy_docs_teletravail:
    # We create empty list called tokens_teletravail
    tokens_teletravail = []
    # for each token in each document of spacy_docs_teletravail
    for token in doc:
        # pre processing 1 and 2
        if len(token.orth_) > 2 and not token.is_stop:     
            # pre processing 3 and 4
            tokens_teletravail.append( token.lemma_.lower() ) 
            # append results of tokens_teletravail in docs_teletravail
    docs_teletravail.append( tokens_teletravail )







#################### CREATE DICTIONARY BASED ON DOCS_TELETRAVAIL LIST

# Last step of the pre processing specific to Gensim
# We are going to create a dictionnary representation of the documents
# This dictionnary will map each word to a unique id
# It will help us to create bag-of-words of each document
# These bag-of-words contain the id of the words from our dictionnary and their frequency (in number of occurence)
# We can remove words the most and the least frequent from the vocabulary
# It will improve the quality of the model and it will accelerate its training



# Create dictionary_teletravail based on our list docs_teletravail
dictionary_teletravail = Dictionary(docs_teletravail)
print('Nombre de mots unique dans les documents initiaux :', len(dictionary_teletravail))


########################################################################
# If we want to filter words according to their occurence we can do :
#dictionary_teletravail.filter_extremes(no_below=3, no_above=0.25)
#print('Nombre de mots unique dans les documents après avoir enlevé les mots fréquents/peu fréquents :', len(dictionary_teletravail))

# I didn't do it because I think we want to keep all of our words
# Explanation to understand no_below and no_above : https://stackoverflow.com/questions/51634656/filtering-tokens-by-frequency-using-filter-extremes-in-gensim
# And documentation : https://radimrehurek.com/gensim/corpora/dictionary.html#gensim.corpora.dictionary.Dictionary.filter_extremes
########################################################################


# Let's have a look at the id that we have created for the fifth row of our list docs_teletravail for example
print("Exemple :", dictionary_teletravail.doc2bow(docs_teletravail[4]))        


# Let's create the id for all rows (lists) in our list docs_teletravail
# Representation in bag-of-words for each document of the corpus
# doc2bow method is used
# Create corpus_teletravail using doc2bow on dictionary_teletravail for each doc (list) in our list docs_teletravail
corpus_teletravail = [ dictionary_teletravail.doc2bow(doc) for doc in docs_teletravail]







##################################################### Topic modeling with LDA 

# We train the LDA using :
    # corpus_teletravail : the bag-of-words representations of our documents
    # id2token : the index mappings of words
    # num_topics : Number of topics that the model has to identify
    # chunksize : the number of documents that the model has to see at each update (affects memory consumption)
    # passes : number of times where we show the total corpus to the model during the training
    # random_state : seed to make sure of the reproductibility (repeat the same training)
    
# LDA works better if we have a lot of data
# If we have a lot of data we can chose a high num_topics to find
# other parameters of LDA model that I don't use/chose :
    # update_every : update the model each chunk (optimization of memory consumption)
    # alpha : asymetric and auto value : the former uses a fixed normalized asymmetric 1.0/topic no prior and the latter learns an asymmetric prior from your data 
    
    # per_word_topics if True allows the extraction of the topic the most likely according to a word. Each word is affected to a topic
    # phi : value steers this process (of per_word_topics). It's a treshold for a word treated as indicative or not
    

# Create model_teletravail thanks to LdaModel function()
# num_topics to 10
# chunksize to 1000
# passes to 5
# random_state to 1 
model_teletravail = LdaModel(corpus=corpus_teletravail,
                             id2word=dictionary_teletravail,
                             num_topics=10,
                             chunksize=1000,
                             passes=5,
                             random_state=1) 
    





########################################### RESULTS AND VIZUALIZATION OF LDA MODEL

# For topic and words in our model_teletravail created with LDA method
# We print the 10 topics with 10 words for each topic
# There are the 10 words the most characteristic for each topic
for (topic, words) in model_teletravail.print_topics():
    print("***********")
    print("* topic", topic+1, "*")
    print("***********")
    print(topic+1, ":", words)
    print()
    
# Result for topic 3 for example :
# 3 : 0.056*"déprime" + 0.032*"teletravail" + 0.017*"burnout" + 0.014*"pandémie" + 0.012*"déplacer" + 0.010*"isolé" + 
# 0.010*"réaliser" + 0.008*"zone" + 0.007*"tombe" + 0.007*"crise"

# déprime is the most representative, then the word teletravail, then burnout etc
# Topic 3 could be called côté négatif du télétravail or risques du télétravail, etc



## Another way to vizualize our LDA model for teletravail is to use pyLDAvis library to have a visual output
# It shows how much our topics are popular in our corpus, how much they are similar, what are the words the most important for this topic


#pyLDAvis.enable_notebook()
#warnings.filterwarnings("ignore", category=DeprecationWarning) 

# Create vis_data_teletravail the graph with our model model_teletravail, our corpus corpus_teletravail and our dictionary dictionary_teletravail
vis_data_teletravail = pyLDAvis.gensim_models.prepare(model_teletravail, corpus_teletravail, dictionary_teletravail, sort_topics=False)

# Export the graph and store it where we want in our computer
pyLDAvis.save_html(vis_data_teletravail, 'C:/Users/mario/Downloads/lda_model.html')
# Once the graph/file is saved, we can click on it and it will open in our browser and we see:
    # Intertopic Distance Map (viau multidimensional scaling)
    # Top-30 Most Salient Terms
    
# On the first graph Intertopic Distance Map we see that topic 3 and 5 are isolated
# While topics 1,4,7,10 seem related because they are close to each other
# Topics 8 and 2 seem related because they are close to each other
# Topics 6 and 9 seem related because they are close to each other

# On the second  graph Top-30 Most Salient Terms we can see the overall term frequency for each word in all topics
# And we can see the estimated term frequency within the selected topic
# This second graph can be adjusted with lambda value (we can chose a value for lambda between 0 and 1)
# When we go on each word with the mouse, we can also see in which topic those terms are present




# Let's have a look at the topics that the LDA model recognizes in some of our documents (tweets)
# We see the probability for each first documents (the fourth first tweets in this example)
n_doc = 4
i = 0
for (text, doc) in zip(final_df["Preprocessed_tweets"][:n_doc], docs_teletravail[:n_doc]):
    i += 1
    print("***********")
    print("* doc", i, "  *")
    print("***********")
    print(text)
    print([(topic+1, prob) for (topic, prob) in model_teletravail[dictionary_teletravail.doc2bow(doc)] if prob > 0.1])
    print()


