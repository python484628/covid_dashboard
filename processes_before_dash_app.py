# Author : Marion Estoup
# Email : marion_110@hotmail.fr
# Date March 2023


########################################################################### PROCESSINGS BEFORE DASH APP

# Code to scrape tweets --> final files to clean 
# Code to clean and organize tweets --> final files to open and use here 
# Code to process tweets and tabs for dashboard (here) --> final files to open and use in dashboard
# Code to create and use dashboard


################################################################## LINKS
# GRAPHS
# https://plotly.com/python/line-charts/


# SENTIMENT ANALYSIS 
# https://larevueia.fr/nlp-avec-python-analyse-de-sentiments-sur-twitter/#:~:text=Le%20nettoyage%20des%20tweets%20comprendra,facile%20avec%20les%20reg%2Dex
# https://melaniewalsh.github.io/Intro-Cultural-Analytics/05-Text-Analysis/04-Sentiment-Analysis.html
# # https://pythonfordatascienceorg.wordpress.com/chi-square-python/
# https://sites.google.com/view/aide-python/statistiques/test-du-%CF%87-khi2
# https://proc.conisar.org/2018/pdf/4812.pdf
# https://datascientest.com/test-du-khi-2
# https://stackoverflow.com/questions/59162071/chi-square-test-for-multiple-features-in-pandas
# https://codereview.stackexchange.com/questions/96761/chi-square-independence-test-for-two-pandas-df-columns
# https://stats.stackexchange.com/questions/2391/what-is-the-relationship-between-a-chi-squared-test-and-test-of-equal-proportion
# https://www.pythonfordatascience.org/chi-square-test-of-independence-python/
# https://larevueia.fr/nlp-avec-python-analyse-de-sentiments-sur-twitter/#:~:text=Le%20nettoyage%20des%20tweets%20comprendra,facile%20avec%20les%20reg%2Dex
# https://stackoverflow.com/questions/48963354/sentiment-analysis-using-bigrams


# WORDCLOUDS 
# https://dash.plotly.com/datatable/tooltips
# https://stackoverflow.com/questions/75370081/dash-datatable-with-individual-formatting-of-certain-cells
# https://stackoverflow.com/questions/68866089/can-i-save-a-high-resolution-image-of-my-plotly-scatter-plot
# To improve resolution it can be scale parameter or adjust width and height but it didn't work for me
# We can do it within the dcc.Graph(style={"height": "500px", "width":"700px"}, config={"scale":6})
# https://pypi.org/project/dash-holoniq-wordcloud/
# https://stackoverflow.com/questions/72361789/best-way-to-generate-a-high-quality-word-cloud-image-for-a-dash-app
# https://plotly.com/python/templates/
# https://plotly.github.io/plotly.py-docs/generated/plotly.io.write_image.html
# https://stackoverflow.com/questions/49851280/showing-a-simple-matplotlib-plot-in-plotly-dash
# https://github.com/Coding-with-Adam/Dash-by-Plotly/blob/master/Other/Altair-matplot-bokeh/matplot-dash.py
# https://community.plotly.com/t/showing-matplotlib-figures-with-dash-mpl-to-plotly/15710
# https://github.com/plotly/dash-core-components/issues/403


# https://stackoverflow.com/questions/68866089/can-i-save-a-high-resolution-image-of-my-plotly-scatter-plot





# LDA 
# https://stackoverflow.com/questions/32313062/what-is-the-best-way-to-obtain-the-optimal-number-of-topics-for-a-lda-model-usin
# https://radimrehurek.com/gensim/models/ldamodel.html
# https://www.tutorialspoint.com/gensim/gensim_documents_and_lda_model.htm
# https://towardsdatascience.com/latent-dirichlet-allocation-lda-9d1cd064ffa2
# https://curiousml.github.io/teaching/DSA/dsa_nlp_tp_corr.html
# https://github.com/plotly/dash-sample-apps/tree/main/apps/dash-cytoscape-lda



# NETWORK GRAPH
# https://www.earthdatascience.org/courses/use-data-open-source-python/intro-to-apis/calculate-tweet-word-bigrams/
# https://networkx.org/documentation/stable/tutorial.html
# https://networkx.org/documentation/networkx-1.9/tutorial/tutorial.html
# https://stackoverflow.com/questions/65960506/plotly-dash-plotting-networkx-in-python
# https://stackoverflow.com/questions/49851280/showing-a-simple-matplotlib-plot-in-plotly-dash
# https://matplotlib.org/stable/gallery/user_interfaces/web_application_server_sgskip.html
# https://stackoverflow.com/questions/56777332/local-png-image-not-loading
# https://stackoverflow.com/questions/72323871/in-plotly-dash-how-do-i-source-a-local-image-file-to-dash-html-img

# https://dash.plotly.com/cytoscape/events to print label of selected node
# https://stackoverflow.com/questions/66623561/working-with-dash-cytoscape-from-data-frame to create nodes and labels from data frame
# https://dash.plotly.com/cytoscape/callbacks to make the user choose the color of nodes and edges and also but dropdown list to make the user choose the layout of the graph (circle, grid, etc)
# https://dash.plotly.com/cytoscape
# https://plotly.com/python/network-graphs/
# https://www.analyticsvidhya.com/blog/2021/09/what-are-n-grams-and-how-to-implement-them-in-python/#:~:text=N%2Dgrams%20are%20continuous%20sequences,(Natural%20Language%20Processing)%20tasks.
# https://rpubs.com/Shaahin/anxiety-bigram




################################################################## LOAD LIBRARIES 


# Data manipulation, data cleaning
import pandas as pd
import numpy as np


# Sentiment analysis
from textblob import TextBlob # create sentiments scores
from textblob_fr import PatternTagger, PatternAnalyzer # create sentiments scores
import scipy.stats # chi-squred test

# LDA analysis
import spacy
#from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel
# Vizualization (LDA)
import pyLDAvis.gensim_models
#import warnings
#import importlib

# Network graph
from nltk import bigrams
import itertools
import collections
import networkx as nx
import matplotlib.pyplot as plt
from nltk import ngrams








################################################################## OPEN FILES

################################################# PATHS
clean_files_path = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/Final codes and files/Clean_data/final_df_cleaned/"

path_to_export = "C:/Users/mario/Downloads/"

################################################# MAIN FILE
final_df = pd.read_csv(clean_files_path+'final_df_cleaned_python.csv', sep='\t',index_col=0)

# explore data 
final_df.columns # columns User, Tweet, Lanugage, Tweet_coordinates, Tweet_place, etc
final_df.dtypes # all columns have dtype object

# Convert List_words column from object to string 
final_df["List_words"] = final_df["List_words"].astype('string')
# Check types of column 
final_df.dtypes # Column List_words is a string type

# Count how many different users we have
final_df.User.nunique() # 6705 so on 7639 tweets in total, only 6705 expressed by different users





################################################################## PROCESSINGS



################################################# TAB 1 DATA INFORMATION

################################################# TAB 1 DATA INFORMATION
################################################# SUBTAB 1 BASIC INFORMATION ABOUT THE DASHBOARD

# Text written in the dashboard app directly


################################################# TAB 1 DATA INFORMATION
################################################# SUBTAB 2 NUMBER OF COLLECTED TWEETS

### Histogram of sentiment analysis per year (how many tweets collected for the different years)
# Create column Year
final_df['Year'] = pd.DatetimeIndex(final_df['Dates']).year

# Create  df with years and number of tweets 
df_nb_tweets_year = final_df[['Tweet']].groupby(final_df['Year']).count().reset_index()



################################################# TAB 1 DATA INFORMATION
################################################# SUBTAB 3 NUMBER OF COLLECTED TWEETS PER MONTH EACH YEAR

# Create df with number of tweets per month each year (user selects which year he wants to see)
# Create column Month
final_df['Month'] = pd.DatetimeIndex(final_df['Dates']).month

# Replace Month numbers by Month names 
final_df.loc[final_df["Month"] == 1, "Month"] = "January"
final_df.loc[final_df["Month"] == 2, "Month"] = "February"
final_df.loc[final_df["Month"] == 3, "Month"] = "March"
final_df.loc[final_df["Month"] == 4, "Month"] = "April"
final_df.loc[final_df["Month"] == 5, "Month"] = "May"
final_df.loc[final_df["Month"] == 6, "Month"] = "June"
final_df.loc[final_df["Month"] == 7, "Month"] = "July"
final_df.loc[final_df["Month"] == 8, "Month"] = "August"
final_df.loc[final_df["Month"] == 9, "Month"] = "September"
final_df.loc[final_df["Month"] == 10, "Month"] = "October"
final_df.loc[final_df["Month"] == 11, "Month"] = "November"
final_df.loc[final_df["Month"] == 12, "Month"] = "December"


# df for year 2019
# Subset rows where year = 2019
df_nb_tweets_2019 = final_df[final_df["Year"] == 2019]
# count number of tweets for each month during year 2019
df_nb_tweets_2019_month = df_nb_tweets_2019[['Tweet']].groupby(df_nb_tweets_2019['Month']).count().reset_index()
# add column year with value = 2019
df_nb_tweets_2019_month["Year"] = 2019


# df for year 2020
# Subset rows where year = 2020
df_nb_tweets_2020 = final_df[final_df["Year"] == 2020]
# count number of tweets for each month during year 2020
df_nb_tweets_2020_month = df_nb_tweets_2020[['Tweet']].groupby(df_nb_tweets_2020['Month']).count().reset_index()
# add column year with value = 2020
df_nb_tweets_2020_month["Year"] = 2020


# df for year 2021
# Subset rows where year = 2021
df_nb_tweets_2021 = final_df[final_df["Year"] == 2021]
# count number of tweets for each month during year 2021
df_nb_tweets_2021_month = df_nb_tweets_2021[['Tweet']].groupby(df_nb_tweets_2021['Month']).count().reset_index()
# add column year with value = 2021
df_nb_tweets_2021_month["Year"] = 2021


# df for year 2022
# Subset rows where year = 2022
df_nb_tweets_2022 = final_df[final_df["Year"] == 2022]
# count number of tweets for each month during year 2020
df_nb_tweets_2022_month = df_nb_tweets_2022[['Tweet']].groupby(df_nb_tweets_2022['Month']).count().reset_index()
# add column year with value = 2022
df_nb_tweets_2022_month["Year"] = 2022

# create list to store all df for number of tweet per month each year
list_all_nb_tweets_month = [df_nb_tweets_2019_month, df_nb_tweets_2020_month, df_nb_tweets_2021_month, df_nb_tweets_2022_month]
# create df to store all df for number of tweet per month each year
all_df_nb_tweet_month = pd.concat(list_all_nb_tweets_month)



################################################# TAB 1 DATA INFORMATION
################################################# SUBTAB 4 CHRONOLOGICAL EVENTS

# Word document created by hand with main events during covid in France (lock downs, curfew, etc)
# Opened directly in the code for dash app






###################################################################################################################################################





################################################# TAB 2 SENTIMENT ANALYSIS

### Generate sentiments (positive, negative, neutral)

# Create polarity_sentiments_teletravail an empty list that will store the different sentiments
polarity_sentiments_teletravail = []

# Create for loop to associate sentiments with each cleaned tweets
# For each tweet in the column Liwt_words
for tweet in final_df["List_words"]:
    # We append the associated sentiment of each tweet in our list polarity_sentiments_teletravail
  polarity_sentiments_teletravail.append(TextBlob(tweet,pos_tagger=PatternTagger(),analyzer=PatternAnalyzer()).sentiment[0])
  
# if polarity < 0 it means negative sentiment
# if polarity = 0 it means neutral sentiment
# if polarity > 0 it means positive sentiment



### Add column Sentiment_scores_teletravail that contain the sentiments that we have generated

# We add polarity_sentiments_teletravail list to dataframe in a new column called Sentiment_scores_teletravail
# Create column Sentiment_scores_teletravail with polarity_sentiments_teletravail list results
final_df["Sentiment_scores_teletravail"] = polarity_sentiments_teletravail

# Round Sentiment_scores_teletravail results
final_df['Sentiment_scores_teletravail'] = final_df['Sentiment_scores_teletravail'].round(decimals = 2)



### Create column Sentiment_obtained_teletravail 

# It has Neutre if Sentiment_scores_teletravail is equal to 0
# It has Positif if Sentiment_scores_teletravail is higher than 0
# It has Negatif if Sentiment_scores_teletravail if smaller than 0
# After reading the results for 35 tweets, the sentiments didn't fit very well
# Therefore, we adjusted the scores (more appropriate but not perfect) : 
# positive between ]0.30 to +1], neutral 0.3 and negative  between [-1 to 0.30[
final_df.loc[final_df["Sentiment_scores_teletravail"] > 0.30, "Sentiment_obtained_teletravail"] = "Positive"
final_df.loc[final_df["Sentiment_scores_teletravail"] < 0.30, "Sentiment_obtained_teletravail"] = "Negative"
final_df.loc[final_df["Sentiment_scores_teletravail"] == 0.30, "Sentiment_obtained_teletravail"] = "Neutral"



### Create column Rouded_time to have the rounded values of time to get to know how many positive, negative, neutral tweets per hour 
final_df['Rounded_Time'] = pd.to_datetime(final_df['Time'], format='%H:%M:%S').dt.hour



### Create column days if we want to do analysis or visualization and compare between week days and weekend days 
# Convert column Dates into a date object with to_datetime
final_df['Dates'] = pd.to_datetime(final_df['Dates'])
# Create column Days that contains the days extracted from Dates column
final_df['Days'] = final_df['Dates'].dt.day_name()







################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 1 (id 5) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS

### Number and percentage of tweets positive, negative, neutral (all years all hours)
# User chooses between bar chart or pie chart 

# Create  df with years and number of tweets 
df_nb_tweets_sentiment = final_df[['Tweet']].groupby(final_df['Sentiment_obtained_teletravail']).count().reset_index()
# negative then neutral then positive

# Calculate percentage of positive , neutral and negative tweets  
final_df['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 78%
# neutral 3%
# positive 19%

# We add the result in the new column Sentiment_obtained_percentage
df_nb_tweets_sentiment["Sentiment_obtained_percentage"] = [78, 3, 19]





################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 2 (id 6) PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS

### Percentage of positive,negative,neutral tweets per time ranges (4 different)
# User chooses between pie chart or bar chart
# We created different time ranges and tested them with chi-squared to know if there is a relationship between time and sentiments
# We didn't put all tests here, we kept only two of them for the examples
# In the end we kept those time ranges : Midnight-9am ; 9am-1pm ; 1pm-5pm and 5pm-Midnight


### Khi-squared test
# Get to know if there is a link between sentiments and time 
# we can use khi-squared test to do that 
# H0 There is no relationship between variable one and variable two.
# H1 There is a relationship between variable 1 and variable 2.
# If the p-value is significant, you can reject the null hypothesis and claim that the findings support the alternative hypothesis.

# We have to create different time slots/ranges 
# We can decide to do time slots based on work, work break and sleep
# We can decide to do time slots based on quartiles 
# We can decide to do time slots based on median or mean


# Convert object to datetime
final_df['Time'] = pd.to_datetime(final_df['Time'])
# Note : By doing that we have new dates in Time column but it's not the real date of the tweets
# The real date of the tweets is in the other column Dates (this one has wrong times)

# Calculate mean time
final_df['Time'].mean() # Timestamp('2023-03-11 13:14:54.167430400')

# Calculate median time
final_df['Time'].median() # Timestamp('2023-03-11 13:04:01')

# Calculate mode time
final_df['Time'].mode() # 0   2023-03-11 07:00:41
#                                   1   2023-03-11 07:30:00
#                                   2   2023-03-11 10:48:40

# desciribe() function can give us a few parameters like how many distinct times, which one is the first, last, top, etc
final_df['Time'].describe()
# count                    7639
# unique                   7179
# top       2023-03-31 10:48:40
# freq                        4
# first     2023-03-31 00:00:01
# last      2023-03-31 23:59:11


# Calculate Q1,Q2,Q3, Q4 (Q2 being the median)
final_df.Time.quantile([0.25,0.5,0.75,1.00])
# 0.25   2023-03-11 09:07:09.500
# 0.50   2023-03-11 13:04:01.000
# 0.75   2023-03-11 17:22:07.000
# 1.00   2023-03-11 23:59:11.000



# Based on this results we can create 2 or 3 different time slots and do the khi-squared test to compare between the 3 possibilities
# First based on Q1-Q4 : Create column Time_range
# Between midnight and 9am
final_df.loc[(final_df["Time"] > '00:00:00') & (final_df["Time"] <= '09:07:00'), "Time_range"] = "Midnight - 9am"

# Between 9am and 1pm
final_df.loc[(final_df["Time"] > '09:07:00') & (final_df["Time"] <= '13:04:00'), "Time_range"] = "9am - 1pm"

# Between 1pm and 5pm
final_df.loc[(final_df["Time"] > '13:04:00') & (final_df["Time"] <= '17:22:07'), "Time_range"] = "1pm - 5pm"

# Between 5pm and midnight
final_df.loc[(final_df["Time"] > '17:22:07'), "Time_range"] = "5pm - Midnight"



# We can also create another column time_range_2 based on work hours and work breaks
# Between 6pm and 7am 
final_df.loc[(final_df["Time"] > '18:00:00'), "Time_range_2"] = "6pm - 7am"

# Between 7am and midday
final_df.loc[(final_df["Time"] >= '07:00:00') & (final_df["Time"] <= '12:00:00'), "Time_range_2"] = "7am - Midday"

# Between midday and 2pm
final_df.loc[(final_df["Time"] > '12:00:00') & (final_df["Time"] <= '14:00:00'), "Time_range_2"] = "Midday - 2pm"

# Between 2pm and 6pm 
final_df.loc[(final_df["Time"] > '14:00:00') & (final_df["Time"] <= '18:00:00'), "Time_range_2"] = "2pm - 6pm"

# Between 6pm and 7am
final_df.loc[(final_df["Time"] < '07:00:00'), "Time_range_2"] = "6pm - 7am"




### Chi-squared test between time ranges and sentiments

# Test between Time_range and sentiments
scipy.stats.chi2_contingency(pd.crosstab(final_df.Time_range, final_df.Sentiment_obtained_teletravail))

# (6.530518743118639,
#  0.3664510854532599,
#  6,
# array([[1034.17646289,  465.55452284,  411.26901427],
 #       [1033.09412227,  465.06728629,  410.83859144],
  #      [1033.63529258,  465.31090457,  411.05380285],
   #     [1033.09412227,  465.06728629,  410.83859144]]))

# The first value (6.530518743118639) is the Chi-square value, 
# followed by the p-value (0.3664510854532599), 
# then comes the degrees of freedom (6), 
# and lastly it outputs the expected frequencies as an array. 
# Since all of the expected frequencies are greater than 5, the chi2 test results can be trusted. 
# Here p value is higher than 5% so we accept H0
# H0 There is no relationship between variable one and variable two
# So there isn't any relationship between time_range 1 and sentiments

# We have to conduct post hoc tests to test where the relationship is between the different levels (categories) 
# of each variable. This example will use the Bonferroni-adjusted p-value method
# Here we don't have to do it because we don't have any relationship

# Test between Time_range_2 and sentiments
scipy.stats.chi2_contingency(pd.crosstab(final_df.Time_range_2, final_df.Sentiment_obtained_teletravail))
# Here p value is higher than 5% so we accept H0
# H0 There is no relationship between variable one and variable two
# So there isn't any relationship between Time_range_2 and sentiments



# If we had a relationship between time_range and sentiments we could do :
# Bonferoni test 
time_slots = pd.get_dummies(final_df['Time_range'])
time_slots.head()


for series in time_slots:
    nl = "\n"
    
    crosstab = pd.crosstab(time_slots[f"{series}"], final_df['Sentiment_obtained_teletravail'])
    print(crosstab, nl)
    chi2, p, dof, expected = scipy.stats.chi2_contingency(crosstab)
    print(f"Chi2 value= {chi2}{nl}p-value= {p}{nl}Degrees of freedom= {dof}{nl}")

# All p-values are higher than 5% so no relationship


# See link to unsertand how bonferoni works if we have to explain it
# https://pythonfordatascienceorg.wordpress.com/chi-square-python/
# Interpretation : 
# We have to calculate p value 
# If p value of test smaller than our chosen p value 
# There there is a signification relationship between that time and the sentiments
# then we can say during this time there is more negative or neutral or positive sentiments



# Create function to apply chi-2 test and have a cleaner output
def chi2_table(series1, series2, to_csv = False, csv_name = None, 
                prop= False):
    
    if type(series1) != list:
        crosstab = pd.crosstab(series1, series2)
        crosstab2 = pd.crosstab(series1, series2, margins= True)
        crosstab_proprow = round(crosstab2.div(crosstab2.iloc[:,-1], axis=0).mul(100, axis=0), 2)
        crosstab_propcol = round(crosstab2.div(crosstab2.iloc[-1,:], axis=1).mul(100, axis=1), 2)
        chi2, p, dof, expected = scipy.stats.chi2_contingency(crosstab)
        
        if prop == False:
            print("\n",
          f"Chi-Square test between " + series1.name + " and " + series2.name,
          "\n", "\n",
          crosstab2,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
            if to_csv == True:
                if csv_name == None:
                    csv_name = f"{series2.name}.csv"
                                             
                file = open(csv_name, 'a')
                file.write(f"{crosstab2.columns.name}\n")
                file.close()
                crosstab2.to_csv(csv_name, header= True, mode= 'a')
                file = open(csv_name, 'a')
                file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                file.write("\n")
                file.close()              
                
        if prop == 'Row':
            print("\n",
          f"Chi-Square test between " + series1.name + " and " + series2.name,
          "\n", "\n",
          crosstab_proprow,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
            if to_csv == True:
                if csv_name == None:
                    csv_name = f"{series2.name}.csv"
                
                file = open(csv_name, 'a')
                file.write(f"{crosstab_proprow.columns.name}\n")
                file.close()
                crosstab_proprow.to_csv(csv_name, header= True, mode= 'a')
                file = open(csv_name, 'a')
                file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                file.write("\n")
                file.close()

        if prop == 'Col':
            print("\n",
          f"Chi-Square test between " + series1.name + " and " + series2.name,
          "\n", "\n",
          crosstab_propcol,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")

            if to_csv == True:
                if csv_name == None:
                    csv_name = f"{series2.name}.csv"
                    
                file = open(csv_name, 'a')
                file.write(f"{crosstab_propcol.columns.name}\n")
                file.close()
                crosstab_propcol.to_csv(csv_name, header= True, mode= 'a')
                file = open(csv_name, 'a')
                file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                file.write("\n")
                file.close()

    elif type(series1) == list and type(series2) == list:
        for entry2 in series2:
            for entry1 in series1:
                crosstab = pd.crosstab(entry1, entry2)
                crosstab2 = pd.crosstab(entry1, entry2, margins= True)
                crosstab_proprow = round(crosstab2.div(crosstab2.iloc[:,-1], axis=0).mul(100, axis=0), 2)
                crosstab_propcol = round(crosstab2.div(crosstab2.iloc[-1,:], axis=1).mul(100, axis=1), 2)
                chi2, p, dof, expected = scipy.stats.chi2_contingency(crosstab)
                
                if prop == False:
            
                    print("\n",
          f"Chi-Square test between " + entry1.name + " and " + entry2.name,
          "\n", "\n",
          crosstab2,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
                    if to_csv == True:
                        
                        file = open("%s.csv" %(entry2.name), 'a')
                        file.write(f"{crosstab2.columns.name}\n")
                        file.close()
                        crosstab2.to_csv("%s.csv" %(entry2.name), header= True, mode= 'a')
                        file = open("%s.csv" %(entry2.name), 'a')
                        file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                        file.write("\n")
                        file.close()                        

                if prop == 'Row':
            
                    print("\n",
          f"Chi-Square test between " + entry1.name + " and " + entry2.name,
          "\n", "\n",
          crosstab_proprow,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
                    if to_csv == True:
                        file = open("%s.csv" %(entry2.name), 'a')
                        file.write(f"{crosstab_proprow.columns.name}\n")
                        file.close()
                        crosstab_proprow.to_csv("%s.csv" %(entry2.name), header= True, mode= 'a')
                        file = open("%s.csv" %(entry2.name), 'a')
                        file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                        file.write("\n")
                        file.close()
                    
                if prop == 'Col':
            
                    print("\n",
          f"Chi-Square test between " + entry1.name + " and " + entry2.name,
          "\n", "\n",
          crosstab_propcol,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
                    if to_csv == True:
                        file = open("%s.csv" %(entry2.name), 'a')
                        file.write(f"{crosstab_propcol.columns.name}\n")
                        file.close()
                        crosstab_propcol.to_csv("%s.csv" %(entry2.name), header= True, mode= 'a')
                        file = open("%s.csv" %(entry2.name), 'a')
                        file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                        file.write("\n")
                        file.close()


    elif type(series1) == list:
        for entry in series1:
            crosstab = pd.crosstab(entry, series2)
            crosstab2 = pd.crosstab(entry, series2, margins= True)
            crosstab_proprow = round(crosstab2.div(crosstab2.iloc[:,-1], axis=0).mul(100, axis=0), 2)
            crosstab_propcol = round(crosstab2.div(crosstab2.iloc[-1,:], axis=1).mul(100, axis=1), 2)
            chi2, p, dof, expected = scipy.stats.chi2_contingency(crosstab)
            
            if prop == False:
                print("\n",
          f"Chi-Square test between " + entry.name + " and " + series2.name,
          "\n", "\n",
          crosstab2,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
                if to_csv == True:
                    file = open("%s.csv" %(series2.name), 'a')
                    file.write(f"{crosstab2.columns.name}\n")
                    file.close()
                    crosstab2.to_csv("%s.csv" %(series2.name), header= True, mode= 'a')
                    file = open("%s.csv" %(series2.name), 'a')
                    file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                    file.write("\n")
                    file.close()

            if prop == 'Row':
                print("\n",
          f"Chi-Square test between " + entry.name + " and " + series2.name,
          "\n", "\n",
          crosstab_proprow,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
                if to_csv == True:
                    file = open("%s.csv" %(series2.name), 'a')
                    file.write(f"{crosstab_proprow.columns.name}\n")
                    file.close()
                    crosstab_proprow.to_csv("%s.csv" %(series2.name), header= True, mode= 'a')
                    file = open("%s.csv" %(series2.name), 'a')
                    file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                    file.write("\n")
                    file.close()

            if prop == 'Col':
                print("\n",
          f"Chi-Square test between " + entry.name + " and " + series2.name,
          "\n", "\n",
          crosstab_propcol,
          "\n", "\n",
          f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
            
                if to_csv == True:
                    file = open("%s.csv" %(series2.name), 'a')
                    file.write(f"{crosstab_propcol.columns.name}\n")
                    file.close()
                    crosstab_propcol.to_csv("%s.csv" %(series2.name), header= True, mode= 'a')
                    file = open("%s.csv" %(series2.name), 'a')
                    file.write(f"Pearson Chi2({dof})= {chi2:.4f} p-value= {p:.4f}")
                    file.write("\n")
                    file.close()


# Use of the custom function with Time_range 
chi2_table(final_df["Time_range"],final_df["Sentiment_obtained_teletravail"])

# If we want to export csv file with chi-2 test results we can do
#chi2_table(final_df["Time_range"],final_df["Sentiment_obtained_teletravail"],
 #          to_csv= True, csv_name = "Chi_square_tests_1.csv")

# Open csv if necessary
#chi2_timerange = pd.read_csv('C:/Users/mario/downloads/Chi_square_tests_1.csv')



# To have proportions returned in the crosstab, pass either ‘Row’ or ‘Col’ into the “prop= ” argument. 
# The data is tested using the count data, so we don’t violate on of the assumptions of the test, 
# but then returns the proportion data.
chi2_table(final_df['Time_range'], final_df['Sentiment_obtained_teletravail'], prop= 'Row')

chi2_table(final_df['Time_range'], final_df['Sentiment_obtained_teletravail'], prop= 'Col')



###  Chi-2 test with the second time range Time_range_2
# Use of the custom function
chi2_table(final_df["Time_range_2"],final_df["Sentiment_obtained_teletravail"])

# Here p-value smaller than before because = 0.21
# But p value still higher than 5% so we accept H0
# Which means no relationship between time_range_2 and sentiments

# Other time ranges have been tested in other code files





### Create dataframes with the different time ranges for plots in dash app

final_df_range_1 = final_df.loc[final_df["Time_range"] == "Midnight - 9am"]
final_df_range_2 = final_df.loc[final_df["Time_range"] == "9am - 1pm"]
final_df_range_3 = final_df.loc[final_df["Time_range"] == "1pm - 5pm"]
final_df_range_4 = final_df.loc[final_df["Time_range"] == "5pm - Midnight"]
             


### Calculate percentage of positive,negative,neutral tweets for each time range
# Create df percentage for each different time range (empty)
df_sentiment_percentage_range_1 = pd.DataFrame()
df_sentiment_percentage_range_2 = pd.DataFrame()
df_sentiment_percentage_range_3 = pd.DataFrame()
df_sentiment_percentage_range_4 = pd.DataFrame()



# Create Sentiment_obtained_percentage column that has the percentage of the occurrence of the different 
# sentiments in Sentiment_obtained_teletravail column of our dataframe 
# We calculate the percentage for each df_sentiment_percentage (time range 1,2,3,4)
df_sentiment_percentage_range_1["Sentiment_obtained_percentage"] = final_df_range_1['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
df_sentiment_percentage_range_2["Sentiment_obtained_percentage"] = final_df_range_2['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
df_sentiment_percentage_range_3["Sentiment_obtained_percentage"] = final_df_range_3['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
df_sentiment_percentage_range_4["Sentiment_obtained_percentage"] = final_df_range_4['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100

# Reset index of the dataframe df_sentiment_percentage because it has "Positif", "Negatif","Neutre" instead of numbers
df_sentiment_percentage_range_1 = df_sentiment_percentage_range_1.reset_index()
df_sentiment_percentage_range_2 = df_sentiment_percentage_range_2.reset_index()
df_sentiment_percentage_range_3 = df_sentiment_percentage_range_3.reset_index()
df_sentiment_percentage_range_4 = df_sentiment_percentage_range_4.reset_index()

# Create column Sentiment_obtained by renaming the column index that contains the sentiments (positive, neutral, negative)
df_sentiment_percentage_range_1 = df_sentiment_percentage_range_1.rename(columns={"index": "Sentiment_obtained"})
df_sentiment_percentage_range_2 = df_sentiment_percentage_range_2.rename(columns={"index": "Sentiment_obtained"})
df_sentiment_percentage_range_3 = df_sentiment_percentage_range_3.rename(columns={"index": "Sentiment_obtained"})
df_sentiment_percentage_range_4 = df_sentiment_percentage_range_4.rename(columns={"index": "Sentiment_obtained"})

# Add column time slot to know which hours for which percentage
df_sentiment_percentage_range_1["Time_slot"] = "Midnight - 9am"
df_sentiment_percentage_range_2["Time_slot"] = "9am - 1pm"
df_sentiment_percentage_range_3["Time_slot"] = "1pm - 5pm"
df_sentiment_percentage_range_4["Time_slot"] = "5pm - Midnight"

# Round results for percentage 
df_sentiment_percentage_range_1.Sentiment_obtained_percentage = df_sentiment_percentage_range_1.Sentiment_obtained_percentage.round()
df_sentiment_percentage_range_2.Sentiment_obtained_percentage = df_sentiment_percentage_range_2.Sentiment_obtained_percentage.round()
df_sentiment_percentage_range_3.Sentiment_obtained_percentage = df_sentiment_percentage_range_3.Sentiment_obtained_percentage.round()
df_sentiment_percentage_range_4.Sentiment_obtained_percentage = df_sentiment_percentage_range_4.Sentiment_obtained_percentage.round()

# By rounding results we have more than 100% for range 4 so I round by hand
df_sentiment_percentage_range_4["Sentiment_obtained_percentage"] = [78,19,3]








################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 3 NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS

### Two bar charts but we have all time slots on the same graph instead of separated graph

### Calculate Number of tweets for all time ranges
# Calculate number of tweets range 1
df_time_slot_1 = final_df_range_1[['Tweet']].groupby(final_df_range_1['Sentiment_obtained_teletravail']).count().reset_index()
df_time_slot_1["Time_slot"] = "Midnight - 9am"
# negative 1471, neutral 62, positive 376

# Calculate number of tweets range 2
df_time_slot_2 = final_df_range_2[['Tweet']].groupby(final_df_range_2['Sentiment_obtained_teletravail']).count().reset_index()
df_time_slot_2["Time_slot"] = "9am - 1pm"
#Ê negative 1513, neutral 54, positive 343

# calculate number of tweets range 3
df_time_slot_3 = final_df_range_3[['Tweet']].groupby(final_df_range_3['Sentiment_obtained_teletravail']).count().reset_index()
df_time_slot_3["Time_slot"] = "1pm - 5pm"
# negative 1512, neutral 45, positive 354

# calculate number of tweets range 4
df_time_slot_4 = final_df_range_4[['Tweet']].groupby(final_df_range_4['Sentiment_obtained_teletravail']).count().reset_index()
df_time_slot_4["Time_slot"] = "5pm - Midnight"
# negative 1499, neutral 56, positive 354


# Create list with all time slots together and number of tweets for each sentiment (positive negative neutral)
list_df_all_time_slots = [df_time_slot_1, df_time_slot_2, df_time_slot_3, df_time_slot_4]
df_all_time_slots = pd.concat(list_df_all_time_slots)


### Create dataframe for percentage of tweets for all time ranges
# We show number and percentage so we also have to create a table with percentage
# All time slots together and percentage of tweets for each sentiment (positive negative neutral)
list_df_all_time_slots_percentage = [df_sentiment_percentage_range_1, df_sentiment_percentage_range_2,
                                     df_sentiment_percentage_range_3, df_sentiment_percentage_range_4]

# Create dataframe based on the previous list
df_all_time_slots_percentage = pd.concat(list_df_all_time_slots_percentage)






################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 4 NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS EACH YEAR


### Number or percentage of positive and negative tweets by year on the same graph

### Create dataframes for each year
# df for year 2019
# Subset rows where year = 2019
df_nb_tweets_2019 = final_df[final_df["Year"] == 2019]

# df for year 2020
# Subset rows where year = 2020
df_nb_tweets_2020 = final_df[final_df["Year"] == 2020]

# df for year 2021
# Subset rows where year = 2021
df_nb_tweets_2021 = final_df[final_df["Year"] == 2021]

# df for year 2022
# Subset rows where year = 2022
df_nb_tweets_2022 = final_df[final_df["Year"] == 2022]



### Calculate Numbers and Percentages each year for positive negative neutral tweets
# For Year 2019 calculate number of positive, negative, neutral tweets
df_nb_tweets_per_year_2019 = df_nb_tweets_2019[['Tweet']].groupby(df_nb_tweets_2019['Sentiment_obtained_teletravail']).count().reset_index()
# Add column Year
df_nb_tweets_per_year_2019["Year"] = 2019
# For year 2019 calculate percentage of positive, negative, neutral tweets
df_nb_tweets_2019['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 72%
# neutral 5%
# positive 24%
# We add the results in the new column Sentiment_obtained_percentage
df_nb_tweets_per_year_2019["Sentiment_obtained_percentage"] = [72, 5, 24]


# For year 2020 calculate number of positive, negative, neutral tweets
df_nb_tweets_per_year_2020 = df_nb_tweets_2020[['Tweet']].groupby(df_nb_tweets_2020['Sentiment_obtained_teletravail']).count().reset_index()
# Add column Year
df_nb_tweets_per_year_2020["Year"] = 2020
# For year 2020 calculate percentage of positive, negative, neutral tweets
df_nb_tweets_2020['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 85%
# neutral 2%
# positive 13%
# We add the results in the new column Sentiment_obtained_percentage
df_nb_tweets_per_year_2020["Sentiment_obtained_percentage"] = [85, 2, 13]


# For year 2021
df_nb_tweets_per_year_2021 = df_nb_tweets_2021[['Tweet']].groupby(df_nb_tweets_2021['Sentiment_obtained_teletravail']).count().reset_index()
# Add column Year
df_nb_tweets_per_year_2021["Year"] = 2021
# For year 2020 calculate percentage of positive, negative, neutral tweets
df_nb_tweets_2021['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 86%
# neutral 4%
# positive 10%
# We add the results in the new column Sentiment_obtained_percentage
df_nb_tweets_per_year_2021["Sentiment_obtained_percentage"] = [86, 4, 10]



# For year 2022
df_nb_tweets_per_year_2022 = df_nb_tweets_2022[['Tweet']].groupby(df_nb_tweets_2022['Sentiment_obtained_teletravail']).count().reset_index()
# Add column Year
df_nb_tweets_per_year_2022["Year"] = 2022
# For year 2022 calculate percentage of positive, negative, neutral tweets
df_nb_tweets_2022['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 74%
# neutral 3%
# positive 23%
# We add the results in the new column Sentiment_obtained_percentage
df_nb_tweets_per_year_2022["Sentiment_obtained_percentage"] = [74, 3, 23]



# Store all df per year in a list
list_df_per_year = [df_nb_tweets_per_year_2019, df_nb_tweets_per_year_2020,
                                     df_nb_tweets_per_year_2021, df_nb_tweets_per_year_2022]

# Concatenate all df in list_df_per_year_number on dataframe to use them for graphs in dash app
df_all_per_year = pd.concat(list_df_per_year)







################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 5 (id 30) SENTIMENTS PER HOUR, DAY, MONTH

### 3 Scatter plots, tweets ' sentiments per hour,day,month

### Calculate mean sentiments'scores per rounded time
# Create final_df_by_time to have mean of sentiments scores by rounded time
# 0 is midnight
final_df_by_time = final_df.groupby("Rounded_Time").mean().reset_index()


### Calculate mean sentiments'scores per day
# Create final_df_day to have mean of sentiments scores by day
final_df_day = final_df.groupby("Days").mean()
# Re order data frame to have days from Monday to Sunday in the correct order
days_list = ['Monday', 'Tuesday', 'Wednesday', 'Thursday','Friday','Saturday','Sunday']
final_df_day = final_df_day.loc[days_list]
final_df_day = final_df_day.reset_index()

#test = final_df_time.groupby("Rounded_Time").mean('Sentiment_scores_teletravail').reset_index()
#test = final_df_time.groupby('Rounded_Time', as_index=False)['Sentiment_scores_teletravail'].mean()
# same results


### Calculate mean sentiments'scores per month
# Create final_df_month to have mean of sentiments scores by month
final_df_month = final_df.groupby("Month").mean()
# Re order data frame to have month from january to december in the correct order
month_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
final_df_month = final_df_month.loc[month_list]
final_df_month = final_df_month.reset_index()


### Then there is a small text directly written in dash app exapling the sentiments'scores







################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 6 (id 31) NUMBER AND SENTIMENTS DURING WEEK VS WEEKEND

### 6 graphs 3 percentages and 3 numbers
# User chooses between bar charts or pie charts

### Create two final df, one with week days and one with weekend days

# Create one dataframe with only week days from monday to friday and save it
final_df_week = final_df.loc[final_df['Days'].isin(['Monday','Tuesday','Wednesday','Thursday','Friday'])]

# Create one dataframe with only week end days saturday and sunday and save it
final_df_weekend = final_df.loc[final_df['Days'].isin(['Saturday','Sunday'])]


### Calculate number and percentage of tweets during week and weekend
final_df_week[['Tweet']].count().reset_index() # 6292 tweets during week
final_df_weekend[['Tweet']].count().reset_index() # 1347 tweets during weekend
# Calculate percentage of tweets during week and weekend
# 6292 + 1347 = 7639
# Percentage of tweets during week = (6292/7639)*100 = 82%
# Percentage of tweets during weekend = (1347/7639)*100 = 18%

# Store results in dataframe
df_nb_tweets = pd.DataFrame()
df_nb_tweets['When'] = ["Week","Weekend"]
df_nb_tweets['Number_tweets'] = [6292,1347]
df_nb_tweets['Percentage_tweets'] = [82,18]


### Calculate Number and percentage of positive/negative/neutral tweets during week and weekends

# For week
df_nb_tweets_per_week = final_df_week[['Tweet']].groupby(final_df_week['Sentiment_obtained_teletravail']).count().reset_index()
# Add column When
df_nb_tweets_per_week["When"] = "Week"
# For week, we calculate percentage of positive, negative, neutral tweets
final_df_week['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 78%
# neutral 3%
# positive 19%
# We add the results in the new column Sentiment_obtained_percentage
df_nb_tweets_per_week["Sentiment_obtained_percentage"] = [78, 3, 19]


# For weekend
df_nb_tweets_per_weekend = final_df_weekend[['Tweet']].groupby(final_df_weekend['Sentiment_obtained_teletravail']).count().reset_index()
# Add column When
df_nb_tweets_per_weekend["When"] = "Weekend"
# For weekend, we calculate percentage of positive, negative, neutral tweets
final_df_weekend['Sentiment_obtained_teletravail'].value_counts(normalize=True)*100
# negative 83%
# neutral 1%
# positive 16%
# We add the results in the new column Sentiment_obtained_percentage
df_nb_tweets_per_weekend["Sentiment_obtained_percentage"] = [83, 1, 16]




###################################################################################################################################################





################################################# TAB 3 WORDCLOUDS

# I've tried to create and save wordclouds as png on computer then open them in dash app
# But image resolution is bad so I have to create them in the dash app directly
# We just need appropriate dataframes that we'll create here and export them as csv files



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 1 GENERAL WORDCLOUD

# We use final_df in dash app directly to generate the general wordcloud



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 2 POSITIVE WORDCLOUD

# To generate the wordcloud based on tweets considered as positive only, we need a dataframe with positive tweets only
final_df_positive = final_df[final_df["Sentiment_obtained_teletravail"] == "Positive"]
# Then we export it and open it in dash app to generate the positive wordcloud



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 3 NEGATIVE WORDCLOUD

# To generate the wordcloud based on tweets considered as negative only, we need a dataframe with negative tweets only
final_df_negative = final_df[final_df["Sentiment_obtained_teletravail"] == "Negative"]
# Then we export it and open it in dash app to generate the negative wordcloud



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 4 NEUTRAL WORDCLOUD

# To generate the wordcloud based on tweets considered as neutral only, we need a dataframe with neutral tweets only
final_df_neutral = final_df[final_df["Sentiment_obtained_teletravail"] == "Neutral"]
# Then we export it and open it in dash app to generate the neutral wordcloud



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 5 WORDCLOUDS TABLE

### Calculate words occurrences in final_df for the general wordcloud

# Check frequency of words  with str count or str contains
# final_df[final_df['Preprocessed_tweets'].str.contains("fatigue")] # 285 or with
final_df.Preprocessed_tweets.str.count("fatigue").sum() #299
final_df.Preprocessed_tweets.str.count("teletravail").sum() #5590
final_df.Preprocessed_tweets.str.count("aime").sum() # 1078
final_df.Preprocessed_tweets.str.count("risque").sum() #6 764
final_df.Preprocessed_tweets.str.count("dépression").sum()  # 561
final_df.Preprocessed_tweets.str.count("confinement").sum() #♣ 390
final_df.Preprocessed_tweets.str.count("trajet").sum() #' 331
final_df.Preprocessed_tweets.str.count("déprime").sum() # 355
final_df.Preprocessed_tweets.str.count("anxiété").sum() # 389
final_df.Preprocessed_tweets.str.count("enfant").sum() # 301
final_df.Preprocessed_tweets.str.count("seul").sum() # 238
final_df.Preprocessed_tweets.str.count("santé").sum() # 216
final_df.Preprocessed_tweets.str.count("épuisé").sum() # 194
final_df.Preprocessed_tweets.str.count("burnout").sum() # 108
final_df.Preprocessed_tweets.str.count("mal").sum() # 535
final_df.Preprocessed_tweets.str.count("stress").sum() # 530
final_df.Preprocessed_tweets.str.count("famille").sum() # 128
final_df.Preprocessed_tweets.str.count("femme").sum() # 157
final_df.Preprocessed_tweets.str.count("cool").sum() # 199
final_df.Preprocessed_tweets.str.count("problème").sum() # 134
final_df.Preprocessed_tweets.str.count("isolement").sum() # 92
final_df.Preprocessed_tweets.str.count("adore").sum() # 311
final_df.Preprocessed_tweets.str.count("çava").sum() # 174
final_df.Preprocessed_tweets.str.count("heureux").sum() # 26
final_df.Preprocessed_tweets.str.count("surcharge").sum() # 43
final_df.Preprocessed_tweets.str.count("incroyable").sum() # 26
final_df.Preprocessed_tweets.str.count("content").sum() # 58
final_df.Preprocessed_tweets.str.count("physique").sum() # 72
final_df.Preprocessed_tweets.str.count("réunion").sum() # 67
final_df.Preprocessed_tweets.str.count("vélo").sum() # 141
final_df.Preprocessed_tweets.str.count("train").sum() # 141
final_df.Preprocessed_tweets.str.count("voiture").sum() # 152
final_df.Preprocessed_tweets.str.count("covid").sum() # 379
final_df.Preprocessed_tweets.str.count("solitude").sum() # 56
final_df.Preprocessed_tweets.str.count("travailler").sum() # 319
final_df.Preprocessed_tweets.str.count("malade").sum() # 50
final_df.Preprocessed_tweets.str.count("crise").sum() # 107
final_df.Preprocessed_tweets.str.count("peur").sum() # 80




### Calculate words occurrences in final_df_positive for the positive wordcloud

final_df_positive.Preprocessed_tweets.str.count("fatigue").sum() #38
final_df_positive.Preprocessed_tweets.str.count("teletravail").sum() #1310
final_df_positive.Preprocessed_tweets.str.count("aime").sum() # 481
final_df_positive.Preprocessed_tweets.str.count("risque").sum() #97
final_df_positive.Preprocessed_tweets.str.count("dépression").sum()  # 82
final_df_positive.Preprocessed_tweets.str.count("confinement").sum() # 43
final_df_positive.Preprocessed_tweets.str.count("trajet").sum() #' 50
final_df_positive.Preprocessed_tweets.str.count("déprime").sum() # 44
final_df_positive.Preprocessed_tweets.str.count("anxiété").sum() # 46
final_df_positive.Preprocessed_tweets.str.count("enfant").sum() # 42
final_df_positive.Preprocessed_tweets.str.count("seul").sum() # 18
final_df_positive.Preprocessed_tweets.str.count("santé").sum() # 31
final_df_positive.Preprocessed_tweets.str.count("épuisé").sum() # 28
final_df_positive.Preprocessed_tweets.str.count("burnout").sum() # 11
final_df_positive.Preprocessed_tweets.str.count("mal").sum() # 43
final_df_positive.Preprocessed_tweets.str.count("stress").sum() # 63
final_df_positive.Preprocessed_tweets.str.count("famille").sum() # 20
final_df_positive.Preprocessed_tweets.str.count("femme").sum() # 36
final_df_positive.Preprocessed_tweets.str.count("cool").sum() # 36
final_df_positive.Preprocessed_tweets.str.count("problème").sum() # 27
final_df_positive.Preprocessed_tweets.str.count("isolement").sum() # 16
final_df_positive.Preprocessed_tweets.str.count("adore").sum() # 177
final_df_positive.Preprocessed_tweets.str.count("çava").sum() # 43
final_df_positive.Preprocessed_tweets.str.count("heureux").sum() # 16
final_df_positive.Preprocessed_tweets.str.count("surcharge").sum() # 33
final_df_positive.Preprocessed_tweets.str.count("incroyable").sum() # 15
final_df_positive.Preprocessed_tweets.str.count("content").sum() # 28
final_df_positive.Preprocessed_tweets.str.count("physique").sum() # 38
final_df_positive.Preprocessed_tweets.str.count("réunion").sum() # 21
final_df_positive.Preprocessed_tweets.str.count("vélo").sum() # 24
final_df_positive.Preprocessed_tweets.str.count("train").sum() # 28
final_df_positive.Preprocessed_tweets.str.count("voiture").sum() # 21
final_df_positive.Preprocessed_tweets.str.count("covid").sum() # 50
final_df_positive.Preprocessed_tweets.str.count("solitude").sum() # 36
final_df_positive.Preprocessed_tweets.str.count("travailler").sum() # 72
final_df_positive.Preprocessed_tweets.str.count("malade").sum() # 1
final_df_positive.Preprocessed_tweets.str.count("crise").sum() # 13
final_df_positive.Preprocessed_tweets.str.count("peur").sum() # 22



### Calculate words occurrences in final_df_negative for the negative wordcloud

final_df_negative.Preprocessed_tweets.str.count("fatigue").sum() #172
final_df_negative.Preprocessed_tweets.str.count("teletravail").sum() #2854
final_df_negative.Preprocessed_tweets.str.count("aime").sum() # 542
final_df_negative.Preprocessed_tweets.str.count("risque").sum() #465
final_df_negative.Preprocessed_tweets.str.count("dépression").sum()  # 307
final_df_negative.Preprocessed_tweets.str.count("confinement").sum() # 235
final_df_negative.Preprocessed_tweets.str.count("trajet").sum() # 224
final_df_negative.Preprocessed_tweets.str.count("déprime").sum() # 203
final_df_negative.Preprocessed_tweets.str.count("anxiété").sum() # 209
final_df_negative.Preprocessed_tweets.str.count("enfant").sum() # 196
final_df_negative.Preprocessed_tweets.str.count("seul").sum() # 208
final_df_negative.Preprocessed_tweets.str.count("santé").sum() # 132
final_df_negative.Preprocessed_tweets.str.count("épuisé").sum() # 100
final_df_negative.Preprocessed_tweets.str.count("burnout").sum() # 74
final_df_negative.Preprocessed_tweets.str.count("mal").sum() # 436
final_df_negative.Preprocessed_tweets.str.count("stress").sum() # 297
final_df_negative.Preprocessed_tweets.str.count("famille").sum() # 88
final_df_negative.Preprocessed_tweets.str.count("femme").sum() # 91
final_df_negative.Preprocessed_tweets.str.count("cool").sum() # 96
final_df_negative.Preprocessed_tweets.str.count("problème").sum() # 82
final_df_negative.Preprocessed_tweets.str.count("isolement").sum() # 52
final_df_negative.Preprocessed_tweets.str.count("adore").sum() # 128
final_df_negative.Preprocessed_tweets.str.count("çava").sum() # 93
final_df_negative.Preprocessed_tweets.str.count("heureux").sum() # 10
final_df_negative.Preprocessed_tweets.str.count("surcharge").sum() # 6
final_df_negative.Preprocessed_tweets.str.count("incroyable").sum() # 10
final_df_negative.Preprocessed_tweets.str.count("content").sum() # 28
final_df_negative.Preprocessed_tweets.str.count("physique").sum() # 31
final_df_negative.Preprocessed_tweets.str.count("réunion").sum() # 30
final_df_negative.Preprocessed_tweets.str.count("vélo").sum() # 85
final_df_negative.Preprocessed_tweets.str.count("train").sum() # 96
final_df_negative.Preprocessed_tweets.str.count("voiture").sum() # 113
final_df_negative.Preprocessed_tweets.str.count("covid").sum() # 216
final_df_negative.Preprocessed_tweets.str.count("solitude").sum() # 16
final_df_negative.Preprocessed_tweets.str.count("travailler").sum() # 191
final_df_negative.Preprocessed_tweets.str.count("malade").sum() # 49
final_df_negative.Preprocessed_tweets.str.count("crise").sum() # 64
final_df_negative.Preprocessed_tweets.str.count("peur").sum() # 41



### Calculate words occurrences in final_df_neutral for the neutral wordcloud

final_df_neutral.Preprocessed_tweets.str.count("fatigue").sum() #89
final_df_neutral.Preprocessed_tweets.str.count("teletravail").sum() #1426
final_df_neutral.Preprocessed_tweets.str.count("aime").sum() # 55
final_df_neutral.Preprocessed_tweets.str.count("risque").sum() #202
final_df_neutral.Preprocessed_tweets.str.count("dépression").sum()  # 172
final_df_neutral.Preprocessed_tweets.str.count("confinement").sum() # 112
final_df_neutral.Preprocessed_tweets.str.count("trajet").sum() # 57
final_df_neutral.Preprocessed_tweets.str.count("déprime").sum() # 108
final_df_neutral.Preprocessed_tweets.str.count("anxiété").sum() # 134
final_df_neutral.Preprocessed_tweets.str.count("enfant").sum() # 63
final_df_neutral.Preprocessed_tweets.str.count("seul").sum() # 12
final_df_neutral.Preprocessed_tweets.str.count("santé").sum() # 53
final_df_neutral.Preprocessed_tweets.str.count("épuisé").sum() # 66
final_df_neutral.Preprocessed_tweets.str.count("burnout").sum() # 23
final_df_neutral.Preprocessed_tweets.str.count("mal").sum() # 56
final_df_neutral.Preprocessed_tweets.str.count("stress").sum() # 170
final_df_neutral.Preprocessed_tweets.str.count("famille").sum() # 20
final_df_neutral.Preprocessed_tweets.str.count("femme").sum() # 30
final_df_neutral.Preprocessed_tweets.str.count("cool").sum() # 67
final_df_neutral.Preprocessed_tweets.str.count("problème").sum() # 25
final_df_neutral.Preprocessed_tweets.str.count("isolement").sum() # 24
final_df_neutral.Preprocessed_tweets.str.count("adore").sum() # 6
final_df_neutral.Preprocessed_tweets.str.count("çava").sum() # 38
final_df_neutral.Preprocessed_tweets.str.count("heureux").sum() # 0
final_df_neutral.Preprocessed_tweets.str.count("surcharge").sum() # 4
final_df_neutral.Preprocessed_tweets.str.count("incroyable").sum() # 1
final_df_neutral.Preprocessed_tweets.str.count("content").sum() # 2
final_df_neutral.Preprocessed_tweets.str.count("physique").sum() # 3
final_df_neutral.Preprocessed_tweets.str.count("réunion").sum() # 16
final_df_neutral.Preprocessed_tweets.str.count("vélo").sum() # 32
final_df_neutral.Preprocessed_tweets.str.count("train").sum() # 17
final_df_neutral.Preprocessed_tweets.str.count("voiture").sum() # 18
final_df_neutral.Preprocessed_tweets.str.count("covid").sum() # 113
final_df_neutral.Preprocessed_tweets.str.count("solitude").sum() # 4
final_df_neutral.Preprocessed_tweets.str.count("travailler").sum() # 56
final_df_neutral.Preprocessed_tweets.str.count("malade").sum() # 0
final_df_neutral.Preprocessed_tweets.str.count("crise").sum() # 30
final_df_neutral.Preprocessed_tweets.str.count("peur").sum() # 17




### Create dataframe with all results from above
wordclouds_table = pd.DataFrame()

# Create column words with the list of words counted before
wordclouds_table["Words"] = ["fatigue", "teletravail", "aime", "risque", "dépression", "confinement", "trajet",
                             "déprime", "anxiété", "enfant", "seul", "santé", "épuisé", "burnout", "mal",
                             "stress", "famille", "femme", "cool", "problème", "isolement", "adore", "çava", "heureux",
                             "surcharge", "incroyable", "content", "physique", "réunion", "vélo", "train", "voiture",
                             "covid", "solitude", "travailler", "malade", "crise", "peur"]

# Create column Words_Positive (frequencies of the previous words when the sentiment is positive)
wordclouds_table["Words_Positive"] = [38, 1310, 481, 97, 82, 43, 50, 44, 46, 42, 18, 31, 28, 11, 43, 63, 20, 36, 36, 
                                      27, 16, 177, 43, 16, 33, 15, 28, 38, 21, 24, 28, 21, 50, 36, 72, 1, 13, 22]

# Create column Words_Negative (frequencies of the previous words when the sentiment is negative)
wordclouds_table["Words_Negative"] = [172, 2854, 542, 465, 307, 235, 224, 203, 209, 196, 208, 132, 100, 74, 436, 297, 
                                      88, 91, 96, 82, 52, 128, 96, 10, 6, 10, 28, 31, 30, 85, 96, 113, 216, 16, 191, 49, 64, 41]

# Create column Words_Neutral (frequencies of the previous words when the sentiment is neutral)
wordclouds_table["Words_Neutral"] = [89, 1426, 55, 202, 172, 112, 57, 108, 134, 63, 12, 53, 66, 23, 56, 170, 20, 30, 67, 
                                     25, 24, 6, 38, 0, 4, 1, 2, 3, 16, 32, 17, 18, 113, 4, 56, 0, 30, 17]

# Create column Words_Total (frequencies of the previous words in all tweets all sentiments)
wordclouds_table["Words_Total"] = wordclouds_table["Words_Positive"] + wordclouds_table["Words_Negative"] + wordclouds_table["Words_Neutral"]








###################################################################################################################################################






################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL

# Help with : https://curiousml.github.io/teaching/DSA/dsa_nlp_tp_corr.html
# LDA is a model that allows to do topic modeling
# Topic modeling is used to discover the different topics that are in our data
# Topic modeling doesn't create a title for each topics. It's up to us to create a title for each topic found
# For example it will find the words/topics "player", "game", "ball" and it's up to us to create the title soccer
# It's a non supervized approach


################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 1 LDA MODEL INTERPRETATION

# Text explaining what is LDA and its interpretation (directly in dash app)




################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 2 LDA MODEL 5 TOPICS

# Once LDA models are created, I can display them in dash app from the path where there are in my computer 
# https://curiousml.github.io/teaching/DSA/dsa_nlp_tp_corr.html



### Clean and vectorize the documents to create list docs_teletravail


### Before training LDA model we have to tokenize our text
# We tokenize with spaCy library
# We load a pipeline trained for french language
# We store this pipeline in nlp_teletravail
nlp_teletravail = spacy.load("fr_core_news_sm")



### Add or remove stopwords from the default stop words list
# Have a look at the default stopwords from fr_core_news_sm and store them in default_stopwords_nlp
default_stopwords_nlp = nlp_teletravail.Defaults.stop_words

# Remove some words from the default stopwords list (in order to keep them in our analysis)
nlp_teletravail.vocab["seule"].is_stop = False
nlp_teletravail.vocab["seul"].is_stop = False
nlp_teletravail.vocab["seules"].is_stop = False
nlp_teletravail.vocab["seuls"].is_stop = False

# We can add new stopwords which are  not on the default stopwords list (words that we don't want to keep in our analysis)
nlp_teletravail.vocab["en"].is_stop = True

# We can add the stopwords that I used to create the wordclouds
# We can compare the two lists to know the differences between my stopwords list from wordcloud and the default stop word here
# stopwords_teletravail = [...]
# second_set = set(stopwords_teletravail)
# default_stopwords_nlp.symmetric_difference(second_set) 

nlp_teletravail.vocab["Preprocessed_tweets"].is_stop = True
nlp_teletravail.vocab["c"].is_stop = True
nlp_teletravail.vocab["h"].is_stop = True
nlp_teletravail.vocab["d"].is_stop = True
nlp_teletravail.vocab["hui"].is_stop = True
nlp_teletravail.vocab["s"].is_stop = True
nlp_teletravail.vocab["m"].is_stop = True
nlp_teletravail.vocab["v"].is_stop = True
nlp_teletravail.vocab["l"].is_stop = True
nlp_teletravail.vocab["salut"].is_stop = True
nlp_teletravail.vocab["lmaoooo"].is_stop = True
nlp_teletravail.vocab["denis"].is_stop = True
nlp_teletravail.vocab["commeunlundi"].is_stop = True
nlp_teletravail.vocab["Name"].is_stop = True
nlp_teletravail.vocab["ben"].is_stop = True
nlp_teletravail.vocab["cec"].is_stop = True
nlp_teletravail.vocab["giec"].is_stop = True
nlp_teletravail.vocab["quadra"].is_stop = True
nlp_teletravail.vocab["moui"].is_stop = True
nlp_teletravail.vocab["ainperdu"].is_stop = True
nlp_teletravail.vocab["tain"].is_stop = True
nlp_teletravail.vocab["dom"].is_stop = True
nlp_teletravail.vocab["habitent"].is_stop = True
nlp_teletravail.vocab["Length"].is_stop = True
nlp_teletravail.vocab["so"].is_stop = True
nlp_teletravail.vocab["domicil"].is_stop = True
nlp_teletravail.vocab["qu"].is_stop = True
nlp_teletravail.vocab["avant"].is_stop = True
nlp_teletravail.vocab["pendant"].is_stop = True
nlp_teletravail.vocab["etc"].is_stop = True






### Create spacy document and list of words docs_teletravail

# Create spacy_docs_teletravail to create spacy document for each tweet
spacy_docs_teletravail = list(nlp_teletravail.pipe(final_df["Preprocessed_tweets"]))


# Now we have a list of documents for each tweet with spacy stored in spacy_docs_teletravail
# We can transform each document in a list of tokens (words)
# We are going to :
    # 1- remove word that have less than 2 letters
    # 2- remove stop words
    # 3- lemmatize the words left
    # 4- put words in lower case
    
# Create empty list called docs_teletravail
docs_teletravail = []

# Pre processing
# For each document in spacy_docs_teletravail
for doc in spacy_docs_teletravail:
    # We create empty list called tokens_teletravail
    tokens_teletravail = []
    # for each token in each document of spacy_docs_teletravail
    for token in doc:
        # pre processing 1 and 2
        if len(token.orth_) > 2 and not token.is_stop:     
            # pre processing 3 and 4
            tokens_teletravail.append( token.lemma_.lower() ) 
            # append results of tokens_teletravail in docs_teletravail
    docs_teletravail.append( tokens_teletravail )


# Save list to open it to create network graph
# Function to save list
def saveList(myList,filename):
    # the filename should mention the extension 'npy'
    np.save(filename,myList)
    
# Save list (it will be saved in the path by default in spyder on the top right)
saveList(docs_teletravail,'docs_teletravail_list.npy')




### Create dictionnary based on docs_teletravail list

# Last step of the pre processing specific to Gensim
# We are going to create a dictionnary representation of the documents
# This dictionnary will map each word to a unique id
# It will help us to create bag-of-words of each document
# These bag-of-words contain the id of the words from our dictionnary and their frequency (in number of occurence)
# We can remove words the most and the least frequent from the vocabulary
# It will improve the quality of the model and it will accelerate its training


# Create dictionary_teletravail based on our list docs_teletravail
dictionary_teletravail = Dictionary(docs_teletravail)
print('Nombre de mots unique dans les documents initiaux :', len(dictionary_teletravail))
########################################################################
# If we want to filter words according to their occurence we can do :
#dictionary_teletravail.filter_extremes(no_below=3, no_above=0.25)
#print('Nombre de mots unique dans les documents après avoir enlevé les mots fréquents/peu fréquents :', len(dictionary_teletravail))

# I didn't do it because I think we want to keep all of our words
# Explanation to understand no_below and no_above : https://stackoverflow.com/questions/51634656/filtering-tokens-by-frequency-using-filter-extremes-in-gensim
# And documentation : https://radimrehurek.com/gensim/corpora/dictionary.html#gensim.corpora.dictionary.Dictionary.filter_extremes
########################################################################


# Let's have a look at the id that we have created for the fifth row of our list docs_teletravail for example
print("Exemple :", dictionary_teletravail.doc2bow(docs_teletravail[4]))        


# Let's create the id for all rows (lists) in our list docs_teletravail
# Representation in bag-of-words for each document of the corpus
# doc2bow method is used
# Create corpus_teletravail using doc2bow on dictionary_teletravail for each doc (list) in our list docs_teletravail
corpus_teletravail = [ dictionary_teletravail.doc2bow(doc) for doc in docs_teletravail]





### Topic modeling with LDA 
# We train the LDA using :
    # corpus_teletravail : the bag-of-words representations of our documents
    # id2token : the index mappings of words
    # num_topics : Number of topics that the model has to identify
    # chunksize : the number of documents that the model has to see at each update (affects memory consumption)
    # passes : number of times where we show the total corpus to the model during the training
    # random_state : seed to make sure of the reproductibility (repeat the same training)
    
# LDA works better if we have a lot of data
# If we have a lot of data we can chose a high num_topics to find
# other parameters of LDA model that I don't use/chose :
    # update_every : update the model each chunk (optimization of memory consumption)
    # alpha : asymetric and auto value : the former uses a fixed normalized asymmetric 1.0/topic no prior and the latter learns an asymmetric prior from your data 
    
    # per_word_topics if True allows the extraction of the topic the most likely according to a word. Each word is affected to a topic
    # phi : value steers this process (of per_word_topics). It's a treshold for a word treated as indicative or not
  

# Subtab with 5 topics so we create only this model with 5 topics and 10 passes
# But other models have been created and tested in other codes

# LDA Model for 5 topics, 10 passes
model_teletravail_5_topics = LdaModel(corpus=corpus_teletravail,
                             id2word=dictionary_teletravail,
                             num_topics=5,
                             chunksize=1000,
                             passes=10,
                             random_state=1) 



### Results and vizualization of LDA Model

# For topic and words in our model_teletravail created with LDA method
# We print the 5 topics with 10 words for each topic
# There are the 10 words the most characteristic for each topic
for (topic, words) in model_teletravail_5_topics.print_topics():
    print("***********")
    print("* topic", topic+1, "*")
    print("***********")
    print(topic+1, ":", words)
    print()
    

# Another way to vizualize our LDA model for teletravail is to use pyLDAvis library to have a visual output
# It shows how much our topics are popular in our corpus, how much they are similar, 
# what are the words the most important for this topic

# Create vis_data_teletravail the graph with our model model_teletravail, our corpus corpus_teletravail 
# and our dictionary dictionary_teletravail
vis_data_teletravail_5_topics = pyLDAvis.gensim_models.prepare(model_teletravail_5_topics, corpus_teletravail, dictionary_teletravail, sort_topics=False)
# After we can export our model (see Export file section)



# Let's have a look at the topics that the LDA model recognizes in some of our documents (tweets)
# We see the probability for each first documents (the fourth first tweets in this example)
n_doc = 4
i = 0
for (text, doc) in zip(final_df["Preprocessed_tweets"][:n_doc], docs_teletravail[:n_doc]):
    i += 1
    print("***********")
    print("* doc", i, "  *")
    print("***********")
    print(text)
    print([(topic+1, prob) for (topic, prob) in model_teletravail_5_topics[dictionary_teletravail.doc2bow(doc)] if prob > 0.1])
    print()




################################################# TESTS COHERENCE NUM TOPICS
################################################# TO DO IF I HAVE TIME
# https://www.tutorialspoint.com/gensim/gensim_documents_and_lda_model.htm

# gensim library, def coeherence_values_computation and then plt for the graph









################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 3 LDA MODEL 3 TOPICS

# Everything was done in the LDA Model 5 topics section
# Here we just have to create the LDA model with 3 topics and vizualize it to be able to export it in the export files section
# LDA Model for 3 topics, 10 passes
model_teletravail_3_topics = LdaModel(corpus=corpus_teletravail,
                             id2word=dictionary_teletravail,
                             num_topics=3,
                             chunksize=1000,
                             passes=10,
                             random_state=1) 


# Create vis_data_teletravail the graph with our model model_teletravail, our corpus corpus_teletravail 
# and our dictionary dictionary_teletravail
vis_data_teletravail_3_topics = pyLDAvis.gensim_models.prepare(model_teletravail_3_topics, corpus_teletravail, dictionary_teletravail, sort_topics=False)
# After we can export our model (see Export file section)

# If we want the vizualization from before we just have to copy and paste from subtab 2 topics 5






###################################################################################################################################################





################################################# TAB 5 NETWORK GRAPH

# To display png files of the network graph in dash app, we need to create and save them
# We need the list dosc_teletravail created with sentiment analysis so we import it if needed 
# Or we create it in the previous sections (sentiment analysis)

# The path on the top right needs to be the one where the list is saved
# Function to open the list
def loadList(filename):
    # the filename should mention the extension 'npy'
    tempNumpyArray=np.load(filename,allow_pickle=True)
    return tempNumpyArray.tolist()

# Open the list
docs_teletravail = loadList('docs_teletravail_list.npy')




################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 1 NETWORK BIGRAMS

### Create bigrams, save them and afterwards create network graph with bigrams

# Create list of lists containing bigrams of our cleaned tweets
bigram_teletravail = [list(bigrams(words)) for words in docs_teletravail]


# View bigrams for the first tweet for example
bigram_teletravail[0]


# Create bigrams_list variable
bigrams_list = list(itertools.chain(*bigram_teletravail))

# Create counter of words in bigrams to count occurence of each word contained in our bigrams
bigram_counts = collections.Counter(bigrams_list)

# Show the 20 most commun bigrams that we have in bigram_counts
bigram_counts.most_common(20)

# Create dataframe of bigram and counter for the 40 most common bigrams
bigram_df_teletravail = pd.DataFrame(bigram_counts.most_common(40), 
                             columns=['bigram', 'count'])

# Create dictionary of bigrams and their counts
bigram_dictionnary_teletravail = bigram_df_teletravail.set_index('bigram').T.to_dict('records')

# Print first row of bigram_dictionnary_teletravail
bigram_dictionnary_teletravail[0]




### Create network graph bigram and export/save it

# Create network plot, store it in network_graph 
# Create empty graph with no nodes and no edges
network_graph  = nx.Graph()

# Create connections between nodes
# Add edges to our graph network_graph 
# Create connections between nodes
# k are all the bigrams (if bigram ('faire','bien') then k0 is faire, k1 is bien)
# v I think the number of times there is each bigrams
for k, v in bigram_dictionnary_teletravail[0].items():
    network_graph.add_edge(k[0], k[1], weight=(v)) # v or v*10

# TEST to have edges bigger with number of times ngram appear (not working)
#for k, v in bigram_dictionnary_teletravail[0].items():
 #   network_graph.add_weighted_edges_from(k, weight=(v))

    

    
#network_graph.add_node("teletravail", weight=100)

#plt.style.use('dark_background')
#plt.style.use('grayscale')
# Define figure and axes sizes (starts here to run all until save fig)
fig, ax = plt.subplots(figsize=(20, 15))

# k distance between the nodes
# seed for reproductibility
pos = nx.spring_layout(network_graph , k=10, seed=7)


# TEST to have edges bigger with number of times ngram appear
# bigram counts
#bigram_counts = [i for i in bigram_df_teletravail['count']]

# Plot networks
network_fig = nx.draw_networkx(network_graph, pos,
                 #edge_labels= edge_weight,
                 #font_size=8,
                 width=3, # width of the edges
                 edge_color='grey',
                 node_color='firebrick',
                 with_labels = False, # Nodes names, if True they are inside the circles but not clean because big
                 ax=ax)  # ax size of the white panel (background)



# TEST to have edges bigger with number of times ngram appear
# nx.draw_networkx_edges(network_graph, pos, width=bigram_counts, ax=ax)
# Result isn't very great

# Create offset labels (labels for nodes appearing above or under the symbols)
# key is the labels' nodes (travail, maison, etc)
# value is the position of the labels' nodes (x and y coordinates)
for key, value in pos.items():
    print(value)
    x, y = value[0]+.050, value[1]+.080 # Define location of labels text
    #x, y = value[0]+.135, value[1]+.045 # Define location of labels text

    ax.text(x, y, # location of labels text
            s=key,
            bbox=dict(facecolor='firebrick', alpha=0.6), # red color and transparency of background with alpha=0.25
            horizontalalignment='center', fontsize=13) # align label text in the center and size of text with fontsize

ax.set_facecolor('beige')
ax.axis('off') # remove black border
fig.set_facecolor('beige')
plt.savefig('C:/Users/mario/Desktop/networkgraph_40bigrams.png') # Export/save networkgraph



# Check how many nodes and oh many edges we have in network_graph
#network_graph.number_of_nodes()
#network_graph.number_of_edges()


# Check which nodes do we have in network_graph
#network_graph.nodes()

# Check which edges do we have in network_graph
#network_graph.edges()

#for test,test2 in network_graph.edges():
 #   print(test,'................',test2)


#for test3, test4 in bigram_df_teletravail.bigram:
 #   print(test3,'............', test4)



# Convert column type
bigram_df_teletravail["bigram"] = bigram_df_teletravail["bigram"].astype('string')
bigram_df_teletravail.dtypes
# Sort values of data frame
bigram_df_teletravail = bigram_df_teletravail.sort_values('count')






################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 2 NETWORK TRIGRAMS

### Create trigrams, save them and afterwards create network graph with trigrams

# How many grams
n=3

# List for trigram
trigram_teletravail = [list(ngrams(words,n)) for words in docs_teletravail]

# Create list
trigrams_list = list(itertools.chain(*trigram_teletravail))

# Create counter of words to count occurence of each word contained in our trigrams
trigram_counts = collections.Counter(trigrams_list)

# Create dataframe of trigram and counter for the 40 most common trigrams
trigram_df_teletravail = pd.DataFrame(trigram_counts.most_common(40), 
                             columns=['trigram', 'count'])

# Create dictionary of trigrams and their counts
trigram_dictionnary_teletravail = trigram_df_teletravail.set_index('trigram').T.to_dict('records')



### Create network graph trigram and export/save it

# Create network plot, store it in network_graph 
# Create empty graph with no nodes and no edges
network_graph_tri  = nx.Graph()

# Create connections between nodes
# Add edges to our graph network_graph 
# Create connections between nodes
# k are all the bigrams
# v I think the number of times there is each bigrams
for k, v in trigram_dictionnary_teletravail[0].items():
    network_graph_tri .add_edge(k[0], k[1], weight=(v)) # v or v*10


# Define figure and axes sizes
fig, ax = plt.subplots(figsize=(20, 15))

# k distance between the nodes
# seed for reproductibility
pos = nx.spring_layout(network_graph_tri , k=10, seed=7)

# Plot networks
nx.draw_networkx(network_graph_tri, pos,
                 #edge_labels= edge_weight,
                 #font_size=8,
                 width=3, # width of the edges
                 edge_color='grey',
                 node_color='firebrick',
                 with_labels = False, # Nodes names, if True they are inside the circles but not clean because big
                 ax=ax)  # ax size of the white panel (background)

# Create offset labels (labels for nodes appearing above or under the symbols)
for key, value in pos.items():
    x, y = value[0]+.080, value[1]+.050 # Define location of labels text
    ax.text(x, y, # location of labels text
            s=key,
            bbox=dict(facecolor='firebrick', alpha=0.6), # red color and transparency of background with alpha=0.25
            horizontalalignment='center', fontsize=13) # align label text in the center and size of text with fontsize

ax.set_facecolor('beige')
ax.axis('off') # remove black border
fig.set_facecolor('beige')
plt.savefig('C:/Users/mario/Desktop/networkgraph_40trigrams.png') # Export/save networkgraph




# Convert column type
trigram_df_teletravail["trigram"] = trigram_df_teletravail["trigram"].astype('string')
trigram_df_teletravail.dtypes
# Sort values of data frame
trigram_df_teletravail = trigram_df_teletravail.sort_values('count')




################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 3 NETWORK FOURGRAMS

### Create fourgrams, save them and afterwards create network graph with trigrams

# How many grams
n_4=4

# Fourgram list
fourgram_teletravail = [list(ngrams(words,n_4)) for words in docs_teletravail]

# Create fourgrams_list variable
fourgrams_list = list(itertools.chain(*fourgram_teletravail))

# Create counter of words in fourgrams to count occurence of each word contained in our fourgrams
fourgram_counts = collections.Counter(fourgrams_list)


# Create dataframe of bigram and counter for the 40 most common fourgrams
fourgram_df_teletravail = pd.DataFrame(fourgram_counts.most_common(40), 
                             columns=['fourgram', 'count'])

# Create dictionary of fourgrams and their counts
fourgram_dictionnary_teletravail = fourgram_df_teletravail.set_index('fourgram').T.to_dict('records')



### Create network graph fourgram and export/save it

# Create network plot, store it in network_graph 
# Create empty graph with no nodes and no edges
network_graph_four  = nx.Graph()

# Create connections between nodes
# Add edges to our graph network_graph 
# Create connections between nodes
# k are all the grams
# v I think the number of times there is each fourgrams
for k, v in fourgram_dictionnary_teletravail[0].items():
    network_graph_four .add_edge(k[0], k[1], weight=(v)) # v or v*10


# Define figure and axes sizes
fig, ax = plt.subplots(figsize=(20, 15))

# k distance between the nodes
# seed for reproductibility
pos = nx.spring_layout(network_graph_four , k=10, seed=7)

# Plot networks
nx.draw_networkx(network_graph_four, pos,
                 #edge_labels= edge_weight,
                 #font_size=8,
                 width=3, # width of the edges
                 edge_color='grey',
                 node_color='firebrick',
                 with_labels = False, # Nodes names, if True they are inside the circles but not clean because big
                 ax=ax)  # ax size of the white panel (background)

# Create offset labels (labels for nodes appearing above or under the symbols)
for key, value in pos.items():
    x, y = value[0]+.080, value[1]+.050 # Define location of labels text
    ax.text(x, y, # location of labels text
            s=key,
            bbox=dict(facecolor='firebrick', alpha=0.6), # red color and transparency of background with alpha=0.25
            horizontalalignment='center', fontsize=13) # align label text in the center and size of text with fontsize




ax.set_facecolor('beige')
ax.axis('off') # remove black border
fig.set_facecolor('beige')   
plt.savefig('C:/Users/mario/Desktop/networkgraph_40fourgrams.png') # Export/save networkgraph



# Convert column type
fourgram_df_teletravail["fourgram"] = fourgram_df_teletravail["fourgram"].astype('string')
fourgram_df_teletravail.dtypes
# Sort values of data frame
fourgram_df_teletravail = fourgram_df_teletravail.sort_values('count')




################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 4 NETWORK FIFTHGRAMS

# I did it but I think I won't display it because there isn't any additional information/words compared to the other graphs

### Create trigrams, save them and afterwards create network graph with trigrams

# How many grams
n_5=5

# List for trigram
fifthgram_teletravail = [list(ngrams(words,n_5)) for words in docs_teletravail]

# Create list
fifthgrams_list = list(itertools.chain(*fifthgram_teletravail))

# Create counter of words to count occurence of each word contained in our trigrams
fifthgram_counts = collections.Counter(fifthgrams_list)

# Create dataframe of trigram and counter for the 40 most common trigrams
fifthgram_df_teletravail = pd.DataFrame(fifthgram_counts.most_common(40), 
                             columns=['trigram', 'count'])

# Create dictionary of trigrams and their counts
fifthgram_dictionnary_teletravail = fifthgram_df_teletravail.set_index('trigram').T.to_dict('records')



### Create network graph trigram and export/save it

# Create network plot, store it in network_graph 
# Create empty graph with no nodes and no edges
network_graph_fifth  = nx.Graph()

# Create connections between nodes
# Add edges to our graph network_graph 
# Create connections between nodes
# k are all the bigrams
# v I think the number of times there is each bigrams
for k, v in fifthgram_dictionnary_teletravail[0].items():
    network_graph_fifth .add_edge(k[0], k[1], weight=(v)) # v or v*10


# Define figure and axes sizes
fig, ax = plt.subplots(figsize=(20, 15))

# k distance between the nodes
# seed for reproductibility
pos = nx.spring_layout(network_graph_fifth , k=10, seed=7)

# Plot networks
nx.draw_networkx(network_graph_fifth, pos,
                 #edge_labels= edge_weight,
                 #font_size=8,
                 width=3, # width of the edges
                 edge_color='grey',
                 node_color='purple',
                 with_labels = False, # Nodes names, if True they are inside the circles but not clean because big
                 ax=ax)  # ax size of the white panel (background)

# Create offset labels (labels for nodes appearing above or under the symbols)
for key, value in pos.items():
    x, y = value[0]+.080, value[1]+.050 # Define location of labels text
    ax.text(x, y, # location of labels text
            s=key,
            bbox=dict(facecolor='red', alpha=0.25), # red color and transparency of background with alpha=0.25
            horizontalalignment='center', fontsize=13) # align label text in the center and size of text with fontsize
    
plt.axis('off')
plt.savefig('C:/Users/mario/Desktop/networkgraph_40fifthgrams.png') # Export/save networkgraph


# Convert column type
fifthgram_df_teletravail["trigram"] = fifthgram_df_teletravail["trigram"].astype('string')
fifthgram_df_teletravail.dtypes
# Sort values of data frame
fifthgram_df_teletravail = fifthgram_df_teletravail.sort_values('count')


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 5 INTERACTIVE NETWORK


# Done directly in dash app




###################################################################################################################################################






########################################################## EXPORT FINAL DATA FRAMES FOR DASH APP

################################################# MAIN FILE
# Export file in UTF-16 to be able to open it with excel
final_df.to_csv(path_to_export+"final_df_dash.csv", sep='\t', encoding='UTF-16')

# Export file to be able to open it with python 
final_df.to_csv(path_to_export+"final_df_dash_python.csv", sep='\t')

# Export clean tweets to use it with iramuteq
clean_tweets_txt = pd.DataFrame(final_df["Preprocessed_tweets"])
# Remove accents
clean_tweets_txt['Preprocessed_tweets']= clean_tweets_txt['Preprocessed_tweets'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

export_path = r'C:\Users\mario\Downloads\cleaned_tweets.txt'
with open(export_path, 'a') as f:
    dfAsString = clean_tweets_txt.to_string(header=False, index=False)
    f.write(dfAsString)
    
    




################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 1 (id 20) BASIC INFORMATION ABOUT THE DASHBOARD

# No files needed


################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 2 (id2) NUMBER OF COLLECTED TWEETS

df_nb_tweets_year.to_csv(path_to_export+"df_nb_tweets_year_python.csv", sep='\t')




################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 3 (id 3) NUMBER OF COLLECTED TWEETS PER MONTH EACH YEAR

all_df_nb_tweet_month.to_csv(path_to_export+"all_df_nb_tweet_month_python.csv", sep='\t')

    

################################################# TAB 1 DATA INFORMATION 
################################################# SUBTAB 4 (id 4) CHRONOLOGICAL EVENTS

# Filed created with word
# Path image_chronological_events = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/chronological_events.png"
# And convert image with image_chronological_events = base64.b64encode(open(image_chronological_events, 'rb').read()).decode('ascii')
# But it's done in the dash app directly







################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 1 (id 5) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS

df_nb_tweets_sentiment.to_csv(path_to_export+"df_nb_tweets_sentiment_python.csv", sep='\t')

################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 2 (id 6) PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS

df_sentiment_percentage_range_1.to_csv(path_to_export+"df_sentiment_percentage_range_1_python.csv", sep='\t')
df_sentiment_percentage_range_2.to_csv(path_to_export+"df_sentiment_percentage_range_2_python.csv", sep='\t')
df_sentiment_percentage_range_3.to_csv(path_to_export+"df_sentiment_percentage_range_3_python.csv", sep='\t')
df_sentiment_percentage_range_4.to_csv(path_to_export+"df_sentiment_percentage_range_4_python.csv", sep='\t')


################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 3 (id 7) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS AND TIME SLOTS

df_all_time_slots.to_csv(path_to_export+"df_all_time_slots_python.csv", sep='\t')
df_all_time_slots_percentage.to_csv(path_to_export+"df_all_time_slots_percentage_python.csv", sep='\t')



################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 4 (id 8) NUMBER OR PERCENTAGE OF TWEETS ACCORDING TO SENTIMENTS EACH YEAR

df_all_per_year.to_csv(path_to_export+"df_all_per_year_python.csv", sep='\t')



################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 5 (id 30) SENTIMENTS PER HOUR, DAY, MONTH

final_df_by_time.to_csv(path_to_export+"final_df_by_time_python.csv", sep='\t')

final_df_day.to_csv(path_to_export+"final_df_day_python.csv", sep='\t')

final_df_month.to_csv(path_to_export+"final_df_month_python.csv", sep='\t')


################################################# TAB 2 SENTIMENT ANALYSIS
################################################# SUBTAB 6 (id 31) NUMBER AND SENTIMENTS DURING WEEK VS WEEKEND

df_nb_tweets.to_csv(path_to_export+"df_nb_tweets_python.csv", sep='\t')

df_nb_tweets_per_week.to_csv(path_to_export+"df_nb_tweets_per_week_python.csv", sep='\t')

df_nb_tweets_per_weekend.to_csv(path_to_export+"df_nb_tweets_per_weekend_python.csv", sep='\t')



###################################################################################################################################################



################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 1 (id 22) GENERAL WORDCLOUD

# Wordclous are generated in dash app directly 

################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 2 (id 23) POSITIVE WORDCLOUD 

final_df_positive.to_csv(path_to_export+"final_df_positive_python.csv", sep='\t')

# Export file in UTF-16 to be able to open it with excel
#final_df_positive.to_csv("C:/Users/mario/Downloads/final_df_positive.csv", sep='\t', encoding='UTF-16')


################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 3 (id 24) NEGATIVE WORDCLOUD

final_df_negative.to_csv(path_to_export+"final_df_negative_python.csv", sep='\t')

# Export file in UTF-16 to be able to open it with excel
#final_df_negative.to_csv("C:/Users/mario/Downloads/final_df_negative.csv", sep='\t', encoding='UTF-16')


################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 4 (id 25) NEUTRAL WORDCLOUD

final_df_neutral.to_csv(path_to_export+"final_df_neutral_python.csv", sep='\t')

# Export file in UTF-16 to be able to open it with excel
#final_df_neutral.to_csv("C:/Users/mario/Downloads/final_df_neutral.csv", sep='\t', encoding='UTF-16')


################################################# TAB 3 WORDCLOUDS
################################################# SUBTAB 5 (id 26) WORDCLOUD TABLE
wordclouds_table.to_csv(path_to_export+"wordclouds_table_python.csv", sep='\t')




###################################################################################################################################################


################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 1 LDA MODEL INTERPRETATION

# Nothing to export

################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 2 LDA MODEL 5 TOPICS

# Export LDA Model for 5 topics and 10 passes
pyLDAvis.save_html(vis_data_teletravail_5_topics, path_to_export+'lda_model_passes_10_topics_5.html')
# Once the graph/file is saved, we can click on it and it will open in our browser and we see:
    # Intertopic Distance Map (via multidimensional scaling)
    # Top-30 Most Salient Terms
    
# Export the graph and store it where we want in our computer (JSON)
#pyLDAvis.save_json(vis_data_teletravail_5_topics, path_to_export+'lda_model_passes_10_topic_5_json.json')


################################################# TAB 4 LATENT DIRICHLET ALLOCATION MODEL
################################################# SUBTAB 2 LDA MODEL 3 TOPICS

# Export LDA Model for 3 topics and 10 passes
pyLDAvis.save_html(vis_data_teletravail_3_topics, path_to_export+'lda_model_passes_10_topics_3.html')
# Once the graph/file is saved, we can click on it and it will open in our browser and we see:
    # Intertopic Distance Map (via multidimensional scaling)
    # Top-30 Most Salient Terms
    
# Export the graph and store it where we want in our computer (JSON)
#pyLDAvis.save_json(vis_data_teletravail_3_topics, path_to_export+'lda_model_passes_10_topic_3_json.json')






###################################################################################################################################################



################################################# TAB 5 NETWORK GRAPH



################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 1 NETWORK BIGRAMS

# Network graph bigram exported before when we created it (we have to run everything together)

bigram_df_teletravail.to_csv(path_to_export+"bigram_df_teletravail_python.csv", sep='\t')


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 2 NETWORK TRIGRAMS

# Network graph trigram exported before when we created it (we have to run everything together)

trigram_df_teletravail.to_csv(path_to_export+"trigram_df_teletravail_python.csv", sep='\t')


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 3 NETWORK FOURGRAMS

# Network graph fourgram exported before when we created it (we have to run everything together)

fourgram_df_teletravail.to_csv(path_to_export+"fourgram_df_teletravail_python.csv", sep='\t')


################################################# TAB 5 NETWORK GRAPH
################################################# SUBTAB 4 INTERACTIVE NETWORK

# Graph needs to be created on the dash app code file

fifthgram_df_teletravail.to_csv(path_to_export+"fifthgram_df_teletravail_python.csv", sep='\t')







