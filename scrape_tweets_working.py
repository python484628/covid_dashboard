# Author : Marion Estoup
# Email : marion_110@hotmail.fr
# Date September 2022

############################## Profesionnal Project M2 DSS / Health consequences and differences between men and women due to teleworking during covid-19 ################


## Notes
## all the keywords to look for and replace synonyms for teletravail
# query_teletravail = "teletravail(fatigue OR epuise OR çava OR ça va OR depression OR deprime OR j'en peux plus OR jen peux plus OR ras le bol OR j'en ai plein le cul OR jen ai plein le cul OR j'en ai marre OR jen ai marre OR isole OR envie de sortir OR stress OR anxiete OR anxieu OR burnout OR risque OR douleur OR super OR cool OR bien OR aime OR adore') lang:fr since:2019-01-01"



####################################### PATHS #####################################

path_to_export = "C:/Users/mario/Downloads/"


####################################### LIBRARIES NEEDED #######################################

## Install packages

#  Install snscrape
#!pip install snscrape


## Import needed modules/libraries

# Import snscrape module as sntwitter
import snscrape.modules.twitter as sntwitter
# Import pandas library
import pandas as pd








####################################### QUERIES #####################################



################### teletravail 

### douleur or fatigue or super (2 negatives, 1 positive)
# 3 keywords to look for because it seems to work, but above 3 it doesn't work well
# We try to mix positive and negative keywords in the same search to mix as much as possible
query_teletravail = "teletravail AND (douleur OR fatigue OR super) lang:fr since:2019-01-01"


### epuise or çava or depression (2 negatives, 1 positive)
# ça va makes ça AND va that's why it doesn't work well with expressions I think
# With ça va I have only teletravail and ça va in the tweets (ça va or ça and later va in the sentence)
# So I put çava and it gives çava or cava but also epuise or depression
query_teletravail_2 = "teletravail AND (epuise OR çava OR depression) lang:fr since:2019-01-01"


### bien or cool or isole (1 negative, 2 positives)
query_teletravail_3 = "teletravail AND (bien OR cool OR isole) lang:fr since:2019-01-01"


### super or stress or adore (1 negative, 2 positives)
query_teletravail_4 = "teletravail AND (super OR stress OR adore) lang:fr since:2019-01-01"

### risque or aime or burnout (2 negatives, 1 positive)
query_teletravail_5 = "teletravail AND (risque OR aime OR burnout) lang:fr since:2019-01-01"


### deprime or anxieu or anxiete (3 negatives)
query_teletravail_6 = "teletravail AND (deprime OR anxieu OR anxiete) lang:fr since:2019-01-01"




################### travail a la maison 


### douleur or fatigue or super (2 negatives, 1 positive)
# 3 keywords to look for because it seems to work, but above 3 it doesn' work well
# We try to mix positive and negative keywords in the same search to mix as much as possible
query_maison = "travail a la maison AND (douleur OR fatigue OR super) lang:fr since:2019-01-01"

### epuise or çava or depression (2 negatives, 1 positive)
# ça va makes ça AND va that's why it doesn't work well with expressions I think
# With ça va I have only teletravail and ça va in the tweets (ça va or ça and later va in the sentence)
# So I put çava and it gives çava or cava but also epuise or depression
query_maison_2 = "travail a la maison AND (epuise OR çava OR depression) lang:fr since:2019-01-01"


### bien or cool or isole (1 negative, 2 positives)
query_maison_3 = "travail a la maison AND (bien OR cool OR isole) lang:fr since:2019-01-01"


### super or stress or adore (1 negative, 2 positives)
query_maison_4 = "travail a la maison AND (super OR stress OR adore) lang:fr since:2019-01-01"

### risque or aime or burnout (2 negatives, 1 positive)
query_maison_5 = "travail a la maison AND (risque OR aime OR burnout) lang:fr since:2019-01-01"


### deprime or anxieu or anxiete (3 negatives)
query_maison_6 = "travail a la maison AND (deprime OR anxieu OR anxiete) lang:fr since:2019-01-01"





################### travail a domicile


### douleur or fatigue or super (2 negatives, 1 positive)
# 3 keywords to look for because it seems to work, but above 3 it doesn't work well
# We try to mix positive and negative keywords in the same search to mix as much as possible
query_domicile = "travail a domicile AND (douleur OR fatigue OR super) lang:fr since:2019-01-01"

### epuise or çava or depression (2 negatives, 1 positive)
# ça va makes ça AND va that's why it doesn't work well with expressions I think
# With ça va I have only teletravail and ça va in the tweets (ça va or ça and later va in the sentence)
# So I put çava and it gives çava or cava but also epuise or depression
query_domicile_2 = "travail a domicile AND (epuise OR çava OR depression) lang:fr since:2019-01-01"


### bien or cool or isole (1 negative, 2 positives)
query_domicile_3 = "travail a domicile AND (bien OR cool OR isole) lang:fr since:2019-01-01"


### super or stress or adore (1 negative, 2 positives)
query_domicile_4 = "travail a domicile AND (super OR stress OR adore) lang:fr since:2019-01-01"

### risque or aime or burnout (2 negatives, 1 positive)
query_domicile_5 = "travail a domicile AND (risque OR aime OR burnout) lang:fr since:2019-01-01"


### deprime or anxieu or anxiete (3 negatives)
query_domicile_6 = "travail a domicile AND (deprime OR anxieu OR anxiete) lang:fr since:2019-01-01"







################################ FUNCTION ###########################


## Create function to scrape data on twitter

# Name of the function : func_to_scrap
# 2 parameters : the query we want to look for (query) and the number of tweets that we want (limit)
def func_to_scrap(query,limit):
    
    # For each tweet that correspond to the query (that contain our keywords)
    # We get them with the get_items() method
    for tweet in sntwitter.TwitterSearchScraper(query).get_items():
        
        
        # if the number of tweets recovered is equal to the limit we have put then we stop scraping
        if len(tweets) == limit:
            break
        
        # if the number of tweets recovered is less than the limit we have put then we add them to our list called tweets
        # In this list tweets, we store the date, the username, the content of the tweets, the language of the tweets, the coordinates of the location, the location
        else:
            tweets.append([tweet.date, tweet.user.username, tweet.content, tweet.lang,
                           tweet.coordinates, tweet.place])
            







########################### USE OF THE FUNCTION FUNC_TO_SCRAP ##########################





#### Use of func_to_scrap with teletravail queries


# Use of func_to_scrap with query_teletravail
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_teletravail and limit (number of tweets) to 1000
func_to_scrap(query_teletravail,1000)

# Create dataframe df_teletravail to store results
df_teletravail = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])


# Use of funct_to_scrap with query_teletravail_2
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_teletravail_2 and limit (number of tweets) to 1000
func_to_scrap(query_teletravail_2,1000)

# Create dataframe df_teletravail_2 to store results
df_teletravail_2 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])


# Use of funct_to_scrap with query_teletravail_3
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_teletravail_3 and limit (number of tweets) to 1000
func_to_scrap(query_teletravail_3,1000)

# Create dataframe df_teletravail_3 to store results
df_teletravail_3 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])




# Use of funct_to_scrap with query_teletravail_4
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_teletravail_4 and limit (number of tweets) to 1000
func_to_scrap(query_teletravail_4,1000)

# Create dataframe df_teletravail_4 to store results
df_teletravail_4 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])



# Use of funct_to_scrap with query_teletravail_5
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_teletravail_5 and limit (number of tweets) to 1000
func_to_scrap(query_teletravail_5,1000)

# Create dataframe df_teletravail_5 to store results
df_teletravail_5 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])



# Use of funct_to_scrap with query_teletravail_6
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_teletravail_6 and limit (number of tweets) to 1000
func_to_scrap(query_teletravail_6,1000)

# Create dataframe df_teletravail_6 to store results
df_teletravail_6 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])












#### Use of func_to_scrap with travail a la maison queries


# Use of func_to_scrap with query_maison
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_maison and limit (number of tweets) to 1000
func_to_scrap(query_maison,1000)

# Create dataframe df_maison to store results
df_maison = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_maison_2
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_maison_2 and limit (number of tweets) to 1000
func_to_scrap(query_maison_2,1000)

# Create dataframe df_maison_2 to store results
df_maison_2 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])




# Use of func_to_scrap with query_maison_3
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_maison and limit (number of tweets) to 1000
func_to_scrap(query_maison_3,1000)

# Create dataframe df_maison_3 to store results
df_maison_3 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_maison_4
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_maison_4 and limit (number of tweets) to 1000
func_to_scrap(query_maison_4,1000)

# Create dataframe df_maison_4 to store results
df_maison_4 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])




# Use of func_to_scrap with query_maison_5
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_maison_5 and limit (number of tweets) to 1000
func_to_scrap(query_maison_5,1000)

# Create dataframe df_maison_5 to store results
df_maison_5 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])




# Use of func_to_scrap with query_maison_6
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_maison_6 and limit (number of tweets) to 1000
func_to_scrap(query_maison_6,1000)

# Create dataframe df_maison_6 to store results
df_maison_6 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])















#### Use of func_to_scrap with travail a domicile queries


# Use of func_to_scrap with query_domicile
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_domicile and limit (number of tweets) to 1000
func_to_scrap(query_domicile,1000)

# Create dataframe df_domicile to store results
df_domicile = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_domicile_2
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_domicile_2 and limit (number of tweets) to 1000
func_to_scrap(query_domicile_2,1000)

# Create dataframe df_domicile_2 to store results
df_domicile_2 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_domicile_3
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_domicile_3 and limit (number of tweets) to 1000
func_to_scrap(query_domicile_3,1000)

# Create dataframe df_domicile_3 to store results
df_domicile_3 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_domicile_4
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_domicile_4 and limit (number of tweets) to 1000
func_to_scrap(query_domicile_4,1000)

# Create dataframe df_domicile_4 to store results
df_domicile_4 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_domicile_5
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_domicile_5 and limit (number of tweets) to 1000
func_to_scrap(query_domicile_5,1000)

# Create dataframe df_domicile_5 to store results
df_domicile_5 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])





# Use of func_to_scrap with query_domicile_6
# Creating empty list called tweets
tweets = []
# Use of func_to_scrap with query_domicile_6 and limit (number of tweets) to 1000
func_to_scrap(query_domicile_6,1000)

# Create dataframe df_domicile_6 to store results
df_domicile_6 = pd.DataFrame(tweets, columns=['Date', 'User', 'Tweet','Language',
                                          'Tweet_coordinates','Tweet_place'])










#################################### EXPORT DATA ###################################

#### EXPORT FILES TO CSV (teletravail)
# Export file in UTF-16 to be able to open it with excel
# Export file without encoding=UTF-16 to be able to open it with python 


#### FIRST, EXPORT FILES TO CSV (TELETRAVAIL) TO BE ABLE TO OPEN THEM WITH EXCEL

# Gather all tabs (dataframes) together in one tab (dataframe)
final_df_teletravail = pd.concat([df_teletravail, df_teletravail_2, df_teletravail_3,
                  df_teletravail_4, df_teletravail_5, df_teletravail_6])


# Export all tabs (dataframes) to csv files
# First tab (df_teletravail)
df_teletravail.to_csv(path_to_export+ 'df_teletravail_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Second tab (df_teletravail_2)
df_teletravail_2.to_csv(path_to_export+ 'df_teletravail_2_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Third tab (df_teletravail_3)
df_teletravail_3.to_csv(path_to_export+ 'df_teletravail_3_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Fourth tab (df_teletravail_4)
df_teletravail_4.to_csv(path_to_export+ 'df_teletravail_4_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Fifth tab (df_teletravail_5)
df_teletravail_5.to_csv(path_to_export+ 'df_teletravail_5_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Sixth tab (df_teletravail_6)
df_teletravail_6.to_csv(path_to_export+ 'df_teletravail_6_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Final tab (final_df_teletravail)
final_df_teletravail.to_csv(path_to_export+ 'final_df_teletravail_excel.csv', sep='\t',index=False, encoding='UTF-16') 




#### SECOND, EXPORT FILES TO CSV (TELETRAVAIL) TO BE ABLE TO OPEN THEM WITH PYTHON

# First tab (df_teletravail)
df_teletravail.to_csv(path_to_export+ 'df_teletravail.csv', sep='\t') 

# Second tab (df_teletravail_2)
df_teletravail_2.to_csv(path_to_export+ 'df_teletravail_2.csv', sep='\t') 

# Third tab (df_teletravail_3)
df_teletravail_3.to_csv(path_to_export+ 'df_teletravail_3.csv', sep='\t') 

# Fourth tab (df_teletravail_4)
df_teletravail_4.to_csv(path_to_export+ 'df_teletravail_4.csv', sep='\t') 

# Fifth tab (df_teletravail_5)
df_teletravail_5.to_csv(path_to_export+ 'df_teletravail_5.csv', sep='\t') 

# Sixth tab (df_teletravail_6)
df_teletravail_6.to_csv(path_to_export+ 'df_teletravail_6.csv', sep='\t') 

# Final tab (final_df_teletravail)
final_df_teletravail.to_csv(path_to_export+ 'final_df_teletravail.csv', sep='\t') 



###########################################################################################################

#### FIRST, EXPORT FILES TO CSV (TRAVAIL A LA MAISON) TO BE ABLE TO OPEN THEM WITH EXCEL

# Gather all tabs (dataframes) together in one tab (dataframe)
final_df_maison = pd.concat([df_maison, df_maison_2, df_maison_3,
                  df_maison_4, df_maison_5, df_maison_6])


# Export all tabs (dataframes) to csv files
# First tab (df_maison)
df_maison.to_csv(path_to_export+ 'df_maison_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Second tab (df_maison_2)
df_maison_2.to_csv(path_to_export+ 'df_maison_2_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Third tab (df_maison_3)
df_maison_3.to_csv(path_to_export+ 'df_maison_3_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Fourth tab (df_maison_4)
df_maison_4.to_csv(path_to_export+ 'df_maison_4_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Fifth tab (df_maison_5)
df_maison_5.to_csv(path_to_export+ 'df_maison_5_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Sixth tab (df_maison_6)
df_maison_6.to_csv(path_to_export+ 'df_maison_6_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Final tab (final_df_maison)
final_df_maison.to_csv(path_to_export+ 'final_df_maison_excel.csv', sep='\t',index=False, encoding='UTF-16') 





#### SECOND, EXPORT FILES TO CSV (TELETRAVAIL) TO BE ABLE TO OPEN THEM WITH PYTHON
# Export all tabs (dataframes) to csv files
# First tab (df_maison)
df_maison.to_csv(path_to_export+ 'df_maison.csv', sep='\t') 

# Second tab (df_maison_2)
df_maison_2.to_csv(path_to_export+ 'df_maison_2.csv', sep='\t') 

# Third tab (df_maison_3)
df_maison_3.to_csv(path_to_export+ 'df_maison_3.csv', sep='\t') 

# Fourth tab (df_maison_4)
df_maison_4.to_csv(path_to_export+ 'df_maison_4.csv', sep='\t') 

# Fifth tab (df_maison_5)
df_maison_5.to_csv(path_to_export+ 'df_maison_5.csv', sep='\t') 

# Sixth tab (df_maison_6)
df_maison_6.to_csv(path_to_export+ 'df_maison_6.csv', sep='\t') 

# Final tab (final_df_maison)
final_df_maison.to_csv(path_to_export+ 'final_df_maison.csv', sep='\t') 






############################################################################################################



#### FIRST, EXPORT FILES TO CSV (TRAVAIL A DOMICILE) TO BE ABLE TO OPEN THEM WITH EXCEL

# Gather all tabs (dataframes) together in one tab (dataframe)
final_df_domicile = pd.concat([df_domicile, df_domicile_2, df_domicile_3,
                  df_domicile_4, df_domicile_5, df_domicile_6])


# Export all tabs (dataframes) to csv files
# First tab (df_domicile)
df_domicile.to_csv(path_to_export+ 'df_domicile_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Second tab (df_domicile_2)
df_domicile_2.to_csv(path_to_export+ 'df_domicile_2_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Third tab (df_domicile_3)
df_domicile_3.to_csv(path_to_export+ 'df_domicile_3_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Fourth tab (df_domicile_4)
df_domicile_4.to_csv(path_to_export+ 'df_domicile_4_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Fifth tab (df_domicile_5)
df_domicile_5.to_csv(path_to_export+ 'df_domicile_5_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Sixth tab (df_domicile_6)
df_domicile_6.to_csv(path_to_export+ 'df_domicile_6_excel.csv', sep='\t',index=False, encoding='UTF-16') 

# Final tab (final_df_domicile)
final_df_domicile.to_csv(path_to_export+ 'final_df_domicile_excel.csv', sep='\t',index=False, encoding='UTF-16') 





#### SECOND, EXPORT FILES TO CSV (TRAVAIL A DOMICILE) TO BE ABLE TO OPEN THEM WITH PYTHON

# Export all tabs (dataframes) to csv files
# First tab (df_domicile)
df_domicile.to_csv(path_to_export+ 'df_domicile.csv', sep='\t') 

# Second tab (df_domicile_2)
df_domicile_2.to_csv(path_to_export+ 'df_domicile_2.csv', sep='\t') 

# Third tab (df_domicile_3)
df_domicile_3.to_csv(path_to_export+ 'df_domicile_3.csv', sep='\t') 

# Fourth tab (df_domicile_4)
df_domicile_4.to_csv(path_to_export+ 'df_domicile_4.csv', sep='\t') 

# Fifth tab (df_domicile_5)
df_domicile_5.to_csv(path_to_export+ 'df_domicile_5.csv', sep='\t') 

# Sixth tab (df_domicile_6)
df_domicile_6.to_csv(path_to_export+ 'df_domicile_6.csv', sep='\t') 

# Final tab (final_df_domicile)
final_df_domicile.to_csv(path_to_export+ 'final_df_domicile.csv', sep='\t') 




























