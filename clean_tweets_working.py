# Author : Marion Estoup
# Email : marion_110@hotmail.fr
# Date November 2022

###################################################################### DATA CLEANING

########################## PATHS
path_to_import = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Mémoire/Projet Pro M2 2022 2023/Final codes and files/Scrape_tweets/First final files without good encoding/"
path_to_export = "C:/Users/mario/Downloads/"



########################## LOAD LIBRARIES

import pandas as pd 

# LDA analysis
import spacy


########################## OPEN FILES

### Open final dataframes (dataframes that contain the tweets that we have collected)


# Open file final_df_teletravail
final_df_teletravail = pd.read_csv(path_to_import+ 'final_df_teletravail.csv', index_col=0) 

# Open file final_df_maison
final_df_maison = pd.read_csv(path_to_import+ 'final_df_maison.csv', index_col=0)

# Open file final_df_domicile
final_df_domicile = pd.read_csv(path_to_import+ 'final_df_domicile.csv', index_col=0) 




########################## CHECK IF DUPLICATED TWEETS

# Put all 3 dataframes in only 1 dataframe 
final_df = pd.concat([final_df_teletravail, final_df_maison, final_df_domicile], axis=0).reset_index(drop=True)

# Check if nan value in Tweet column 
final_df[final_df['Tweet'].isna()]
# No nan values in Tweet column so no need to remove rows

# Check if tweets are duplicated 
final_df[final_df['Tweet'].duplicated()]
# or final_df.pivot_table(index=['Tweet'], aggfunc='size') to know how many times the tweet appears
# Length of 10361 duplicated tweets

# Remove duplicated tweets
final_df = final_df.drop_duplicates('Tweet')
# Now our final_df has 7652 rows




########################## CREATE COLUMNS


# Create column Preprocessed_tweets with the content of the column Tweet 
final_df["Preprocessed_tweets"] = final_df["Tweet"]


# Put the column Preprocessed_tweets to lower case
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.lower()



# Create column Dates and column Time based on the content of the column Date
# Column Dates
final_df['Dates'] = pd.to_datetime(final_df['Date']).dt.date
# Column Time
final_df['Time'] = pd.to_datetime(final_df['Date']).dt.time

# Drop column Date that contains dates and times
final_df = final_df.drop('Date', axis=1)




########################## CLEAN AND VECTORIZE THE DOCUMENTS

# Before training LDA model we have to tokenize our text
# We tokenize with spaCy library
# We load a pipeline trained for french language
# We store this pipeline in nlp_teletravail
nlp_teletravail = spacy.load("fr_core_news_sm")

# See the stopwords by default in nlp library
print(nlp_teletravail.Defaults.stop_words)
# Result 

# {'etais', 'pense', 'après', 'moi-meme', 'votres', 'vous-mêmes', "j'", 'quatrième', 'au', 'hormis', 'té', 'tant', 
# 'anterieure', 'puis', 'tenant', 'nôtre', 'lesquelles', 'hi', 'être', 'lui-même', 'quelque', 'seront', 'doivent', 'suffit', 
# 'notre', 'pour', 'où', 'rendre', 'environ', 'excepté', 'cette', 'dix', 'ils', 'anterieures', 'o', 'soit', 'cent', 
# 'elles-mêmes', 'm’', 'specifique', "d'", 'ne', 'on', 'selon', 'autres', 'exactement', 'avoir', 'un', 'a', 'dessous', 
# 'laisser', 'sauf', 'tellement', 'quinze', 'différents', 'dite', 'restent', 'differents', 'or', 'nous', 'feront', 'aux', 
# 'hou', 'jusque', 'anterieur', 'celle-ci', 't’', 'là', 'pouvait', 'dejà', 'mien', 'sans', 'onze', 'seule', 'antérieure', 
# 'dès', 'i', 'moins', 'effet', 'mon', 'nouveau', 'derrière', 'cinquante', 'en', 'malgre', 'desquelles', 'divers', 
# 'desormais', 'quoique', 'autre', 'nous-mêmes', 'personne', 'juste', 'vas', 'longtemps', 'antérieur', 'devers', 'faisant', 
# 'sien', 'pourrait', 'vers', 'plutot', 'hep', 'telles', 'douze', 'celui-ci', 'suivant', 'ses', 'ait', "m'", 'dehors', 
# 'duquel', 'plusieurs', 'ou', 'autrement', 'pendant', 'soixante', 'premièrement', 'diverses', 'sont', 'differentes', "l'", 
# 'ces', 'relativement', 'faisaient', 'par', 'neuvième', 'soi-meme', "qu'", 'ouvert', 'retour', 'spécifique', 'tes', 
# 'egalement', 'vingt', 'specifiques', 'mes', 'notamment', 'préalable', 'tien', 'dans', 's’', 'toi-meme', 'auraient', 'ho', 
# 'vos', 'na', 'compris', 'laquelle', 'cela', 'permet', 'sait', 'vu', 'cinquième', 'toutes', 'lequel', 'puisque', 'pourrais',
# 'memes', 'première', 'hui', 'tiennes', 'troisièmement', 'également', 'bas', 'peut', 'troisième', 'celles-là', 'lui', 'nos', 
# 'revoilà', 'ceux', 'hé', 'précisement', 'qu’', 'quand', 'ouverte', 'sera', 'suivante', 'font', 'huit', 'neanmoins', 
# 'comment', 'les', 'seraient', 'chacun', 'debout', 'elle-même', 'ô', 'six', 'suit', 'entre', 'es', 'néanmoins', 'dedans', 
# 'maintenant', 'va', 'celui-la', 'son', 'celui', 'derriere', 'treize', "quelqu'un", 'auxquelles', 'seize', 'stop', 'sa', 
# 'tiens', 'y', 'deuxièmement', 'dit', 'allaient', 'eh', 'moindres', 'malgré', 'diverse', 'celle', 'relative', 'suffisant', 
# 'mienne', 'certains', 'nul', 'facon', 'houp', 'pu', 'certain', 'precisement', 'parle', 'différente', 'deja', 'quels', 
# 'aie', 'celles-la', 'le', 'dix-huit', 'via', 'étant', 'trente', 'dessus', 'miens', 'déjà', 'merci', 'ta', 'peux', 'enfin', 
# 'et', 'ça', 'sous', 'surtout', 'celui-là', 'différent', 'sinon', 'autrui', 'tienne', 'vé', 'pas', 'da', 'lorsque', 'assez', 
# 'directe', 'ouste', 'dix-neuf', 'ha', "n'", 'etaient', 'auront', 'touchant', 'de', 'desquels', 'procedant', 'celle-là', 
# 'parler', 'possibles', 'vous', 'trois', 'possible', 'maint', 'hue', 'toujours', 'quarante', 'outre', 'moi', 'sienne', 
# 'eux-mêmes', 'hors', 'ma', 'jusqu', 'suivre', 'vont', 'douzième', 'celles-ci', 'suffisante', 'fais', 'avec', 'étaient', 
# 'differente', 'ni', 'semble', 'celles', 'sent', 'soi-même', 'quiconque', 'il', 'ah', 'cinquantaine', 'ainsi', 'peu', 
# 'meme', 'reste', 'l’', 'nombreux', 'sixième', 'â', 'semblaient', 'moi-même', 'telle', 'devra', 'cependant', 'dits', 
# 'semblable', 'souvent', 'quelconque', 'très', 'd’', 'une', 'revoici', 'certaine', "t'", 'combien', 'ci', 'ès', 'antérieures',
#  'doit', 'toi', 'désormais', 'c’', 'onzième', 'ceci', 'tous', 'mille', 'nombreuses', 'si', 'etre', 'auquel', 'est', 'près', 
# 'alors', 'envers', 'auxquels', 'directement', 'seul', 'tenir', 'devant', 'tout', 'tel', 'voici', 'voilà', 'quel', 
# 'dix-sept', 'allons', 'tu', 'la', 'deux', 'durant', 'aura', 'parfois', 'vôtres', 'chez', 'septième', 'celle-la', 
# 'quant-à-soi', 'quatrièmement', 'sur', 'voila', 'abord', 'que', 'avons', 'pres', 'parce', "c'", 'spécifiques', 'avais', 
# 'pourquoi', 'hem', 'premier', 'eu', 'dire', 'quant', 'lès', 'à', 'aurait', 'deuxième', 'ouverts', 'partant', 'je', 'car', 
# 'dont', 'importe', 'plus', 'unes', 'déja', 'apres', 'parmi', 'restant', 'quoi', 'leurs', 'ont', 'nôtres', 'proche', 
# 'avant', 'etc', 'tres', 'ayant', 'cet', 'prealable', 'des', 'cinq', 'j’', 'elles-memes', 'chacune', 'uns', 'cinquantième', 
# 'seuls', 'suivants', 'gens', 'semblent', 'même', 'votre', 'différentes', 'peuvent', 'quelle', 'était', 'tente', 'as', 
# 'tend', 'ai', 'lesquels', 'aussi', 'donc', 'ceux-ci', 'étais', 'lors', 'bat', 'soi', 'lui-meme', 'ce', 'toi-même', 
# 'mêmes', 'leur', 'suis', 'fait', 'comme', 'elle', 'attendu', 'qui', 'revoila', 'dixième', 'certes', 'toute', 'seulement', 
# 'aupres', 'quatre', 'avaient', 'different', 'eux', 'façon', 'te', 'sept', 'siennes', 'du', 'huitième', 'ton', 'miennes', 
# 'etant', 'depuis', "s'", 'ouias', 'ceux-là', 'afin', 'basee', 'encore', 'seules', 'concernant', 'n’', 'quelques', 'tels', 
# 'avait', 'parlent', 'vais', 'elle-meme', 'elles', 'me', 'certaines', 'suivantes', 'rend', 'vôtre', 'se', 'chaque', 
# 'quelles', 'etait', 'mais', 'plutôt', 'quatorze', 'delà', 'siens', 'serait', 'quatre-vingt'}




# We have to define words that we want to keep (the ones that we want to remove from the default stopwords):
# We can keep "pas", "ça", "suffit", "tellement", "différents", "differents", "sans", "seule", 'moins', 'effet', 'longtemps',
# 'stop', 'suffisant', 'nul', 'sent', 'très', 'seul', 'tres', 'différentes', 'differentes', 'differents', 'different',
# 'seules', 'seuls'
# because they are important in our analysis



# Remove some words from the default stopwords list (in order to keep them in our analysis)
nlp_teletravail.vocab["pas"].is_stop = False
nlp_teletravail.vocab["ça"].is_stop = False
nlp_teletravail.vocab["suffit"].is_stop = False
nlp_teletravail.vocab["tellement"].is_stop = False
nlp_teletravail.vocab["différents"].is_stop = False
nlp_teletravail.vocab["differents"].is_stop = False
nlp_teletravail.vocab["sans"].is_stop = False
nlp_teletravail.vocab["seule"].is_stop = False
nlp_teletravail.vocab["moins"].is_stop = False
nlp_teletravail.vocab["effet"].is_stop = False
nlp_teletravail.vocab["longtemps"].is_stop = False
nlp_teletravail.vocab["stop"].is_stop = False
nlp_teletravail.vocab["suffisant"].is_stop = False
nlp_teletravail.vocab["nul"].is_stop = False
nlp_teletravail.vocab["sent"].is_stop = False
nlp_teletravail.vocab["très"].is_stop = False
nlp_teletravail.vocab["seul"].is_stop = False
nlp_teletravail.vocab["tres"].is_stop = False
nlp_teletravail.vocab["différentes"].is_stop = False
nlp_teletravail.vocab["differentes"].is_stop = False
nlp_teletravail.vocab["différents"].is_stop = False
nlp_teletravail.vocab["differents"].is_stop = False
nlp_teletravail.vocab["seules"].is_stop = False
nlp_teletravail.vocab["seuls"].is_stop = False


# We can add new stopwords which are  not on the default stopwords list (words that we don't want to keep in our analysis)
# We can add "en" which is not in the default list
nlp_teletravail.vocab["en"].is_stop = True







########################## CLEAN TWEET CONTENT (symbols,links, users, numbers, etc)

### Replace users and links 

# Before using spaCy we can replace the users references (@John or @Jack etc) by @user because we won't use the users names
#final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('@\S+', '@user')

# Or replace users by white spaces
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('@\S+', '')


# In the same way we can replace the links that are in the tweets by https
#final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('https\S+', 'https')

# Or replace links by white spaces
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('https\S+', '')



### Replace specific words/expressions

# Replace tele travail, télétravail, télé travail by teletravail
# Check if exists in df 
final_df[final_df['Preprocessed_tweets'].str.contains('télétravail')] # True
final_df[final_df['Preprocessed_tweets'].str.contains('télé travail')] # True
final_df[final_df['Preprocessed_tweets'].str.contains('tele travail')] # True
# True so we replace them by teletravail

# Replace télétravail by teletravail 
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('télétravail', 'teletravail')

# Replace télé travail by teletravail
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('télé travail', 'teletravail')

# Replace télé travail by teletravail
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('tele travail', 'teletravail')



# Replace ça va, ca va, sava, sa va by cava or çava ?
# Check if exist in df
final_df[final_df['Preprocessed_tweets'].str.contains('sava')] # True
final_df[final_df['Preprocessed_tweets'].str.contains('sa va')] # True
final_df[final_df['Preprocessed_tweets'].str.contains('ca va')] # True
final_df[final_df['Preprocessed_tweets'].str.contains('çava')] # True
final_df[final_df['Preprocessed_tweets'].str.contains('ça va')] # True but ça va être... so not the meaning that we are doing well

# Replace those words by çava 
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('sava', 'çava')
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('sa va', 'çava')
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('ca va', 'çava')
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('ça va', 'çava')




# Replace shortcuts and english word by french words
# English word call
final_df[final_df['Preprocessed_tweets'].str.contains('call')]
# True so we can change it by "appel"
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('call', 'appel')


# English word full
final_df[final_df['Preprocessed_tweets'].str.contains('full')]
# True so we can change it by "plein"
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('full', 'plein')



### Replace symbols, emojis, and numbers

# Replace a lot of symbols () , ; : / ! ? ° % " - & etc AND emojis
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace(r'[^\w]', ' ')

# Check for other symbols 
final_df[final_df['Preprocessed_tweets'].str.contains('_')] # True

# Replace other symbols
# Replace hyphens _
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('_', ' ')


# Check if numbers 
final_df[final_df['Preprocessed_tweets'].str.contains('\d+', regex=True)]
# Replace
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace('\d+', '')


# Replace white spaces with length more than one space
final_df["Preprocessed_tweets"] = final_df["Preprocessed_tweets"].str.replace(r'\s+', ' ')







########################## CREATE SPACY DOCUMENT AND LIST OF WORDS


# Create spacy_docs_teletravail to create spacy document for each tweet
spacy_docs_teletravail = list(nlp_teletravail.pipe(final_df["Preprocessed_tweets"]))


# Now we have a list of documents for each tweet with spacy stored in spacy_docs_teletravail
# We can transform each document in a list of tokens
# We are going to :
    # 1- remove word that have less than 2 letters
    # 2- remove stop words
    # 3- lemmatize the words left
    # 4- put words in lower case
    

# Create empty list called docs_teletravail
docs_teletravail = []

# Pre processing
# For each document in spacy_docs_teletravail
for doc in spacy_docs_teletravail:
    # We create empty list called tokens_teletravail
    tokens_teletravail = []
    # for each token in each document of spacy_docs_teletravail
    for token in doc:
        # pre processing 1 and 2
        if len(token.orth_) > 2 and not token.is_stop:     
            # pre processing 3 and 4
            tokens_teletravail.append( token.lemma_.lower() ) 
            # append results of tokens_teletravail in docs_teletravail
    docs_teletravail.append( tokens_teletravail )



# Now our list docs_teletravail contains list in each row
# Each list contains the words kept of each corresponding tweet 
# Let's have a look at the first tokenized document (first list corresponding to the words kept of the first tweet)
print(docs_teletravail[0])
# Result : ['être', 'pas', 'très', 'favorable', 'teletravail', 'super', 'local', 'neuf', 'manque', 'babyfoot', 'courtier',
# 'rejoindre', 'rang', 'start', 'nation']


# Let's have a look at the second tokenized document
print(docs_teletravail[1])
# Result : ['étude', 'spire', 'healthcare', 'révéler', 'teletravail', 'impact', 'cyclemenstruel', 'cause', 'fatigue', 'stress',
#  'manque', 'activité', 'physique', 'induire', 'travail', 'distance']

            




########################## CLEAN LIST OF WORDS DOCS_TELETRAVAIL

## Clean list of words in docs_teletravail (remove the different white spaces that we have)
# Replace \xa0 in docs_teletravail
#docs_teletravail = [[x.replace(u'\xa0', u'') for x in l] for l in docs_teletravail]

# Replace \n in docs_teletravail
#docs_teletravail = [[x.replace(u'\n', u'') for x in l] for l in docs_teletravail]


# Replace empty strings (white spaces)
#docs_teletravail = [[delete for delete in l if delete.strip()] for l in docs_teletravail]






########################## ADD LIST OF WORDS TO DATAFRAME

# Add the column List_words with the content of the list docs_teletravail
final_df["List_words"] = docs_teletravail



########################## EXPORT CLEANED DATAFRAME 


# Now final_df is cleaned and we have the list of words
# We can use it to do analysis and vizualization 
# Export file in UTF-16 to be able to open it with excel
final_df.to_csv(path_to_export+ "final_df_cleaned.csv", sep='\t', encoding='UTF-16')

# Export file to be able to open it with python 
final_df.to_csv(path_to_export+ "final_df_cleaned_python.csv", sep='\t')
